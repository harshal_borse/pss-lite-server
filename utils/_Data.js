// import { generateUID, getUID, generateConfigID } from "./_Data";
let configObj = {
	"elementName": "pssconfiguration",
	"results": [
		{
			"_id": "5b31f56ee4b0cae581deeb32",
			"configid": 141,
			"createdTimestamp": "26-06-2018:13:42:30",
			"customerId": 13,
			"elementId": 14329,
			"elementName": "pssconfiguration",
			"isactive": true,
			"isuploadcompleted": false,
			"lineid": 1,
			"linemodel": {
				"id": 1,
				"nodeid": 1,
				"Name": "Gear.Machining.Line",
				"field": "Line",
				"coupleType": "",
				"isvirtualSubAssembly": false,
				"linelogo": "",
				"linedescription": "",
				"Inputs": [],
				"Outputs": [],
				"States": [],
				"ParentChild": [
					{
						"parent": 1,
						"child": 2,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 3,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 4,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 5,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 6,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 33,
						"isCopy": false
					},
					{
						"parent": 2,
						"child": 86,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 7,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 8,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 9,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 10,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 11,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 12,
						"isCopy": false
					},
					{
						"parent": 3,
						"child": 87,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 14,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 15,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 16,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 17,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 18,
						"isCopy": false
					},
					{
						"parent": 4,
						"child": 19,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 20,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 21,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 22,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 23,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 24,
						"isCopy": false
					},
					{
						"parent": 5,
						"child": 25,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 26,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 27,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 28,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 29,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 30,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 31,
						"isCopy": false
					},
					{
						"parent": 6,
						"child": 32,
						"isCopy": false
					},
					{
						"parent": 7,
						"child": 40,
						"isCopy": false
					},
					{
						"parent": 7,
						"child": 41,
						"isCopy": false
					},
					{
						"parent": 8,
						"child": 42,
						"isCopy": false
					},
					{
						"parent": 8,
						"child": 43,
						"isCopy": false
					},
					{
						"parent": 9,
						"child": 44,
						"isCopy": false
					},
					{
						"parent": 9,
						"child": 45,
						"isCopy": false
					},
					{
						"parent": 10,
						"child": 46,
						"isCopy": false
					},
					{
						"parent": 10,
						"child": 51,
						"isCopy": false
					},
					{
						"parent": 11,
						"child": 47,
						"isCopy": false
					},
					{
						"parent": 11,
						"child": 48,
						"isCopy": false
					},
					{
						"parent": 11,
						"child": 49,
						"isCopy": false
					},
					{
						"parent": 11,
						"child": 50,
						"isCopy": false
					},
					{
						"parent": 11,
						"child": 80,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 52,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 53,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 54,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 55,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 56,
						"isCopy": false
					},
					{
						"parent": 12,
						"child": 57,
						"isCopy": false
					},
					{
						"parent": 14,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 15,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 16,
						"child": 74,
						"isCopy": false
					},
					{
						"parent": 16,
						"child": 75,
						"isCopy": false
					},
					{
						"parent": 17,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 18,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 19,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 20,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 21,
						"child": 76,
						"isCopy": false
					},
					{
						"parent": 21,
						"child": 77,
						"isCopy": false
					},
					{
						"parent": 22,
						"child": 78,
						"isCopy": false
					},
					{
						"parent": 22,
						"child": 79,
						"isCopy": false
					},
					{
						"parent": 23,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 24,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 25,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 26,
						"child": 58,
						"isCopy": false
					},
					{
						"parent": 26,
						"child": 59,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 60,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 61,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 62,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 63,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 64,
						"isCopy": false
					},
					{
						"parent": 27,
						"child": 81,
						"isCopy": false
					},
					{
						"parent": 28,
						"child": 65,
						"isCopy": false
					},
					{
						"parent": 28,
						"child": 66,
						"isCopy": false
					},
					{
						"parent": 28,
						"child": 67,
						"isCopy": false
					},
					{
						"parent": 28,
						"child": 68,
						"isCopy": false
					},
					{
						"parent": 29,
						"child": 82,
						"isCopy": false
					},
					{
						"parent": 30,
						"child": 83,
						"isCopy": false
					},
					{
						"parent": 31,
						"child": 84,
						"isCopy": false
					},
					{
						"parent": 32,
						"child": 85,
						"isCopy": false
					},
					{
						"parent": 33,
						"child": 34,
						"isCopy": false
					},
					{
						"parent": 33,
						"child": 36,
						"isCopy": false
					},
					{
						"parent": 34,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 36,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 40,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 41,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 42,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 43,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 44,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 45,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 46,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 47,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 48,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 49,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 50,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 51,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 52,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 53,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 54,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 55,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 56,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 57,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 58,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 59,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 60,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 61,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 62,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 63,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 64,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 65,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 66,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 67,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 68,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 74,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 75,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 76,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 77,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 78,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 79,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 80,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 81,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 82,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 83,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 84,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 85,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 86,
						"child": 89,
						"isCopy": false
					},
					{
						"parent": 86,
						"child": 90,
						"isCopy": false
					},
					{
						"parent": 87,
						"child": 88,
						"isCopy": false
					},
					{
						"parent": 88,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 89,
						"child": null,
						"isCopy": false
					},
					{
						"parent": 90,
						"child": null,
						"isCopy": false
					},
					{
						"parent": null,
						"child": 1,
						"isCopy": false
					}
				],
				"nodes": [
					{
						"id": 1,
						"nodeid": 2,
						"Name": "Gear.Machining.Cell",
						"field": "Cell",
						"coupleType": "",
						"isvirtualSubAssembly": false,
						"Inputs": [
							{
								"Id": 6,
								"Name": "GANTRY.MANUAL.MODE",
								"Address": "PLC2.DB11.6,X2",
								"InputType": "Sequence",
								"DataType": "BOOL",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							},
							{
								"Id": 5,
								"Name": "GANTRY2.AUTO.RUNNING",
								"Address": "PLC2.DB22.35,X0",
								"InputType": "Sequence",
								"DataType": "BOOL",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							},
							{
								"Id": 4,
								"Name": "GANTRY1.AUTO.RUNNING",
								"Address": "PLC2.DB21.35,X0",
								"InputType": "Sequence",
								"DataType": "BOOL",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							},
							{
								"Id": 3,
								"Name": "GANTRY.SINGLE.CYCLE",
								"Address": "PLC2.DB510.30,B",
								"InputType": "Sequence",
								"DataType": "BYTE",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							},
							{
								"Id": 2,
								"Name": "GANTRY.RUN.OUT.CYCLE",
								"Address": "PLC2.DB510.3,B",
								"InputType": "Sequence",
								"DataType": "BYTE",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							},
							{
								"Id": 1,
								"Name": "GANTRY.AUTO.MODE",
								"Address": "PLC2.DB11.6,X0",
								"InputType": "Sequence",
								"DataType": "BOOL",
								"IOType": "Input",
								"Min": "0",
								"Max": "0"
							}
						],
						"Outputs": [],
						"States": [
							{
								"Id": 261,
								"StateName": "GANTRY.AUTO.MODE",
								"Equation": "GANTRY.AUTO.MODE",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							},
							{
								"Id": 260,
								"StateName": "GANTRY.RUN.OUT.CYCLE",
								"Equation": "GANTRY.RUN.OUT.CYCLE ==1",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							},
							{
								"Id": 259,
								"StateName": "GANTRY.SINGLE.CYCLE",
								"Equation": "GANTRY.SINGLE.CYCLE ==1",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							},
							{
								"Id": 258,
								"StateName": "GANTRY1.AUTO.RUNNING",
								"Equation": "GANTRY1.AUTO.RUNNING",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							},
							{
								"Id": 257,
								"StateName": "GANTRY2.AUTO.RUNNING",
								"Equation": "GANTRY2.AUTO.RUNNING",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							},
							{
								"Id": 256,
								"StateName": "GANTRY.MANUAL.MODE",
								"Equation": "GANTRY.MANUAL.MODE",
								"Priority": 0,
								"subassemblyName": "Gear.Machining.Cell",
								"statetype": "System",
								"SequenceNo": 0,
								"isCustom": true,
								"iscouplingstate": false
							}
						],
						"nodes": [
							{
								"id": 1,
								"nodeid": 3,
								"Name": "Infeed.System",
								"field": "Asset",
								"coupleType": "",
								"isvirtualSubAssembly": false,
								"Inputs": [
									{
										"Id": 5,
										"Name": "INFEED.MANUAL.MODE",
										"Address": "PLC1.DB1000.20,X0",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 4,
										"Name": "INFEED.AUTO.RUNNING",
										"Address": "PLC1.DB1000.20,X3",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 3,
										"Name": "INFEED.SINGLE.CYCLE",
										"Address": "PLC1.DB1000.21,X1",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 2,
										"Name": "INFEED.RUNOUT.CYCLE",
										"Address": "PLC1.DB1000.21,X3",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 1,
										"Name": "INFEED.AUTO.MODE",
										"Address": "PLC1.DB1000.20,X2",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									}
								],
								"Outputs": [],
								"States": [
									{
										"Id": 255,
										"StateName": "INFEED.AUTO.MODE",
										"Equation": "INFEED.AUTO.MODE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 254,
										"StateName": "INFEED.RUNOUT.CYCLE",
										"Equation": "INFEED.RUNOUT.CYCLE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 253,
										"StateName": "INFEED.SINGLE.CYCLE",
										"Equation": "INFEED.SINGLE.CYCLE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 252,
										"StateName": "INFEED.AUTO.RUNNING",
										"Equation": "INFEED.AUTO.RUNNING",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 251,
										"StateName": "INFEED.MANUAL.MODE",
										"Equation": "INFEED.MANUAL.MODE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									}
								],
								"nodes": [
									{
										"id": 7,
										"nodeid": 7,
										"Name": "Loading.Lane1",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "LOADING.LANE1.MC.NUMBER",
												"Address": "PLC1.DB820.4,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "LOADING.LANE1.PART.STATUS",
												"Address": "PLC1.DB820.2,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "LOADING.LANE1.PART.TYPE",
												"Address": "PLC1.DB820.0,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 40,
												"nodeid": 40,
												"Name": "Loading.Lane1.Stopper1",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "LOADING.LANE1.STOPPER1.BUFFER.FULL",
														"Address": "PLC1.I415.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LOADING.LANE1.STOPPER1.RAISED",
														"Address": "PLC1.I415.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LOADING.LANE1.STOPPER1.LOWER",
														"Address": "PLC1.Q460.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LOADING.LANE1.STOPPER1.RAISE",
														"Address": "PLC1.Q460.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 5,
														"StateName": "LOADING.LANE1.STOPPER1.BUFFER.FULL",
														"Equation": "LOADING.LANE1.STOPPER1.BUFFER.FULL",
														"Priority": 0,
														"subassemblyName": "Loading.Lane1.Stopper1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 4,
														"StateName": "LOADING.LANE1.STOPPER1.MOVING.DOWN",
														"Equation": "LOADING.LANE1.STOPPER1.LOWER && LOADING.LANE1.STOPPER1.RAISED",
														"Priority": 0,
														"subassemblyName": "Loading.Lane1.Stopper1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 3,
														"StateName": "LOADING.LANE1.STOPPER1.MOVING.UP",
														"Equation": "LOADING.LANE1.STOPPER1.RAISE && !LOADING.LANE1.STOPPER1.RAISED",
														"Priority": 0,
														"subassemblyName": "Loading.Lane1.Stopper1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 2,
														"StateName": "LOADING.LANE1.STOPPER1.AT.DOWN",
														"Equation": "LOADING.LANE1.STOPPER1.LOWER && !LOADING.LANE1.STOPPER1.RAISED",
														"Priority": 0,
														"subassemblyName": "Loading.Lane1.Stopper1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 1,
														"StateName": "LOADING.LANE1.STOPPER1.AT.UP",
														"Equation": "LOADING.LANE1.STOPPER1.RAISE && LOADING.LANE1.STOPPER1.RAISED",
														"Priority": 0,
														"subassemblyName": "Loading.Lane1.Stopper1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LOADING.LANE1.STOPPER1.LOWER",
														"Address": "PLC1.Q460.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LOADING.LANE1.STOPPER1.RAISE",
														"Address": "PLC1.Q460.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LOADING.LANE1.STOPPER1.BUFFER.FULL",
														"Address": "PLC1.I415.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LOADING.LANE1.STOPPER1.RAISED",
														"Address": "PLC1.I415.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 7
											},
											{
												"id": 41,
												"nodeid": 41,
												"Name": "Escapement.Stopper2",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "ESCAPEMENT.STOPPER2.PART.PASS",
														"Address": "PLC1.I418.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Address": "PLC1.I415.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													},
													{
														"Id": 5,
														"Name": "ESCAPEMENT.STOPPER2.RAISED",
														"Address": "PLC1.I415.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 5
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "ESCAPEMENT.STOPPER2.LOWER",
														"Address": "PLC1.Q460.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "ESCAPEMENT.STOPPER2.RAISE",
														"Address": "PLC1.Q460.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 13,
														"StateName": "ESCAPEMENT.STOPPER2.MOVING.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER2.LOWER && ESCAPEMENT.STOPPER2.RAISED && ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 12,
														"StateName": "ESCAPEMENT.STOPPER2.MOVING.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER2.LOWER && ESCAPEMENT.STOPPER2.RAISED && ! ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 11,
														"StateName": "ESCAPEMENT.STOPPER2.MOVING.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER2.RAISE && ! ESCAPEMENT.STOPPER2.RAISED && ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 10,
														"StateName": "ESCAPEMENT.STOPPER2.MOVING.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER2.RAISE && ! ESCAPEMENT.STOPPER2.RAISED && ! ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 9,
														"StateName": "ESCAPEMENT.STOPPER2.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER2.LOWER && !  ESCAPEMENT.STOPPER2.RAISED && ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 8,
														"StateName": "ESCAPEMENT.STOPPER2.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER2.RAISE && ESCAPEMENT.STOPPER2.RAISED &&  ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 7,
														"StateName": "ESCAPEMENT.STOPPER2.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER2.LOWER && ! ESCAPEMENT.STOPPER2.RAISED && !  ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 6,
														"StateName": "ESCAPEMENT.STOPPER2.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER2.RAISE && ESCAPEMENT.STOPPER2.RAISED && ! ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "ESCAPEMENT.STOPPER2.LOWER",
														"Address": "PLC1.Q460.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "ESCAPEMENT.STOPPER2.RAISE",
														"Address": "PLC1.Q460.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "ESCAPEMENT.STOPPER2.PART.PASS",
														"Address": "PLC1.I418.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "ESCAPEMENT.STOPPER2.PART.PRESENT",
														"Address": "PLC1.I415.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "ESCAPEMENT.STOPPER2.RAISED",
														"Address": "PLC1.I415.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 7
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 8,
										"nodeid": 8,
										"Name": "Loading.Lane2",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "LOADING.LANE2.MC.NUMBER",
												"Address": "PLC2.DB820.24,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "LOADING.LANE2.PART.STATUS",
												"Address": "PLC1.DB820.22,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "LOADING.LANE2.PART.TYPE",
												"Address": "PLC1.DB820.20,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 42,
												"nodeid": 42,
												"Name": "Stopper3",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "STOPPER3.BUFFER.FULL",
														"Address": "PLC1.I416.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "STOPPER3.RAISED",
														"Address": "PLC1.I416.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "STOPPER3.LOWER",
														"Address": "PLC1.Q461.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "STOPPER3.RAISE",
														"Address": "PLC1.Q461.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 18,
														"StateName": "STOPPER3.BUFFER.FULL",
														"Equation": "STOPPER3.BUFFER.FULL",
														"Priority": 0,
														"subassemblyName": "Stopper3",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 17,
														"StateName": "STOPPER3.MOVING.DOWN",
														"Equation": "STOPPER3.LOWER && STOPPER3.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper3",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 16,
														"StateName": "STOPPER3.MOVING.UP",
														"Equation": "STOPPER3.RAISE && !STOPPER3.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper3",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 15,
														"StateName": "STOPPER3.AT.DOWN",
														"Equation": "STOPPER3.LOWER && !STOPPER3.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper3",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 14,
														"StateName": "STOPPER3.AT.UP",
														"Equation": "STOPPER3.RAISE && STOPPER3.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper3",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "STOPPER3.LOWER",
														"Address": "PLC1.Q461.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "STOPPER3.RAISE",
														"Address": "PLC1.Q461.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "STOPPER3.BUFFER.FULL",
														"Address": "PLC1.I416.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "STOPPER3.RAISED",
														"Address": "PLC1.I416.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 8
											},
											{
												"id": 43,
												"nodeid": 43,
												"Name": "Escapement.Stopper4",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "ESCAPEMENT.STOPPER4.PART.PASS",
														"Address": "PLC1.I418.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Address": "PLC1.I416.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													},
													{
														"Id": 5,
														"Name": "ESCAPEMENT.STOPPER4.RAISED",
														"Address": "PLC1.I416.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 5
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "ESCAPEMENT.STOPPER4.LOWER",
														"Address": "PLC1.Q461.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "ESCAPEMENT.STOPPER4.RAISE",
														"Address": "PLC1.Q461.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 26,
														"StateName": "ESCAPEMENT.STOPPER4.MOVING.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER4.LOWER && ESCAPEMENT.STOPPER4.RAISED && ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 25,
														"StateName": "ESCAPEMENT.STOPPER4.MOVING.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER4.LOWER && ESCAPEMENT.STOPPER4.RAISED && ! ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 24,
														"StateName": "ESCAPEMENT.STOPPER4.MOVING.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER4.RAISE && ! ESCAPEMENT.STOPPER4.RAISED && ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 23,
														"StateName": "ESCAPEMENT.STOPPER4.MOVING.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER4.RAISE && ! ESCAPEMENT.STOPPER4.RAISED && ! ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 22,
														"StateName": "ESCAPEMENT.STOPPER4.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER4.LOWER && !  ESCAPEMENT.STOPPER4.RAISED && ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 21,
														"StateName": "ESCAPEMENT.STOPPER4.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPEMENT.STOPPER4.RAISE && ESCAPEMENT.STOPPER4.RAISED &&  ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 20,
														"StateName": "ESCAPEMENT.STOPPER4.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER4.LOWER && ! ESCAPEMENT.STOPPER4.RAISED && !  ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 19,
														"StateName": "ESCAPEMENT.STOPPER4.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPEMENT.STOPPER4.RAISE && ESCAPEMENT.STOPPER4.RAISED && ! ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapement.Stopper4",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "ESCAPEMENT.STOPPER4.LOWER",
														"Address": "PLC1.Q461.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "ESCAPEMENT.STOPPER4.RAISE",
														"Address": "PLC1.Q461.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "ESCAPEMENT.STOPPER4.PART.PASS",
														"Address": "PLC1.I418.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "ESCAPEMENT.STOPPER4.PART.PRESENT",
														"Address": "PLC1.I416.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "ESCAPEMENT.STOPPER4.RAISED",
														"Address": "PLC1.I416.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 8
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 9,
										"nodeid": 9,
										"Name": "Loading.Lane3",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "LOADING.LANE3.MC.NUMBER",
												"Address": "PLC1.DB820.44,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "LOADING.LANE3.PART.STATUS",
												"Address": "PLC1.DB820.42,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "LOADING.LANE3.PART.TYPE",
												"Address": "PLC1.DB820.40,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 44,
												"nodeid": 44,
												"Name": "Stopper5",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "STOPPER5.BUFFER.FULL",
														"Address": "PLC1.I417.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "STOPPER5.RAISED",
														"Address": "PLC1.I417.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "STOPPER5.LOWER",
														"Address": "PLC1.Q462.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "STOPPER5.RAISE",
														"Address": "PLC1.Q462.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 31,
														"StateName": "STOPPER5.BUFFER.FULL",
														"Equation": "STOPPER5.BUFFER.FULL",
														"Priority": 0,
														"subassemblyName": "Stopper5",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 30,
														"StateName": "STOPPER5.MOVING.DOWN",
														"Equation": "STOPPER5.LOWER && STOPPER5.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper5",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 29,
														"StateName": "STOPPER5.MOVING.UP",
														"Equation": "STOPPER5.RAISE && !STOPPER5.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper5",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 28,
														"StateName": "STOPPER5.AT.DOWN",
														"Equation": "STOPPER5.LOWER && !STOPPER5.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper5",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 27,
														"StateName": "STOPPER5.AT.UP",
														"Equation": "STOPPER5.RAISE && STOPPER5.RAISED",
														"Priority": 0,
														"subassemblyName": "Stopper5",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "STOPPER5.LOWER",
														"Address": "PLC1.Q462.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "STOPPER5.RAISE",
														"Address": "PLC1.Q462.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "STOPPER5.BUFFER.FULL",
														"Address": "PLC1.I417.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "STOPPER5.RAISED",
														"Address": "PLC1.I417.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 9
											},
											{
												"id": 45,
												"nodeid": 45,
												"Name": "Escapment.Stopper6",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "ESCAPMENT.STOPPER6.PART.PASS",
														"Address": "PLC1.I418.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "ESCAPMENT.STOPPER6.PART.PRESENT",
														"Address": "PLC1.I417.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													},
													{
														"Id": 5,
														"Name": "ESCAPMENT.STOPPER6.RAISED",
														"Address": "PLC1.I417.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 5
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "ESCAPMENT.STOPPER6.LOWER",
														"Address": "PLC1.Q462.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "ESCAPMENT.STOPPER6.RAISE",
														"Address": "PLC1.Q462.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 39,
														"StateName": "ESCAPMENT.STOPPER6.MOVING.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPMENT.STOPPER6.LOWER && ESCAPMENT.STOPPER6.RAISED && ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 38,
														"StateName": "ESCAPMENT.STOPPER6.MOVING.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPMENT.STOPPER6.LOWER && ESCAPMENT.STOPPER6.RAISED && ! ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 37,
														"StateName": "ESCAPMENT.STOPPER6.MOVING.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPMENT.STOPPER6.RAISE && ! ESCAPMENT.STOPPER6.RAISED && ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 36,
														"StateName": "ESCAPMENT.STOPPER6.MOVING.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPMENT.STOPPER6.RAISE && ! ESCAPMENT.STOPPER6.RAISED && ! ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 35,
														"StateName": "ESCAPMENT.STOPPER6.DOWN.WITH.PART.ABSENT",
														"Equation": "ESCAPMENT.STOPPER6.LOWER && !  ESCAPMENT.STOPPER6.RAISED && ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 34,
														"StateName": "ESCAPMENT.STOPPER6.UP.WITH.PART.ABSENT",
														"Equation": "ESCAPMENT.STOPPER6.RAISE && ESCAPMENT.STOPPER6.RAISED &&  ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 33,
														"StateName": "ESCAPMENT.STOPPER6.DOWN.WITH.PART.PRESENT",
														"Equation": "ESCAPMENT.STOPPER6.LOWER && ! ESCAPMENT.STOPPER6.RAISED && !  ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 32,
														"StateName": "ESCAPMENT.STOPPER6.UP.WITH.PART.PRESENT",
														"Equation": "ESCAPMENT.STOPPER6.RAISE && ESCAPMENT.STOPPER6.RAISED && ! ESCAPMENT.STOPPER6.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Escapment.Stopper6",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "ESCAPMENT.STOPPER6.LOWER",
														"Address": "PLC1.Q462.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "ESCAPMENT.STOPPER6.RAISE",
														"Address": "PLC1.Q462.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "ESCAPMENT.STOPPER6.PART.PASS",
														"Address": "PLC1.I418.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "ESCAPMENT.STOPPER6.PART.PRESENT",
														"Address": "PLC1.I417.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "ESCAPMENT.STOPPER6.RAISED",
														"Address": "PLC1.I417.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 9
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 10,
										"nodeid": 10,
										"Name": "Vision.Infeed",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "VISION.INFEED.MC.NUMBER",
												"Address": "PLC1.DB820.64,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "VISION.INFEED.PART.STATUS",
												"Address": "PLC1.DB820.62,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "VISION.INFEED.PART.TYPE",
												"Address": "PLC1.DB820.60,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 46,
												"nodeid": 46,
												"Name": "Vision.Pre.Stopper",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.PRE.STOPPER.PART.PRESENT",
														"Address": "PLC1.I517.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.PRE.STOPPER.RAISED",
														"Address": "PLC1.I517.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.PRE.STOPPER.LOWER",
														"Address": "PLC1.Q560.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.PRE.STOPPER.RAISE",
														"Address": "PLC1.Q560.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 44,
														"StateName": "VISION.PRE.STOPPER.PART.PRESENT",
														"Equation": "VISION.PRE.STOPPER.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Vision.Pre.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 43,
														"StateName": "VISION.PRE.STOPPER.MOVING.DOWN",
														"Equation": "VISION.PRE.STOPPER.LOWER && VISION.PRE.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Pre.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 42,
														"StateName": "VISION.PRE.STOPPER.MOVING.UP",
														"Equation": "VISION.PRE.STOPPER.RAISE && !VISION.PRE.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Pre.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 41,
														"StateName": "VISION.PRE.STOPPER.AT.DOWN",
														"Equation": "VISION.PRE.STOPPER.LOWER && !VISION.PRE.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Pre.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 40,
														"StateName": "VISION.PRE.STOPPER.AT.UP",
														"Equation": "VISION.PRE.STOPPER.RAISE && VISION.PRE.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Pre.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.PRE.STOPPER.LOWER",
														"Address": "PLC1.Q560.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.PRE.STOPPER.RAISE",
														"Address": "PLC1.Q560.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.PRE.STOPPER.PART.PRESENT",
														"Address": "PLC1.I517.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.PRE.STOPPER.RAISED",
														"Address": "PLC1.I517.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 10
											},
											{
												"id": 51,
												"nodeid": 51,
												"Name": "Vision.Post.Stopper",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.POST.STOPPER.PART.PRESENT",
														"Address": "PLC1.I517.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.POST.STOPPER.RAISED",
														"Address": "PLC1.I517.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.POST.STOPPER.LOWER",
														"Address": "PLC1.Q561.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.POST.STOPPER.RAISE",
														"Address": "PLC1.Q561.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 66,
														"StateName": "VISION.POST.STOPPER.PART.PRESENT",
														"Equation": "VISION.POST.STOPPER.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Vision.Post.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 65,
														"StateName": "VISION.POST.STOPPER.MOVING.DOWN",
														"Equation": "VISION.POST.STOPPER.LOWER && VISION.POST.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Post.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 64,
														"StateName": "VISION.POST.STOPPER.MOVING.UP",
														"Equation": "VISION.POST.STOPPER.RAISE && !VISION.POST.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Post.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 63,
														"StateName": "VISION.POST.STOPPER.AT.DOWN",
														"Equation": "VISION.POST.STOPPER.LOWER && !VISION.POST.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Post.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 62,
														"StateName": "VISION.POST.STOPPER.AT.UP",
														"Equation": "VISION.POST.STOPPER.RAISE && VISION.POST.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Post.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.POST.STOPPER.LOWER",
														"Address": "PLC1.Q561.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.POST.STOPPER.RAISE",
														"Address": "PLC1.Q561.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.POST.STOPPER.PART.PRESENT",
														"Address": "PLC1.I517.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.POST.STOPPER.RAISED",
														"Address": "PLC1.I517.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 10
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 11,
										"nodeid": 11,
										"Name": "Vision.Station",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "VISION.STATION.MC.NUMBER",
												"Address": "PLC1.DB820.84,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "VISION.STATION.PART.STATUS",
												"Address": "PLC1.DB820.82,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "VISION.STATION.PART.TYPE",
												"Address": "PLC1.DB820.80,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 47,
												"nodeid": 47,
												"Name": "Vision.Station.Lifter",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.STATION.LIFTER.LOWERED",
														"Address": "PLC1.I515.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.LIFTER.RAISED",
														"Address": "PLC1.I515.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.LIFTER.LOWER",
														"Address": "PLC1.Q560.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.LIFTER.RAISE",
														"Address": "PLC1.Q560.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 49,
														"StateName": "VISION.STATION.LIFTER.OUTPUTS.OFF",
														"Equation": "! VISION.STATION.LIFTER.LOWER && ! VISION.STATION.LIFTER.RAISE && ! VISION.STATION.LIFTER.LOWERED && ! VISION.STATION.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 48,
														"StateName": "VISION.STATION.LIFTER.MOVING.DOWN",
														"Equation": "VISION.STATION.LIFTER.LOWER && ! VISION.STATION.LIFTER.RAISE &&  ! VISION.STATION.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 47,
														"StateName": "VISION.STATION.LIFTER.MOVING.UP",
														"Equation": "VISION.STATION.LIFTER.RAISE && ! VISION.STATION.LIFTER.LOWER && ! VISION.STATION.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 46,
														"StateName": "VISION.STATION.LIFTER.AT.DOWN.POSITION",
														"Equation": "VISION.STATION.LIFTER.LOWER && ! VISION.STATION.LIFTER.RAISE && VISION.STATION.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 45,
														"StateName": "VISION.STATION.LIFTER.AT.UP.POSITION",
														"Equation": "VISION.STATION.LIFTER.RAISE && ! VISION.STATION.LIFTER.LOWER && VISION.STATION.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.LIFTER.LOWER",
														"Address": "PLC1.Q560.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.LIFTER.RAISE",
														"Address": "PLC1.Q560.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.STATION.LIFTER.LOWERED",
														"Address": "PLC1.I515.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.LIFTER.RAISED",
														"Address": "PLC1.I515.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 11
											},
											{
												"id": 48,
												"nodeid": 48,
												"Name": "Vision.System.Part.Centering.Slide",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC1.I516.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC1.I516.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC1.Q561.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC1.Q561.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 53,
														"StateName": "VISION.SYSTEM.PART.CENTERING.SLIDE.AT.REVERSE",
														"Equation": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSED && ! VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARDED && ( VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Vision.System.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 52,
														"StateName": "VISION.SYSTEM.PART.CENTERING.SLIDE.MOVING.REVERSE",
														"Equation": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSE && ! VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSED",
														"Priority": 0,
														"subassemblyName": "Vision.System.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 51,
														"StateName": "VISION.SYSTEM.PART.CENTERING.SLIDE.AT.FORWARD",
														"Equation": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARDED && ! VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSE && ( VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Vision.System.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 50,
														"StateName": "VISION.SYSTEM.PART.CENTERING.SLIDE.MOVING.FORWARD",
														"Equation": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARD && ! VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Vision.System.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC1.Q561.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC1.Q561.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC1.I516.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.SYSTEM.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC1.I516.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 11
											},
											{
												"id": 49,
												"nodeid": 49,
												"Name": "Vision.Station.Butting.Stopper",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.STATION.BUTTING.STOPPER.REVERSED",
														"Address": "PLC1.I516.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.BUTTING.STOPPER.FORWARDED",
														"Address": "PLC1.I516.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.BUTTING.STOPPER.REVERSE",
														"Address": "PLC1.Q561.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.BUTTING.STOPPER.FORWARD",
														"Address": "PLC1.Q561.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 57,
														"StateName": "VISION.STATION.BUTTING.STOPPER.AT.REVERSE",
														"Equation": "VISION.STATION.BUTTING.STOPPER.REVERSED && ! VISION.STATION.BUTTING.STOPPER.FORWARDED && ( VISION.STATION.BUTTING.STOPPER.REVERSE || ! VISION.STATION.BUTTING.STOPPER.REVERSE)",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 56,
														"StateName": "VISION.STATION.BUTTING.STOPPER.MOVING.REVERSE",
														"Equation": "VISION.STATION.BUTTING.STOPPER.REVERSE && ! VISION.STATION.BUTTING.STOPPER.REVERSED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 55,
														"StateName": "VISION.STATION.BUTTING.STOPPER.AT.FORWARD",
														"Equation": "VISION.STATION.BUTTING.STOPPER.FORWARDED && ! VISION.STATION.BUTTING.STOPPER.REVERSE && ( VISION.STATION.BUTTING.STOPPER.FORWARD || !VISION.STATION.BUTTING.STOPPER.FORWARD)",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 54,
														"StateName": "VISION.STATION.BUTTING.STOPPER.MOVING.FORWARD",
														"Equation": "VISION.STATION.BUTTING.STOPPER.FORWARD && ! VISION.STATION.BUTTING.STOPPER.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.STOPPER.REVERSE",
														"Address": "PLC1.Q561.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.STOPPER.FORWARD",
														"Address": "PLC1.Q561.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.STATION.STOPPER.FORWARDED",
														"Address": "PLC1.I516.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.STOPPER.REVERSED",
														"Address": "PLC1.I516.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 11
											},
											{
												"id": 50,
												"nodeid": 50,
												"Name": "Vision.Station.Pusher",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "VISION.STATION.PUSHER.REVERSED",
														"Address": "PLC1.I516.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.PUSHER.FORWARDED",
														"Address": "PLC1.I516.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.PUSHER.REVERSE",
														"Address": "PLC1.Q561.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.PUSHER.FORWARD",
														"Address": "PLC1.Q561.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 61,
														"StateName": "VISION.STATION.PUSHER.AT.REVERSE",
														"Equation": "VISION.STATION.PUSHER.REVERSED && ! VISION.STATION.PUSHER.FORWARDED && ( VISION.STATION.PUSHER.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Pusher",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 60,
														"StateName": "VISION.STATION.PUSHER.MOVING.REVERSE",
														"Equation": "VISION.STATION.PUSHER.REVERSE && ! VISION.STATION.PUSHER.REVERSED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Pusher",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 59,
														"StateName": "VISION.STATION.PUSHER.AT.FORWARD",
														"Equation": "VISION.STATION.PUSHER.FORWARDED && ! VISION.STATION.PUSHER.REVERSE && ( VISION.STATION.PUSHER.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Pusher",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 58,
														"StateName": "VISION.STATION.PUSHER.MOVING.FORWARD",
														"Equation": "VISION.STATION.PUSHER.FORWARD && ! VISION.STATION.PUSHER.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Vision.Station.Pusher",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "VISION.STATION.PUSHER.REVERSE",
														"Address": "PLC1.Q561.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "VISION.STATION.PUSHER.FORWARD",
														"Address": "PLC1.Q561.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "VISION.STATION.PUSHER.FORWARDED",
														"Address": "PLC1.I516.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "VISION.STATION.PUSHER.REVERSED",
														"Address": "PLC1.I516.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 11
											},
											{
												"id": 80,
												"nodeid": 80,
												"Name": "Vision.Station.Camera",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 6,
														"Name": "TOP.CAMERA.RESULT.NOK",
														"Address": "PLC2.DB1136.83,X4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "TOP.CAMERA.RESULT.OK",
														"Address": "PLC1.DB1136.83,X3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "SIDE.CAMERA.RESULT.NOK",
														"Address": "PLC2.DB1135.83,X4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "SIDE.CAMERA.RESULT.OK",
														"Address": "PLC1.DB1135.83,X3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "SIDE.CAMERA.RESULT",
														"Address": "PLC1.DB1377.550,I",
														"InputType": "Sequence",
														"DataType": "INT",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "TOP.CAMERA.RESULT",
														"Address": "PLC1.DB1377.570,I",
														"InputType": "Sequence",
														"DataType": "INT",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [
													{
														"Id": 2,
														"Name": "SIDE.CAMERA.COMMAND",
														"Address": "PLC1.DB1030.0,X3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"type": "Output"
													},
													{
														"Id": 1,
														"Name": "TOP.CAMERA.COMMAND",
														"Address": "PLC1.DB1031.0,X3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"type": "Output"
													}
												],
												"States": [],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 11
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 12,
										"nodeid": 12,
										"Name": "Gantry.Pickup.Station",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "GANTRY.PICKUP.STATION.MC.NUMBER",
												"Address": "PLC1.DB820.104,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY.PICKUP.STATION.PART.STATUS",
												"Address": "PLC1.DB820.102,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY.PICKUP.LIFTER.PART.TYPE",
												"Address": "PLC1.DB820.100,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 52,
												"nodeid": 52,
												"Name": "Gantry.Pickup.Lifter",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.LIFTER.LOWERED",
														"Address": "PLC1.I518.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.LIFTER.RAISED",
														"Address": "PLC1.I518.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.LIFTER.LOWER",
														"Address": "PLC1.Q562.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.LIFTER.RAISE",
														"Address": "PLC1.Q562.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 71,
														"StateName": "GANTRY.PICKUP.LIFTER.OUTPUTS.OFF",
														"Equation": "! GANTRY.PICKUP.LIFTER.LOWER && ! GANTRY.PICKUP.LIFTER.RAISE && ! GANTRY.PICKUP.LIFTER.LOWERED && ! GANTRY.PICKUP.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 70,
														"StateName": "GANTRY.PICKUP.LIFTER.MOVING.DOWN",
														"Equation": "GANTRY.PICKUP.LIFTER.LOWER && ! GANTRY.PICKUP.LIFTER.RAISE &&  ! GANTRY.PICKUP.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 69,
														"StateName": "GANTRY.PICKUP.LIFTER.MOVING.UP",
														"Equation": "GANTRY.PICKUP.LIFTER.RAISE && ! GANTRY.PICKUP.LIFTER.LOWER && ! GANTRY.PICKUP.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 68,
														"StateName": "GANTRY.PICKUP.LIFTER.AT.DOWN.POSITION",
														"Equation": "GANTRY.PICKUP.LIFTER.LOWER && ! GANTRY.PICKUP.LIFTER.RAISE && GANTRY.PICKUP.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 67,
														"StateName": "GANTRY.PICKUP.LIFTER.AT.UP.POSITION",
														"Equation": "GANTRY.PICKUP.LIFTER.RAISE && ! GANTRY.PICKUP.LIFTER.LOWER && GANTRY.PICKUP.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.LIFTER.LOWER",
														"Address": "PLC1.Q562.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.LIFTER.RAISE",
														"Address": "PLC1.Q562.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.LIFTER.LOWERED",
														"Address": "PLC1.I518.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.LIFTER.RAISED",
														"Address": "PLC1.I518.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											},
											{
												"id": 53,
												"nodeid": 53,
												"Name": "Gantry.Pickup.Station.Part.Centering.Slide",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC1.I518.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC1.I518.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC1.Q562.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC1.Q562.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 75,
														"StateName": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.AT.REVERSE",
														"Equation": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSED && ! GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARDED && ( GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Station.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 74,
														"StateName": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.MOVING.REVERSE",
														"Equation": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSE && ! GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Station.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 73,
														"StateName": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.AT.FORWARD",
														"Equation": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARDED && ! GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSE && ( GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Station.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 72,
														"StateName": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.MOVING.FORWARD",
														"Equation": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARD && ! GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Station.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC1.Q562.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC1.Q562.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC1.I518.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.STATION.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC1.I518.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											},
											{
												"id": 54,
												"nodeid": 54,
												"Name": "Gantry.Pickup.Ref.Slide1",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.FORWARDED",
														"Address": "PLC1.I519.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.REVERSED",
														"Address": "PLC1.I519.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.REVERSE",
														"Address": "PLC1.Q562.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.FORWARD",
														"Address": "PLC1.Q562.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 79,
														"StateName": "GANTRY.PICKUP.REF.SLIDE1.AT.REVERSE",
														"Equation": "GANTRY.PICKUP.REF.SLIDE1.REVERSED && ! GANTRY.PICKUP.REF.SLIDE1.FORWARDED && ( GANTRY.PICKUP.REF.SLIDE1.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 78,
														"StateName": "GANTRY.PICKUP.REF.SLIDE1.MOVING.REVERSE",
														"Equation": "GANTRY.PICKUP.REF.SLIDE1.REVERSE && ! GANTRY.PICKUP.REF.SLIDE1.REVERSED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 77,
														"StateName": "GANTRY.PICKUP.REF.SLIDE1.AT.FORWARD",
														"Equation": "GANTRY.PICKUP.REF.SLIDE1.FORWARDED && ! GANTRY.PICKUP.REF.SLIDE1.REVERSE && ( GANTRY.PICKUP.REF.SLIDE1.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 76,
														"StateName": "GANTRY.PICKUP.REF.SLIDE1.MOVING.FORWARD",
														"Equation": "GANTRY.PICKUP.REF.SLIDE1.FORWARD && ! GANTRY.PICKUP.REF.SLIDE1.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.REVERSE",
														"Address": "PLC1.Q562.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.FORWARD",
														"Address": "PLC1.Q562.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.FORWARDED",
														"Address": "PLC1.I519.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.REF.SLIDE1.REVERSED",
														"Address": "PLC1.I519.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											},
											{
												"id": 55,
												"nodeid": 55,
												"Name": "Gantry.Pickup.Ref.Slide2",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.FORWARDED",
														"Address": "PLC1.I519.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.REVERSED",
														"Address": "PLC1.I519.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.REVERSE",
														"Address": "PLC1.Q562.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.FORWARD",
														"Address": "PLC1.Q562.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 83,
														"StateName": "GANTRY.PICKUP.REF.SLIDE2.AT.REVERSE",
														"Equation": "GANTRY.PICKUP.REF.SLIDE2.REVERSED && ! GANTRY.PICKUP.REF.SLIDE2.FORWARDED && ( GANTRY.PICKUP.REF.SLIDE2.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 82,
														"StateName": "GANTRY.PICKUP.REF.SLIDE2.MOVING.REVERSE",
														"Equation": "GANTRY.PICKUP.REF.SLIDE2.REVERSE && ! GANTRY.PICKUP.REF.SLIDE2.REVERSED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 81,
														"StateName": "GANTRY.PICKUP.REF.SLIDE2.AT.FORWARD",
														"Equation": "GANTRY.PICKUP.REF.SLIDE2.FORWARDED && ! GANTRY.PICKUP.REF.SLIDE2.REVERSE && ( GANTRY.PICKUP.REF.SLIDE2.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 80,
														"StateName": "GANTRY.PICKUP.REF.SLIDE2.MOVING.FORWARD",
														"Equation": "GANTRY.PICKUP.REF.SLIDE2.FORWARD && ! GANTRY.PICKUP.REF.SLIDE2.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Ref.Slide2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.REVERSE",
														"Address": "PLC1.Q562.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.FORWARD",
														"Address": "PLC1.Q562.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.FORWARDED",
														"Address": "PLC1.I519.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.REF.SLIDE2.REVERSED",
														"Address": "PLC1.I519.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											},
											{
												"id": 56,
												"nodeid": 56,
												"Name": "Gantry.Pickup.Part.Clamp.Assembly1",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARDED",
														"Address": "PLC1.I519.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSED",
														"Address": "PLC1.I519.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSE",
														"Address": "PLC1.Q563.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARD",
														"Address": "PLC1.Q563.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 87,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.AT.REVERSE",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSED && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARDED && ( GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 86,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.MOVING.REVERSE",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSE && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 85,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.AT.FORWARD",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARDED && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSE && ( GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 84,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.MOVING.FORWARD",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARD && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSE",
														"Address": "PLC1.Q563.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARD",
														"Address": "PLC1.Q563.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.FORWARDED",
														"Address": "PLC1.I519.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY1.REVERSED",
														"Address": "PLC1.I519.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											},
											{
												"id": 57,
												"nodeid": 57,
												"Name": "Gantry.Pickup.Part.Clamp.Assembly2",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARDED",
														"Address": "PLC1.I519.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSED",
														"Address": "PLC1.I519.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSE",
														"Address": "PLC1.Q563.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARD",
														"Address": "PLC1.Q563.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 91,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.AT.REVERSE",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSED && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARDED && ( GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 90,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.MOVING.REVERSE",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSE && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 89,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.AT.FORWARD",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARDED && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSE && ( GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 88,
														"StateName": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.MOVING.FORWARD",
														"Equation": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARD && ! GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Gantry.Pickup.Part.Clamp.Assembly2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSE",
														"Address": "PLC1.Q563.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARD",
														"Address": "PLC1.Q563.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.FORWARDED",
														"Address": "PLC1.I519.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.PICKUP.PART.CLAMP.ASSEMBLY2.REVERSED",
														"Address": "PLC1.I519.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 12
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									},
									{
										"id": 87,
										"nodeid": 87,
										"Name": "Infeed.Reject.Conveyor",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "INFEED.REJECT.CONV.MC.NUMBER",
												"Address": "PLC1.DB820.124,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "INFEED.REJECT.CONV.PART.STATUS",
												"Address": "PLC1.DB820.122,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "INFEED.REJECT.CONV.PART.TYPE",
												"Address": "PLC1.DB820.120,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 88,
												"nodeid": 88,
												"Name": "Reject.Conveyor",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 2,
														"Name": "IF.REJECT.CONVEYOR.PART.PRESENT",
														"Address": "PLC1.I517.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "IF.REJECT.CONVEYOR.BUFFER.FULL",
														"Address": "PLC1.I517.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 280,
														"StateName": "INFEED.REJECT.CONV.BUFFER.NOT.FULL",
														"Equation": "! IF.REJECT.CONVEYOR.BUFFER.FULL",
														"Priority": 0,
														"subassemblyName": "Infeed.Reject.Conveyor",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 279,
														"StateName": "INFEED.REJECT.CONV.BUFFER.FULL",
														"Equation": "IF.REJECT.CONVEYOR.BUFFER.FULL",
														"Priority": 0,
														"subassemblyName": "Infeed.Reject.Conveyor",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 87
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 1
									}
								],
								"EEA": [
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"IsLogging": false,
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Address": "",
										"IsLogging": true,
										"Type": "Shift"
									},
									{
										"_id": "5b2b7856e4b0ca8345971e12",
										"eeId": 3,
										"Name": "Test Articats",
										"Unit": "DATETIME",
										"Min": "1529535600000",
										"Max": "1529539200000",
										"Type": "Quality"
									},
									{
										"_id": "5b2a151ce4b07b59adf08d18",
										"eeId": 2,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529465400475",
										"Max": "1529487000475",
										"Type": "Shift"
									},
									{
										"_id": "5b29eb1ee4b0d4392184f91a",
										"eeId": 1,
										"Name": "First Shift Break 1",
										"Unit": "DATETIME",
										"Min": "1529476200258",
										"Max": "1529478000258",
										"Type": "Breakdown"
									}
								],
								"coupling": []
							},
							{
								"id": 4,
								"nodeid": 4,
								"Name": "Gantry1",
								"field": "Asset",
								"coupleType": "Coupled",
								"isvirtualSubAssembly": false,
								"EquationStateNumbers": "",
								"Inputs": [
									{
										"Id": 6,
										"Name": "GANTRY1.GRIPPER2.MC.NUMBER",
										"Address": "PLC2.DB820.204,B",
										"InputType": "Quality",
										"DataType": "BYTE",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 5,
										"Name": "GANTRY1.GRIPPER2.PART.STATUS",
										"Address": "PLC2.DB820.200,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 4,
										"Name": "GANTRY1.GRIPPER2.PART.TYPE",
										"Address": "PLC2.DB820.202,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 3,
										"Name": "GANTRY1.GRIPPER1.MC.NUMBER",
										"Address": "PLC2.DB820.134,B",
										"InputType": "Quality",
										"DataType": "BYTE",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 2,
										"Name": "GANTRY1.GRIPPER1.PART.TYPE",
										"Address": "PLC2.DB820.132,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 1,
										"Name": "GANTRY1.GRIPPER1.PART.STATUS",
										"Address": "PLC2.DB820.130,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									}
								],
								"Outputs": [],
								"States": [],
								"nodes": [
									{
										"id": 14,
										"nodeid": 14,
										"Name": "Gantry1.X1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 91,
												"nodeid": 91,
												"Name": "Gantry1.X1.Servo",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.X1.SERVOMOVINGFWD",
														"Address": "PLC2.DB31.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY1.X1.SERVOMOVINGREV",
														"Address": "PLC2.DB31.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 2
													},
													{
														"Id": 3,
														"Name": "GANTRY1.X1.SERVOPOSITION",
														"Address": "PLC2.DB670.12,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 3
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 412,
														"StateName": "GANTRY1.X1.SERVO NEAR NEST3",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13696) && (GANTRY1.X1.SERVOPOSITION <= 13706) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 411,
														"StateName": "GANTRY1.X1.SERVO BETWEEN NEST3 TO NEST2",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13534) && (GANTRY1.X1.SERVOPOSITION <= 13696) && GANTRY1.X1.SERVOMOVINGREV && !GANTRY1.X1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 410,
														"StateName": "GANTRY1.X1.SERVO AT NEST3",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 13696.62) && (GANTRY1.X1.SERVOPOSITION <= 13706.62) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 409,
														"StateName": "GANTRY1.X1.SERVO NEAR NEST2",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13524) && (GANTRY1.X1.SERVOPOSITION <= 13534) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 408,
														"StateName": "GANTRY1.X1.SERVO BETWEEN NEST2 TO NEST1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13237) && (GANTRY1.X1.SERVOPOSITION <= 13524) && GANTRY1.X1.SERVOMOVINGREV && !GANTRY1.X1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 407,
														"StateName": "GANTRY1.X1.SERVO BETWEEN NEST2 TO NEST3",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13534) && (GANTRY1.X1.SERVOPOSITION <= 13696) && GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 406,
														"StateName": "GANTRY1.X1.SERVO AT NEST2",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 13524.57) && (GANTRY1.X1.SERVOPOSITION <= 13534.57) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 405,
														"StateName": "GANTRY1.X1.SERVO NEAR NEST1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13227) && (GANTRY1.X1.SERVOPOSITION <= 13237) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 404,
														"StateName": "GANTRY1.X1.SERVO BETWEEN NEST1 TO COMMON.ZONE",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 8005) && (GANTRY1.X1.SERVOPOSITION <= 13227) && GANTRY1.X1.SERVOMOVINGREV && !GANTRY1.X1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 403,
														"StateName": "GANTRY1.X1.SERVO BETWEEN NEST1 TO NEST2",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 13237) && (GANTRY1.X1.SERVOPOSITION <= 13524) && GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 402,
														"StateName": "GANTRY1.X1.SERVO AT NEST1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 13227.36) && (GANTRY1.X1.SERVOPOSITION <= 13237.36) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 401,
														"StateName": "GANTRY1.X1.SERVO NEAR COMMON.ZONE",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 7995) && (GANTRY1.X1.SERVOPOSITION <= 8005) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 400,
														"StateName": "GANTRY1.X1.SERVO BETWEEN COMMON.ZONE TO MACHINE1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 4818) && (GANTRY1.X1.SERVOPOSITION <= 7995) && GANTRY1.X1.SERVOMOVINGREV && !GANTRY1.X1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 399,
														"StateName": "GANTRY1.X1.SERVO BETWEEN COMMON.ZONE TO NEST1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 8005) && (GANTRY1.X1.SERVOPOSITION <= 13227) && GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 398,
														"StateName": "GANTRY1.X1.SERVO AT COMMON.ZONE",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 7995) && (GANTRY1.X1.SERVOPOSITION <= 8005) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 397,
														"StateName": "GANTRY1.X1.SERVO NEAR MACHINE1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 4808) && (GANTRY1.X1.SERVOPOSITION <= 4818) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 396,
														"StateName": "GANTRY1.X1.SERVO BETWEEN MACHINE1 TO INFEED.CONVEYOR",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 289) && (GANTRY1.X1.SERVOPOSITION <= 4808) && GANTRY1.X1.SERVOMOVINGREV && !GANTRY1.X1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 395,
														"StateName": "GANTRY1.X1.SERVO BETWEEN MACHINE1 TO COMMON.ZONE",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 4818) && (GANTRY1.X1.SERVOPOSITION <= 7995) && GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 394,
														"StateName": "GANTRY1.X1.SERVO AT MACHINE1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 4808.549) && (GANTRY1.X1.SERVOPOSITION <= 4818.549) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 393,
														"StateName": "GANTRY1.X1.SERVO NEAR INFEED.CONVEYOR",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 279) && (GANTRY1.X1.SERVOPOSITION <= 289) && (GANTRY1.X1.SERVOMOVINGFWD || GANTRY1.X1.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 392,
														"StateName": "GANTRY1.X1.SERVO BETWEEN INFEED.CONVEYOR TO MACHINE1",
														"Equation": "(GANTRY1.X1.SERVOPOSITION > 289) && (GANTRY1.X1.SERVOPOSITION <= 4808) && GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 391,
														"StateName": "GANTRY1.X1.SERVO AT INFEED.CONVEYOR",
														"Equation": "(GANTRY1.X1.SERVOPOSITION >= 279.475) && (GANTRY1.X1.SERVOPOSITION <= 289.475) && !GANTRY1.X1.SERVOMOVINGFWD && !GANTRY1.X1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.X1.SERVOMOVINGFWD",
														"Address": "PLC2.DB31.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 2,
														"Name": "GANTRY1.X1.SERVOMOVINGREV",
														"Address": "PLC2.DB31.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 3,
														"Name": "GANTRY1.X1.SERVOPOSITION",
														"Address": "PLC2.DB670.12,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"isCompulsory": true
													}
												],
												"positions": [
													{
														"id": 0,
														"Name": "INFEED.CONVEYOR",
														"Position": 284.475
													},
													{
														"id": 1,
														"Name": "MACHINE1",
														"Position": 4813.549
													},
													{
														"id": 5,
														"Name": "COMMON.ZONE",
														"Position": 8000
													},
													{
														"id": 2,
														"Name": "NEST1",
														"Position": 13232.36
													},
													{
														"id": 3,
														"Name": "NEST2",
														"Position": 13529.57
													},
													{
														"id": 4,
														"Name": "NEST3",
														"Position": 13701.62
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "GANTRY1.X1.SERVOPOSITION",
													"sMovingPlusSelected": "GANTRY1.X1.SERVOMOVINGFWD",
													"sMovingMinusSelected": "GANTRY1.X1.SERVOMOVINGREV"
												},
												"prevStateOption": "Axis",
												"line": 1,
												"asset": 14
											},
											{
												"id": 93,
												"nodeid": 93,
												"Name": "Gantry1.X1.Application.IO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.X1.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I60.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY1.X1.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I60.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "GANTRY1.X.ANTICOLLISION.CH1",
														"Address": "PLC2.I60.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													},
													{
														"Id": 4,
														"Name": "GANTRY1.X.ANTICOLLISION.CH2",
														"Address": "PLC2.I60.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 415,
														"StateName": "GANTRY1.X1.WITHIN.SAFE.LIMITS",
														"Equation": "! GANTRY1.X1.POSITIVE.OVERTRAVEL && ! GANTRY1.X1.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Application.IO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 414,
														"StateName": "GANTRY1.X1.POSITIVE.OVERTRAVEL",
														"Equation": "GANTRY1.X1.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Application.IO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 413,
														"StateName": "GANTRY1.X1.NEGATIVE.OVERTRAVEL",
														"Equation": "GANTRY1.X1.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.X1.Application.IO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.APPLICATION.IO",
														"Address": "PLC2.I60.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY1.APPLICATIONIO.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I60.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY1.APPLICATIONIO.ANTICOLLISION.CH1",
														"Address": "PLC2.I60.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY1.APPLICATIONIO.ANTICOLLISION.CH2",
														"Address": "PLC2.I60.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 14
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4
									},
									{
										"id": 15,
										"nodeid": 15,
										"Name": "Gantry1.Z1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 94,
												"nodeid": 94,
												"Name": "Gantry1.Z1.Servo",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY1.Z1.SAFE",
														"Address": "PLC2.I61.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY1.Z1.MOVING.MINUS",
														"Address": "PLC2.DB33.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "GANTRY1.Z1.MOVING.PLUS",
														"Address": "PLC2.DB33.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 443,
														"StateName": "GANTRY1.Z1.SERVO NEAR HOME",
														"Equation": "(GANTRY1.Z1.POSITION > -5) && (GANTRY1.Z1.POSITION <= 5) && (GANTRY1.Z1.MOVING.PLUS || GANTRY1.Z1.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 442,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN HOME TO POSITION1",
														"Equation": "(GANTRY1.Z1.POSITION > -165) && (GANTRY1.Z1.POSITION <= -5) && GANTRY1.Z1.MOVING.MINUS && !GANTRY1.Z1.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 441,
														"StateName": "GANTRY1.Z1.SERVO AT HOME",
														"Equation": "(GANTRY1.Z1.POSITION >= -5) && (GANTRY1.Z1.POSITION <= 5) && !GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 440,
														"StateName": "GANTRY1.Z1.SERVO NEAR POSITION1",
														"Equation": "(GANTRY1.Z1.POSITION > -175) && (GANTRY1.Z1.POSITION <= -165) && (GANTRY1.Z1.MOVING.PLUS || GANTRY1.Z1.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 439,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN POSITION1 TO POSITION2",
														"Equation": "(GANTRY1.Z1.POSITION > -1230) && (GANTRY1.Z1.POSITION <= -175) && GANTRY1.Z1.MOVING.MINUS && !GANTRY1.Z1.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 438,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN POSITION1 TO HOME",
														"Equation": "(GANTRY1.Z1.POSITION > -165) && (GANTRY1.Z1.POSITION <= -5) && GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 437,
														"StateName": "GANTRY1.Z1.SERVO AT POSITION1",
														"Equation": "(GANTRY1.Z1.POSITION >= -175) && (GANTRY1.Z1.POSITION <= -165) && !GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 436,
														"StateName": "GANTRY1.Z1.SERVO NEAR POSITION2",
														"Equation": "(GANTRY1.Z1.POSITION > -1240) && (GANTRY1.Z1.POSITION <= -1230) && (GANTRY1.Z1.MOVING.PLUS || GANTRY1.Z1.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 435,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN POSITION2 TO POSITION3",
														"Equation": "(GANTRY1.Z1.POSITION > -1271) && (GANTRY1.Z1.POSITION <= -1240) && GANTRY1.Z1.MOVING.MINUS && !GANTRY1.Z1.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 434,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN POSITION2 TO POSITION1",
														"Equation": "(GANTRY1.Z1.POSITION > -1230) && (GANTRY1.Z1.POSITION <= -175) && GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 433,
														"StateName": "GANTRY1.Z1.SERVO AT POSITION2",
														"Equation": "(GANTRY1.Z1.POSITION >= -1240) && (GANTRY1.Z1.POSITION <= -1230) && !GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 432,
														"StateName": "GANTRY1.Z1.SERVO NEAR POSITION3",
														"Equation": "(GANTRY1.Z1.POSITION > -1281) && (GANTRY1.Z1.POSITION <= -1271) && (GANTRY1.Z1.MOVING.PLUS || GANTRY1.Z1.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 431,
														"StateName": "GANTRY1.Z1.SERVO BETWEEN POSITION3 TO POSITION2",
														"Equation": "(GANTRY1.Z1.POSITION > -1271) && (GANTRY1.Z1.POSITION <= -1240) && GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 430,
														"StateName": "GANTRY1.Z1.SERVO AT POSITION3",
														"Equation": "(GANTRY1.Z1.POSITION >= -1281) && (GANTRY1.Z1.POSITION <= -1271) && !GANTRY1.Z1.MOVING.PLUS && !GANTRY1.Z1.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 15,
												"positions": [
													{
														"id": 3,
														"Name": "POSITION3",
														"Position": -1276
													},
													{
														"id": 2,
														"Name": "POSITION2",
														"Position": -1235
													},
													{
														"id": 1,
														"Name": "POSITION1",
														"Position": -170
													},
													{
														"id": 0,
														"Name": "HOME",
														"Position": 0
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "GANTRY1.Z1.POSITION",
													"sMovingPlusSelected": "GANTRY1.Z1.MOVING.PLUS",
													"sMovingMinusSelected": "GANTRY1.Z1.MOVING.MINUS"
												},
												"prevStateOption": "Axis"
											},
											{
												"id": 95,
												"nodeid": 95,
												"Name": "Gantry1.Z1.ApplicationIO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 2,
														"Name": "GANTRY1.Z1.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I61.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "GANTRY1.Z1.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I61.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 446,
														"StateName": "GANTRY1.Z1.IN.RANGE",
														"Equation": "! GANTRY1.Z1.POSITIVE.OVERTRAVEL &&  !GANTRY1.Z1.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 445,
														"StateName": "GANTRY1.Z1.NEGATIVE.OVERTRAVEL",
														"Equation": "GANTRY1.Z1.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 444,
														"StateName": "GANTRY1.Z1.POSITIVE.OVERTRAVEL",
														"Equation": "GANTRY1.Z1.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry1.Z1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 15
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4,
										"positions": [
											{
												"id": 3,
												"Name": "POSITION3",
												"Position": -1276
											},
											{
												"id": 2,
												"Name": "POSITION2",
												"Position": -1235
											},
											{
												"id": 1,
												"Name": "POSITION1",
												"Position": -170
											},
											{
												"id": 0,
												"Name": "HOME",
												"Position": 0
											}
										],
										"positionsSelection": {
											"sPostionsSelected": "GANTRY1.POSITION",
											"sMovingPlusSelected": "GANTRY1.Z1.MOVING.PLUS",
											"sMovingMinusSelected": "GANTRY1.MOVING.MINUS"
										},
										"prevStateOption": "Axis"
									},
									{
										"id": 16,
										"nodeid": 16,
										"Name": "Gantry1.Y1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 74,
												"nodeid": 74,
												"Name": "Gantry1.Y1.SERVO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.Y1.SERVOMOVINGFWD",
														"Address": "PLC2.DB32.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY1.Y1.SERVOMOVINGREV",
														"Address": "PLC2.DB32.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 2
													},
													{
														"Id": 3,
														"Name": "GANTRY1.Y1.SAFE",
														"Address": "PLC2.I67.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 3
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 206,
														"StateName": "GANTRY1.Y1.SERVO MOVING TO LOADING",
														"Equation": "(GANTRY1.Y1.SERVOPOSITION >= 0) && (GANTRY1.Y1.SERVOPOSITION < 229.13) && GANTRY1.Y1.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "GANTRY1.Y1.SERVO",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 205,
														"StateName": "GANTRY1.Y1.SERVO AT LOADING",
														"Equation": "(GANTRY1.Y1.SERVOPOSITION > 224.13) && (GANTRY1.Y1.SERVOPOSITION < 234.13) && !GANTRY1.Y1.SERVOMOVINGFWD && !GANTRY1.Y1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "GANTRY1.Y1.SERVO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 204,
														"StateName": "GANTRY1.Y1.SERVO MOVING TO HOME",
														"Equation": "(GANTRY1.Y1.SERVOPOSITION >= 0) && (GANTRY1.Y1.SERVOPOSITION <= 229.13) && GANTRY1.Y1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "GANTRY1.Y1.SERVO",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 203,
														"StateName": "GANTRY1.Y1.SERVO AT HOME",
														"Equation": "(GANTRY1.Y1.SERVOPOSITION > -5) && (GANTRY1.Y1.SERVOPOSITION < 5) && !GANTRY1.Y1.SERVOMOVINGFWD && !GANTRY1.Y1.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "GANTRY1.Y1.SERVO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY1.Y1.SERVOMOVINGFWD",
														"Address": "PLC2.DB32.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 2,
														"Name": "GANTRY1.Y1.SERVOMOVINGREV",
														"Address": "PLC2.DB32.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 3,
														"Name": "GANTRY1.Y1.SERVOPOSITION",
														"Address": "PLC2.DB670.52,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"isCompulsory": true
													}
												],
												"positions": [
													{
														"id": 1,
														"Name": "HOME",
														"Position": 0
													},
													{
														"id": 1,
														"Name": "LOADING",
														"Position": 229.13
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "GANTRY1.Y1.SERVOPOSITION",
													"sMovingPlusSelected": "GANTRY1.Y1.SERVOMOVINGFWD",
													"sMovingMinusSelected": "GANTRY1.Y1.SERVOMOVINGREV"
												},
												"prevStateOption": "Axis",
												"line": 1,
												"asset": 16
											},
											{
												"id": 75,
												"nodeid": 75,
												"Name": "Y1.ApplicationIO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "Y1.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I67.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "Y1.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I67.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 283,
														"StateName": "Y1.IN.RANGE",
														"Equation": "! Y1.NEGATIVE.OVERTRAVEL && ! Y1.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 282,
														"StateName": "Y1.POSITIVE.OVERTRAVEL",
														"Equation": "Y1.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 281,
														"StateName": "Y1.NEGATIVE.OVERTRAVEL",
														"Equation": "Y1.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y1.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 16
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4,
										"positions": [
											{
												"id": 0,
												"Name": "HOME",
												"Position": 0
											},
											{
												"id": 1,
												"Name": "LOADING",
												"Position": 229.13
											}
										],
										"positionsSelection": {
											"sPostionsSelected": "Y1.POSITION",
											"sMovingPlusSelected": "Y1.MOVING.PLUS",
											"sMovingMinusSelected": "Y1.MOVING.MINUS"
										},
										"prevStateOption": "Axis"
									},
									{
										"id": 17,
										"nodeid": 17,
										"Name": "Gantry1.C1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 2,
												"Name": "C1.AXIS.180.DEGREE",
												"Address": "PLC2.I66.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "C1.AXIS.ZERO.DEGREE",
												"Address": "PLC2.I66.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "C1.AXIS.180.DEG.CMD",
												"Address": "PLC2.Q65.7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "C1.AXIS.0.DEGREE.CMD",
												"Address": "PLC2.Q65.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 210,
												"StateName": "C1.AXIS.MOVING.TO.180.DEGREE",
												"Equation": "C1.AXIS.180.DEG.CMD && ! C1.AXIS.180.DEGREE",
												"Priority": 0,
												"subassemblyName": "Gantry1.C1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 209,
												"StateName": "C1.AXIS.MOVING.TO.ZERO.DEGREE",
												"Equation": "C1.AXIS.0.DEGREE.CMD && ! C1.AXIS.ZERO.DEGREE",
												"Priority": 0,
												"subassemblyName": "Gantry1.C1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 208,
												"StateName": "C1.AXIS.AT.180.DEGREE",
												"Equation": "C1.AXIS.180.DEGREE && ! C1.AXIS.0.DEGREE.CMD && ( C1.AXIS.180.DEG.CMD || ! C1.AXIS.180.DEG.CMD)",
												"Priority": 0,
												"subassemblyName": "Gantry1.C1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 207,
												"StateName": "C1.AXIS.AT.ZERO.DEGREE",
												"Equation": "C1.AXIS.ZERO.DEGREE &&  ! C1.AXIS.180.DEG.CMD && ( C1.AXIS.0.DEGREE.CMD || ! C1.AXIS.0.DEGREE.CMD)",
												"Priority": 0,
												"subassemblyName": "Gantry1.C1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4,
										"ioSelection": {}
									},
									{
										"id": 18,
										"nodeid": 18,
										"Name": "Gantry1.Gripper1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 4,
												"Name": "GANTRY1.GRIPPER1.PART.PRESENT",
												"Address": "PLC2.I66.2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "GANTRY1.GRIPPER1.OPENED",
												"Address": "PLC2.I65.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY1.GRIPPER1.CLOSED1",
												"Address": "PLC2.I65.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY1.GRIPPER1.CLOSED2",
												"Address": "PLC2.I65.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "GANTRY1.GRIPPER1.OPEN",
												"Address": "PLC2.Q65.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "GANTRY1.GRIPPER1.CLOSE",
												"Address": "PLC2.Q65.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 216,
												"StateName": "GANTRY1.GRIPPER1.UNGRIPPING",
												"Equation": "GANTRY1.GRIPPER1.OPEN && ! GANTRY1.GRIPPER1.OPENED",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 215,
												"StateName": "GANTRY1.GRIPPER1.GRIPPING",
												"Equation": "GANTRY1.GRIPPER1.CLOSE && ! GANTRY1.GRIPPER1.CLOSED1 && ! GANTRY1.GRIPPER1.CLOSED2",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 214,
												"StateName": "GANTRY1.GRIPPER1.UNGRIPPED.WITH.PART",
												"Equation": "!GANTRY1.GRIPPER1.PART.PRESENT && GANTRY1.GRIPPER1.OPENED && ! GANTRY1.GRIPPER1.CLOSE && ( GANTRY1.GRIPPER1.OPEN || ! GANTRY1.GRIPPER1.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 213,
												"StateName": "GANTRY1.GRIPPER1.UNGRIPPED.WITHOUT.PART",
												"Equation": "GANTRY1.GRIPPER1.PART.PRESENT && GANTRY1.GRIPPER1.OPENED && ! GANTRY1.GRIPPER1.CLOSE && ( GANTRY1.GRIPPER1.OPEN || ! GANTRY1.GRIPPER1.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 212,
												"StateName": "GANTRY1.GRIPPER1.GRIPPED.WITHOUT.PART",
												"Equation": "GANTRY1.GRIPPER1.PART.PRESENT && GANTRY1.GRIPPER1.CLOSED1 && GANTRY1.GRIPPER1.CLOSED2 && ! GANTRY1.GRIPPER1.OPEN &&  ( GANTRY1.GRIPPER1.CLOSE || ! GANTRY1.GRIPPER1.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 211,
												"StateName": "GANTRY1.GRIPPER1.GRIPPED.WITH.PART",
												"Equation": "!GANTRY1.GRIPPER1.PART.PRESENT && GANTRY1.GRIPPER1.CLOSED1 && GANTRY1.GRIPPER1.CLOSED2 && ! GANTRY1.GRIPPER1.OPEN &&  ( GANTRY1.GRIPPER1.CLOSE || ! GANTRY1.GRIPPER1.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4
									},
									{
										"id": 19,
										"nodeid": 19,
										"Name": "Gantry1.Gripper2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 4,
												"Name": "GANTRY1.GRIPPER2.PART.PRESENT",
												"Address": "PLC2.I66.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "GANTRY1.GRIPPER2.CLOSED1",
												"Address": "PLC2.I65.7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY1.GRIPPER2.OPENED",
												"Address": "PLC2.I65.5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY1.GRIPPER2.CLOSED2",
												"Address": "PLC2.I65.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "GANTRY1.GRIPPER2.OPEN",
												"Address": "PLC2.Q65.5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "GANTRY1.GRIPPER2.CLOSE",
												"Address": "PLC2.Q65.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 222,
												"StateName": "GANTRY1.GRIPPER2.UNGRIPPING",
												"Equation": "GANTRY1.GRIPPER2.OPEN && ! GANTRY1.GRIPPER2.OPENED",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 221,
												"StateName": "GANTRY1.GRIPPER2.GRIPPING",
												"Equation": "GANTRY1.GRIPPER2.CLOSE && ! GANTRY1.GRIPPER2.CLOSED1 && ! GANTRY1.GRIPPER2.CLOSED2",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 220,
												"StateName": "GANTRY1.GRIPPER2.UNGRIPPED.WITHOUT.PART",
												"Equation": "GANTRY1.GRIPPER2.PART.PRESENT && GANTRY1.GRIPPER2.OPENED && ! GANTRY1.GRIPPER2.CLOSE && ( GANTRY1.GRIPPER2.OPEN || ! GANTRY1.GRIPPER2.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 219,
												"StateName": "GANTRY1.GRIPPER2.UNGRIPPED.WITH.PART",
												"Equation": "! GANTRY1.GRIPPER2.PART.PRESENT && GANTRY1.GRIPPER2.OPENED && ! GANTRY1.GRIPPER2.CLOSE && ( GANTRY1.GRIPPER2.OPEN || ! GANTRY1.GRIPPER2.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 218,
												"StateName": "GANTRY1.GRIPPER2.GRIPPED.WITHOUT.PART",
												"Equation": "GANTRY1.GRIPPER2.PART.PRESENT && GANTRY1.GRIPPER2.CLOSED1 && GANTRY1.GRIPPER2.CLOSED2 && ! GANTRY1.GRIPPER2.OPEN && ( GANTRY1.GRIPPER2.CLOSE || ! GANTRY1.GRIPPER2.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 217,
												"StateName": "GANTRY1.GRIPPER2.GRIPPED.WITH.PART",
												"Equation": "! GANTRY1.GRIPPER2.PART.PRESENT && GANTRY1.GRIPPER2.CLOSED1 && GANTRY1.GRIPPER2.CLOSED2 && ! GANTRY1.GRIPPER2.OPEN && ( GANTRY1.GRIPPER2.CLOSE || ! GANTRY1.GRIPPER2.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry1.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 4
									}
								],
								"InputsOutputs": [],
								"coupling": [],
								"EEA": [
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"IsLogging": false,
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Address": "",
										"IsLogging": true,
										"Type": "Shift"
									}
								]
							},
							{
								"id": 5,
								"nodeid": 5,
								"Name": "Gantry2",
								"field": "Asset",
								"coupleType": "Coupled",
								"isvirtualSubAssembly": false,
								"EquationStateNumbers": "",
								"Inputs": [
									{
										"Id": 6,
										"Name": "GANTRY2.GRIPPER2.MC.HANDLED",
										"Address": "PLC2.DB820.344,B",
										"InputType": "Quality",
										"DataType": "BYTE",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 5,
										"Name": "GANTRY2.GRIPPER2.PART.TYPE",
										"Address": "PLC2.DB820.342,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 4,
										"Name": "GANTRY2.GRIPPER2.PART.STATUS",
										"Address": "PLC2.DB820.340,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 3,
										"Name": "GANTRY2.GRIPPER1.MC.NUMBER",
										"Address": "PLC2.DB820.274,B",
										"InputType": "Quality",
										"DataType": "BYTE",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 2,
										"Name": "GANTRY2.GRIPPER1.PART.TYPE",
										"Address": "PLC2.DB820.272,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 1,
										"Name": "GANTRY2.GRIPPER1.PART.STATUS",
										"Address": "PLC2.DB820.270,I",
										"InputType": "Quality",
										"DataType": "INT",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									}
								],
								"Outputs": [],
								"States": [],
								"nodes": [
									{
										"id": 20,
										"nodeid": 20,
										"Name": "Gantry2.X2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 92,
												"nodeid": 92,
												"Name": "Gantry2.X2.Servo",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY2.X2.POSITION",
														"Address": "PLC2.DB670.412,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY2.X2.MOVING.MINUS",
														"Address": "PLC2.DB34.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "GANTRY2.X2.MOVING.PLUS",
														"Address": "PLC2.DB34.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 464,
														"StateName": "GANTRY2.X2.SERVO NEAR OUTFEED",
														"Equation": "(GANTRY2.X2.POSITION > 8559) && (GANTRY2.X2.POSITION <= 8569) && (GANTRY2.X2.MOVING.PLUS || GANTRY2.X2.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 463,
														"StateName": "GANTRY2.X2.SERVO BETWEEN OUTFEED TO MACHINE3",
														"Equation": "(GANTRY2.X2.POSITION > 3189) && (GANTRY2.X2.POSITION <= 8559) && GANTRY2.X2.MOVING.MINUS && !GANTRY2.X2.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 462,
														"StateName": "GANTRY2.X2.SERVO AT OUTFEED",
														"Equation": "(GANTRY2.X2.POSITION >= 8559) && (GANTRY2.X2.POSITION <= 8569) && !GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 461,
														"StateName": "GANTRY2.X2.SERVO NEAR MACHINE3",
														"Equation": "(GANTRY2.X2.POSITION > 3179) && (GANTRY2.X2.POSITION <= 3189) && (GANTRY2.X2.MOVING.PLUS || GANTRY2.X2.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 460,
														"StateName": "GANTRY2.X2.SERVO BETWEEN MACHINE3 TO NEST3",
														"Equation": "(GANTRY2.X2.POSITION > 1184) && (GANTRY2.X2.POSITION <= 3179) && GANTRY2.X2.MOVING.MINUS && !GANTRY2.X2.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 459,
														"StateName": "GANTRY2.X2.SERVO BETWEEN MACHINE3 TO OUTFEED",
														"Equation": "(GANTRY2.X2.POSITION > 3189) && (GANTRY2.X2.POSITION <= 8559) && GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 458,
														"StateName": "GANTRY2.X2.SERVO AT MACHINE3",
														"Equation": "(GANTRY2.X2.POSITION >= 3179) && (GANTRY2.X2.POSITION <= 3189) && !GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 457,
														"StateName": "GANTRY2.X2.SERVO NEAR NEST3",
														"Equation": "(GANTRY2.X2.POSITION > 1174) && (GANTRY2.X2.POSITION <= 1184) && (GANTRY2.X2.MOVING.PLUS || GANTRY2.X2.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 456,
														"StateName": "GANTRY2.X2.SERVO BETWEEN NEST3 TO COMMONZONE",
														"Equation": "(GANTRY2.X2.POSITION > 1005) && (GANTRY2.X2.POSITION <= 1174) && GANTRY2.X2.MOVING.MINUS && !GANTRY2.X2.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 455,
														"StateName": "GANTRY2.X2.SERVO BETWEEN NEST3 TO MACHINE3",
														"Equation": "(GANTRY2.X2.POSITION > 1184) && (GANTRY2.X2.POSITION <= 3179) && GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 454,
														"StateName": "GANTRY2.X2.SERVO AT NEST3",
														"Equation": "(GANTRY2.X2.POSITION >= 1174) && (GANTRY2.X2.POSITION <= 1184) && !GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 453,
														"StateName": "GANTRY2.X2.SERVO NEAR COMMONZONE",
														"Equation": "(GANTRY2.X2.POSITION > 995) && (GANTRY2.X2.POSITION <= 1005) && (GANTRY2.X2.MOVING.PLUS || GANTRY2.X2.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 452,
														"StateName": "GANTRY2.X2.SERVO BETWEEN COMMONZONE TO NEST1",
														"Equation": "(GANTRY2.X2.POSITION > 713) && (GANTRY2.X2.POSITION <= 995) && GANTRY2.X2.MOVING.MINUS && !GANTRY2.X2.MOVING.PLUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 451,
														"StateName": "GANTRY2.X2.SERVO BETWEEN COMMONZONE TO NEST3",
														"Equation": "(GANTRY2.X2.POSITION > 1005) && (GANTRY2.X2.POSITION <= 1174) && GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 450,
														"StateName": "GANTRY2.X2.SERVO AT COMMONZONE",
														"Equation": "(GANTRY2.X2.POSITION >= 995) && (GANTRY2.X2.POSITION <= 1005) && !GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 449,
														"StateName": "GANTRY2.X2.SERVO NEAR NEST1",
														"Equation": "(GANTRY2.X2.POSITION > 703) && (GANTRY2.X2.POSITION <= 713) && (GANTRY2.X2.MOVING.PLUS || GANTRY2.X2.MOVING.MINUS)",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 448,
														"StateName": "GANTRY2.X2.SERVO BETWEEN NEST1 TO COMMONZONE",
														"Equation": "(GANTRY2.X2.POSITION > 713) && (GANTRY2.X2.POSITION <= 995) && GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 447,
														"StateName": "GANTRY2.X2.SERVO AT NEST1",
														"Equation": "(GANTRY2.X2.POSITION >= 703) && (GANTRY2.X2.POSITION <= 713) && !GANTRY2.X2.MOVING.PLUS && !GANTRY2.X2.MOVING.MINUS",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 20,
												"positions": [
													{
														"id": 0,
														"Name": "Nest1",
														"Position": 708
													},
													{
														"id": 1,
														"Name": "commonZone",
														"Position": 1000
													},
													{
														"id": 2,
														"Name": "Nest3",
														"Position": 1179
													},
													{
														"id": 3,
														"Name": "Machine3",
														"Position": 3184
													},
													{
														"id": 4,
														"Name": "Outfeed",
														"Position": 8564
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "GANTRY2.X2.POSITION",
													"sMovingPlusSelected": "GANTRY2.X2.MOVING.PLUS",
													"sMovingMinusSelected": "GANTRY2.X2.MOVING.MINUS"
												},
												"prevStateOption": "Axis"
											},
											{
												"id": 96,
												"nodeid": 96,
												"Name": "Gantry2.X2.ApplicationIO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 2,
														"Name": "X2.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I80.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 1,
														"Name": "X2.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I80.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 467,
														"StateName": "X2.IN.SAFE.POSITION",
														"Equation": "! X2.NEGATIVE.OVERTRAVEL && ! X2.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 466,
														"StateName": "X2.NEGATIVE.OVERTRAVEL",
														"Equation": "X2.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 465,
														"StateName": "X2.POSITIVE.OVERTRAVEL",
														"Equation": "X2.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Gantry2.X2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [],
												"line": 1,
												"asset": 20
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5,
										"positions": [
											{
												"id": 2,
												"Name": "NEST1",
												"Position": 708.899
											},
											{
												"id": 5,
												"Name": "COMMON.ZONE",
												"Position": 1000
											},
											{
												"id": 3,
												"Name": "NEST2",
												"Position": 1004.968
											},
											{
												"id": 4,
												"Name": "NEST3",
												"Position": 1179.299
											},
											{
												"id": 1,
												"Name": "MACHINE3",
												"Position": 3184.63
											},
											{
												"id": 0,
												"Name": "OUTFEED.CONVYOR",
												"Position": 8564.363
											}
										],
										"positionsSelection": {
											"sPostionsSelected": "X2.POSITION",
											"sMovingPlusSelected": "X2.MOVING.PLUS",
											"sMovingMinusSelected": "X2.MOVING.MINUS"
										},
										"prevStateOption": "Axis"
									},
									{
										"id": 21,
										"nodeid": 21,
										"Name": "Gantry2.Z2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 76,
												"nodeid": 76,
												"Name": "Gantry2.Z2.Servo",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "GANTRY2.Z2.SERVOMOVINGFWD",
														"Address": "PLC2.DB36.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY2.Z2.SERVOMOVINGREV",
														"Address": "PLC2.DB36.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 2
													},
													{
														"Id": 3,
														"Name": "GANTRY2.Z2.SERVOPOSITION",
														"Address": "PLC2.DB670.492,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 3
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 390,
														"StateName": "GANTRY2.Z2.SERVO NEAR HOME",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -5) && (GANTRY2.Z2.SERVOPOSITION <= 5) && (GANTRY2.Z2.SERVOMOVINGFWD || GANTRY2.Z2.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 389,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN HOME TO POSITION1",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -164) && (GANTRY2.Z2.SERVOPOSITION <= -5) && GANTRY2.Z2.SERVOMOVINGREV && !GANTRY2.Z2.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 388,
														"StateName": "GANTRY2.Z2.SERVO AT HOME",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION >= -5) && (GANTRY2.Z2.SERVOPOSITION <= 5) && !GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 387,
														"StateName": "GANTRY2.Z2.SERVO NEAR POSITION1",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -174) && (GANTRY2.Z2.SERVOPOSITION <= -164) && (GANTRY2.Z2.SERVOMOVINGFWD || GANTRY2.Z2.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 386,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN POSITION1 TO POSITION2",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1211) && (GANTRY2.Z2.SERVOPOSITION <= -174) && GANTRY2.Z2.SERVOMOVINGREV && !GANTRY2.Z2.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 385,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN POSITION1 TO HOME",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -164) && (GANTRY2.Z2.SERVOPOSITION <= -5) && GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 384,
														"StateName": "GANTRY2.Z2.SERVO AT POSITION1",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION >= -174.5) && (GANTRY2.Z2.SERVOPOSITION <= -164.5) && !GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 383,
														"StateName": "GANTRY2.Z2.SERVO NEAR POSITION2",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1221) && (GANTRY2.Z2.SERVOPOSITION <= -1211) && (GANTRY2.Z2.SERVOMOVINGFWD || GANTRY2.Z2.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 382,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN POSITION2 TO POSITION3",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1273) && (GANTRY2.Z2.SERVOPOSITION <= -1221) && GANTRY2.Z2.SERVOMOVINGREV && !GANTRY2.Z2.SERVOMOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 381,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN POSITION2 TO POSITION1",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1211) && (GANTRY2.Z2.SERVOPOSITION <= -174) && GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 380,
														"StateName": "GANTRY2.Z2.SERVO AT POSITION2",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION >= -1221.1) && (GANTRY2.Z2.SERVOPOSITION <= -1211.1) && !GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 379,
														"StateName": "GANTRY2.Z2.SERVO NEAR POSITION3",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1283) && (GANTRY2.Z2.SERVOPOSITION <= -1273) && (GANTRY2.Z2.SERVOMOVINGFWD || GANTRY2.Z2.SERVOMOVINGREV)",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 378,
														"StateName": "GANTRY2.Z2.SERVO BETWEEN POSITION3 TO POSITION2",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION > -1273) && (GANTRY2.Z2.SERVOPOSITION <= -1221) && GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 377,
														"StateName": "GANTRY2.Z2.SERVO AT POSITION3",
														"Equation": "(GANTRY2.Z2.SERVOPOSITION >= -1283.95) && (GANTRY2.Z2.SERVOPOSITION <= -1273.95) && !GANTRY2.Z2.SERVOMOVINGFWD && !GANTRY2.Z2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Gantry2.Z2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY2.Z2.SERVOMOVINGFWD",
														"Address": "PLC2.DB36.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 2,
														"Name": "GANTRY2.Z2.SERVOMOVINGREV",
														"Address": "PLC2.DB36.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 3,
														"Name": "GANTRY2.Z2.SAFE",
														"Address": "PLC2.I81.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													}
												],
												"positions": [
													{
														"id": 3,
														"Name": "POSITION3",
														"Position": -1278.95
													},
													{
														"id": 2,
														"Name": "POSITION2",
														"Position": -1216.1
													},
													{
														"id": 1,
														"Name": "POSITION1",
														"Position": -169.5
													},
													{
														"id": 0,
														"Name": "HOME",
														"Position": 0
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "GANTRY2.Z2.SERVOPOSITION",
													"sMovingPlusSelected": "GANTRY2.Z2.SERVOMOVINGFWD",
													"sMovingMinusSelected": "GANTRY2.Z2.SERVOMOVINGREV"
												},
												"prevStateOption": "Axis",
												"line": 1,
												"asset": 21
											},
											{
												"id": 77,
												"nodeid": 77,
												"Name": "Z2.ApplicationIO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "Z2.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I81.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "Z2.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I81.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 288,
														"StateName": "Z2.IN.RANGE",
														"Equation": "! Z2.POSITIVE.OVERTRAVEL && ! Z2.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Z2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 287,
														"StateName": "Z2.POSITIVE.OVERTRAVEL",
														"Equation": "Z2.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Z2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 286,
														"StateName": "Z2.NEGATIVE.OVERTRAVEL",
														"Equation": "Z2.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Z2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "Z2.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I81.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "Z2.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I81.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "Z2.AXIS.SAFE",
														"Address": "PLC2.I81.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 21,
												"positions": [],
												"positionsSelection": {}
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5,
										"positions": [
											{
												"id": 3,
												"Name": "POSITION3",
												"Position": -1278.95
											},
											{
												"id": 2,
												"Name": "POSITION2",
												"Position": -1216.1
											},
											{
												"id": 1,
												"Name": "POSITION1",
												"Position": -169
											},
											{
												"id": 6,
												"Name": "HOME",
												"Position": 0
											}
										],
										"positionsSelection": {
											"sPostionsSelected": "Z2.POSITION",
											"sMovingPlusSelected": "Z2.MOVING.PLUS",
											"sMovingMinusSelected": "Z2.MOVING.MINUS"
										},
										"prevStateOption": "Axis"
									},
									{
										"id": 22,
										"nodeid": 22,
										"Name": "Gantry2.Y2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "Y2.MOVING.MINUS",
												"Address": "PLC2.DB35.64,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "Y2.MOVING.PLUS",
												"Address": "PLC2.DB35.64,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "Y2.SAFE",
												"Address": "PLC2.I87.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [
											{
												"Id": 202,
												"StateName": "GANTRY2.Y2 NEAR LOADING",
												"Equation": "(Y2.POSITION > 224) && (Y2.POSITION <= 234) && (Y2.MOVING.PLUS || Y2.MOVING.MINUS)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											},
											{
												"Id": 201,
												"StateName": "GANTRY2.Y2 BETWEEN LOADING TO HOME",
												"Equation": "(Y2.POSITION > 5) && (Y2.POSITION <= 224) && Y2.MOVING.MINUS && !Y2.MOVING.PLUS",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											},
											{
												"Id": 200,
												"StateName": "GANTRY2.Y2 AT LOADING",
												"Equation": "(Y2.POSITION >= 224.13) && (Y2.POSITION <= 234.13) && !Y2.MOVING.PLUS && !Y2.MOVING.MINUS",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											},
											{
												"Id": 199,
												"StateName": "GANTRY2.Y2 NEAR HOME",
												"Equation": "(Y2.POSITION > -5) && (Y2.POSITION <= 5) && (Y2.MOVING.PLUS || Y2.MOVING.MINUS)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											},
											{
												"Id": 198,
												"StateName": "GANTRY2.Y2 BETWEEN HOME TO LOADING",
												"Equation": "(Y2.POSITION > 5) && (Y2.POSITION <= 224) && Y2.MOVING.PLUS && !Y2.MOVING.MINUS",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											},
											{
												"Id": 197,
												"StateName": "GANTRY2.Y2 AT HOME",
												"Equation": "(Y2.POSITION >= -5) && (Y2.POSITION <= 5) && !Y2.MOVING.PLUS && !Y2.MOVING.MINUS",
												"Priority": 0,
												"subassemblyName": "Gantry2.Y2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": false,
												"iscouplingstate": false
											}
										],
										"nodes": [
											{
												"id": 78,
												"nodeid": 78,
												"Name": "Y2.Servo",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "Y2.SERVO_MOVINGFWD",
														"Address": "PLC2.DB35.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 1
													},
													{
														"Id": 2,
														"Name": "Y2.SERVOMOVINGREV",
														"Address": "PLC2.DB35.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 2
													},
													{
														"Id": 3,
														"Name": "Y2.SAFE",
														"Address": "PLC2.I87.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"id": 3
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 234,
														"StateName": "Y2.SERVO MOVING TO LOADING",
														"Equation": "(Y2.SERVOPOSITION >= 0) && (Y2.SERVOPOSITION < 229.13) && Y2.SERVO_MOVINGFWD",
														"Priority": 0,
														"subassemblyName": "Y2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 233,
														"StateName": "Y2.SERVO AT LOADING",
														"Equation": "(Y2.SERVOPOSITION > 224.13) && (Y2.SERVOPOSITION < 234.13) && !Y2.SERVO_MOVINGFWD && !Y2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Y2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 232,
														"StateName": "Y2.SERVO MOVING TO HOME",
														"Equation": "(Y2.SERVOPOSITION >= 0) && (Y2.SERVOPOSITION <= 229.13) && Y2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Y2.Servo",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													},
													{
														"Id": 231,
														"StateName": "Y2.SERVO AT HOME",
														"Equation": "(Y2.SERVOPOSITION > -5) && (Y2.SERVOPOSITION < 5) && !Y2.SERVO_MOVINGFWD && !Y2.SERVOMOVINGREV",
														"Priority": 0,
														"subassemblyName": "Y2.Servo",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": false,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "Y2.SERVO_MOVINGFWD",
														"Address": "PLC2.DB35.64,X7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 2,
														"Name": "Y2.SERVOMOVINGREV",
														"Address": "PLC2.DB35.64,X6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true
													},
													{
														"Id": 3,
														"Name": "Y2.SERVOPOSITION",
														"Address": "PLC2.DB670.452,DI",
														"InputType": "Position",
														"DataType": "REAL",
														"IOType": "Input",
														"isCompulsory": true
													}
												],
												"positions": [
													{
														"id": 0,
														"Name": "HOME",
														"Position": 0
													},
													{
														"id": 1,
														"Name": "LOADING",
														"Position": 229.13
													}
												],
												"positionsSelection": {
													"sPostionsSelected": "Y2.SERVOPOSITION",
													"sMovingPlusSelected": "Y2.SERVO_MOVINGFWD",
													"sMovingMinusSelected": "Y2.SERVOMOVINGREV"
												},
												"prevStateOption": "Axis",
												"line": 1,
												"asset": 22
											},
											{
												"id": 79,
												"nodeid": 79,
												"Name": "Y2.ApplicationIO",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "Y2.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I87.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "Y2.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I87.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "Y2.SAFE",
														"Address": "PLC2.I87.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 295,
														"StateName": "Y2.NOT.AT.HOME",
														"Equation": "! Y2.SAFE",
														"Priority": 0,
														"subassemblyName": "Y2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 294,
														"StateName": "Y2.SAFE",
														"Equation": "Y2.SAFE",
														"Priority": 0,
														"subassemblyName": "Y2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 293,
														"StateName": "Y2.IN.RANGE",
														"Equation": "!Y2.POSITIVE.OVERTRAVEL && ! Y2.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 292,
														"StateName": "Y2.POSITIVE.OVERTRAVEL",
														"Equation": "Y2.POSITIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 291,
														"StateName": "Y2.NEGATIVE.OVERTRAVEL",
														"Equation": "Y2.NEGATIVE.OVERTRAVEL",
														"Priority": 0,
														"subassemblyName": "Y2.ApplicationIO",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "Y2.POSITIVE.OVERTRAVEL",
														"Address": "PLC2.I87.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "Y2.NEGATIVE.OVERTRAVEL",
														"Address": "PLC2.I87.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "Y2.SAFE",
														"Address": "PLC2.I87.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 22
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5,
										"positions": [
											{
												"id": 0,
												"Name": "HOME",
												"Position": 0
											},
											{
												"id": 1,
												"Name": "LOADING",
												"Position": 229.13
											}
										],
										"positionsSelection": {
											"sPostionsSelected": "Y2.POSITION",
											"sMovingPlusSelected": "Y2.MOVING.PLUS",
											"sMovingMinusSelected": "Y2.MOVING.MINUS"
										},
										"prevStateOption": "Axis"
									},
									{
										"id": 23,
										"nodeid": 23,
										"Name": "Gantry2.C2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 2,
												"Name": "C2.180.DEGREE",
												"Address": "PLC2.I86.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "C2.ZERO.DEGREE",
												"Address": "PLC2.I86.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "C2.180.DEGREE.CMD",
												"Address": "PLC2.Q85.7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "C2.ZERO.DEGREE.CMD",
												"Address": "PLC2.Q85.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 238,
												"StateName": "GANTRY2.C2.MOVING.TO.180.DEGREE",
												"Equation": "C2.180.DEGREE.CMD && ! C2.180.DEGREE",
												"Priority": 0,
												"subassemblyName": "Gantry2.C2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 237,
												"StateName": "GANTRY2.C2.MOVING.TO.ZERO.DEGREE",
												"Equation": "C2.ZERO.DEGREE.CMD && ! C2.ZERO.DEGREE",
												"Priority": 0,
												"subassemblyName": "Gantry2.C2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 236,
												"StateName": "GANTRY2.C2.AT.180.DEGREE",
												"Equation": "C2.180.DEGREE && ! C2.ZERO.DEGREE.CMD && ( C2.180.DEGREE.CMD || ! C2.180.DEGREE.CMD)",
												"Priority": 0,
												"subassemblyName": "Gantry2.C2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 235,
												"StateName": "GANTRY2.C2.AT.ZERO.DEGREE",
												"Equation": "C2.ZERO.DEGREE && ! C2.180.DEGREE.CMD && ( C2.ZERO.DEGREE.CMD || ! C2.ZERO.DEGREE.CMD)",
												"Priority": 0,
												"subassemblyName": "Gantry2.C2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5
									},
									{
										"id": 24,
										"nodeid": 24,
										"Name": "Gantry2.Gripper1",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 4,
												"Name": "GANTRY2.GRIPPER1.PART.PRESENT",
												"Address": "PLC2.I86.2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "GANTRY2.GRIPPER1.OPENED",
												"Address": "PLC2.I85.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY2.GRIPPER1.CLOSED1",
												"Address": "PLC2.I85.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY2.GRIPPER1.CLOSED2",
												"Address": "PLC2.I85.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "GANTRY2.GRIPPER1.OPEN",
												"Address": "PLC2.Q85.4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "GANTRY2.GRIPPER1.CLOSE",
												"Address": "PLC2.Q85.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 244,
												"StateName": "GANTRY2.GRIPPER1.UNGRIPPING",
												"Equation": "GANTRY2.GRIPPER1.OPEN && ! GANTRY2.GRIPPER1.OPENED",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 243,
												"StateName": "GANTRY2.GRIPPER1.GRIPPING",
												"Equation": "GANTRY2.GRIPPER1.CLOSE && ! GANTRY2.GRIPPER1.CLOSED1 && ! GANTRY2.GRIPPER1.CLOSED2",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 242,
												"StateName": "GANTRY2.GRIPPER1.UNGRIPPED.WITHOUT.PART",
												"Equation": "GANTRY2.GRIPPER1.PART.PRESENT && GANTRY2.GRIPPER1.OPENED && ! GANTRY2.GRIPPER1.CLOSE && ( GANTRY2.GRIPPER1.OPEN || ! GANTRY2.GRIPPER1.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 241,
												"StateName": "GANTRY2.GRIPPER1.UNGRIPPED.WITH.PART",
												"Equation": "! GANTRY2.GRIPPER1.PART.PRESENT && GANTRY2.GRIPPER1.OPENED && ! GANTRY2.GRIPPER1.CLOSE && ( GANTRY2.GRIPPER1.OPEN || ! GANTRY2.GRIPPER1.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 240,
												"StateName": "GANTRY2.GRIPPER1.GRIPPED.WITHOUT.PART",
												"Equation": "GANTRY2.GRIPPER1.PART.PRESENT && GANTRY2.GRIPPER1.CLOSED1 && GANTRY2.GRIPPER1.CLOSED2 && ! GANTRY2.GRIPPER1.OPEN && ( GANTRY2.GRIPPER1.CLOSE || ! GANTRY2.GRIPPER1.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 239,
												"StateName": "GANTRY2.GRIPPER1.GRIPPED.WITH.PART",
												"Equation": "! GANTRY2.GRIPPER1.PART.PRESENT && GANTRY2.GRIPPER1.CLOSED1 && GANTRY2.GRIPPER1.CLOSED2 && ! GANTRY2.GRIPPER1.OPEN && ( GANTRY2.GRIPPER1.CLOSE || ! GANTRY2.GRIPPER1.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5
									},
									{
										"id": 25,
										"nodeid": 25,
										"Name": "Gantry2.Gripper2",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 4,
												"Name": "GANTRY2.GRIPPER2.PART.PRESENT",
												"Address": "PLC2.I86.3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "GANTRY2.GRIPPER2.OPENED",
												"Address": "PLC2.I85.5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY2.GRIPPER2.CLOSED1",
												"Address": "PLC2.I85.7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY2.GRIPPER2.CLOSED2",
												"Address": "PLC2.I85.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 2,
												"Name": "GANTRY2.GRIPPER2.OPEN",
												"Address": "PLC2.Q85.5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 1,
												"Name": "GANTRY2.GRIPPER2.CLOSE",
												"Address": "PLC2.Q85.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 250,
												"StateName": "GANTRY2.GRIPPER2.UNGRIPPING",
												"Equation": "GANTRY2.GRIPPER2.OPEN && ! GANTRY2.GRIPPER2.OPENED",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 249,
												"StateName": "GANTRY2.GRIPPER2.GRIPPING",
												"Equation": "GANTRY2.GRIPPER2.CLOSE && ! GANTRY2.GRIPPER2.CLOSED1 && ! GANTRY2.GRIPPER2.CLOSED2",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 248,
												"StateName": "GANTRY2.GRIPPER2.UNGRIPPED.WITHOUT.PART",
												"Equation": "GANTRY2.GRIPPER2.PART.PRESENT && GANTRY2.GRIPPER2.OPENED && ! GANTRY2.GRIPPER2.CLOSE && ( GANTRY2.GRIPPER2.OPEN  || ! GANTRY2.GRIPPER2.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 247,
												"StateName": "GANTRY2.GRIPPER2.UNGRIPPED.WITH.PART",
												"Equation": "! GANTRY2.GRIPPER2.PART.PRESENT && GANTRY2.GRIPPER2.OPENED && ! GANTRY2.GRIPPER2.CLOSE && ( GANTRY2.GRIPPER2.OPEN  || ! GANTRY2.GRIPPER2.OPEN)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 246,
												"StateName": "GANTRY2.GRIPPER2.GRIPPED.WITHOUT.PART",
												"Equation": "GANTRY2.GRIPPER2.PART.PRESENT && GANTRY2.GRIPPER2.CLOSED1 && GANTRY2.GRIPPER2.CLOSED2 && ! GANTRY2.GRIPPER2.OPEN && ( GANTRY2.GRIPPER2.CLOSE || ! GANTRY2.GRIPPER2.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 245,
												"StateName": "GANTRY2.GRIPPER2.GRIPPED.WITH.PART",
												"Equation": "! GANTRY2.GRIPPER2.PART.PRESENT && GANTRY2.GRIPPER2.CLOSED1 && GANTRY2.GRIPPER2.CLOSED2 && ! GANTRY2.GRIPPER2.OPEN && ( GANTRY2.GRIPPER2.CLOSE || ! GANTRY2.GRIPPER2.CLOSE)",
												"Priority": 0,
												"subassemblyName": "Gantry2.Gripper2",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 5
									}
								],
								"InputsOutputs": [],
								"coupling": [],
								"EEA": [
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"IsLogging": false,
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Address": "",
										"IsLogging": true,
										"Type": "Shift"
									}
								]
							},
							{
								"id": 6,
								"nodeid": 6,
								"Name": "Outfeed.System",
								"field": "Asset",
								"coupleType": "Coupled",
								"isvirtualSubAssembly": false,
								"EquationStateNumbers": "",
								"Inputs": [
									{
										"Id": 5,
										"Name": "OUTFEED.MANUAL.MODE",
										"Address": "PLC3.DB1000.20,X0",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 4,
										"Name": "OUTFEED.AUTO.RUNNING",
										"Address": "PLC3.DB1000.21,X0",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 3,
										"Name": "OUTFEED.SINGLE.CYCLE",
										"Address": "PLC3.DB1000.21,X1",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 2,
										"Name": "OUTFEED.RUNOUT.CYCLE",
										"Address": "PLC3.DB1000.21,X3",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									},
									{
										"Id": 1,
										"Name": "OUTFEED.AUTO.MODE",
										"Address": "PLC3.DB1000.20,X2",
										"InputType": "Sequence",
										"DataType": "BOOL",
										"IOType": "Input",
										"Min": "0",
										"Max": "0"
									}
								],
								"Outputs": [],
								"States": [
									{
										"Id": 266,
										"StateName": "OUTFEED.AUTO.MODE",
										"Equation": "OUTFEED.AUTO.MODE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 265,
										"StateName": "OUTFEED.RUNOUT.CYCLE",
										"Equation": "OUTFEED.RUNOUT.CYCLE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 264,
										"StateName": "OUTFEED.SINGLE.CYCLE",
										"Equation": "OUTFEED.SINGLE.CYCLE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 263,
										"StateName": "OUTFEED.AUTO.RUNNING",
										"Equation": "OUTFEED.AUTO.RUNNING",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									},
									{
										"Id": 262,
										"StateName": "OUTFEED.MANUAL.MODE",
										"Equation": "OUTFEED.MANUAL.MODE",
										"Priority": 0,
										"subassemblyName": "Asset",
										"statetype": "System",
										"SequenceNo": 0,
										"isCustom": true,
										"iscouplingstate": false
									}
								],
								"nodes": [
									{
										"id": 26,
										"nodeid": 26,
										"Name": "Gantry.Drop.Off.Station",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "GANTRY.DROP.OFF.STATION.MC.NUMBER",
												"Address": "PLC3.DB820.4,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "GANTRY.DROP.OFF.STATION.PART.STATUS",
												"Address": "PLC3.DB820.2,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "GANTRY.DROPP.OFF.STATION.PART.TYPE",
												"Address": "PLC3.DB820.0,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [
											{
												"Id": 316,
												"StateName": "GANTRY.DROP.OFF.PART.TYPE3",
												"Equation": "GANTRY.DROPP.OFF.STATION.PART.TYPE >10 && GANTRY.DROPP.OFF.STATION.PART.TYPE <14",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 315,
												"StateName": "GANTRY.DROP.OFF.PART.TYPE2",
												"Equation": "GANTRY.DROPP.OFF.STATION.PART.TYPE >90 && GANTRY.DROPP.OFF.STATION.PART.TYPE <95",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 314,
												"StateName": "GANTRY.DROP.OFF.PART.TYPE1",
												"Equation": "GANTRY.DROPP.OFF.STATION.PART.TYPE >80 && GANTRY.DROPP.OFF.STATION.PART.TYPE <85",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 313,
												"StateName": "GANTRY.DROP.OFF.PART.BYPASSED",
												"Equation": "GANTRY.DROP.OFF.STATION.PART.STATUS ==5",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 312,
												"StateName": "GANTRY.DROP.OFF.PART.QC",
												"Equation": "GANTRY.DROP.OFF.STATION.PART.STATUS ==4",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 311,
												"StateName": "GANTRY.DROP.OFF.PART.REJECT",
												"Equation": "GANTRY.DROP.OFF.STATION.PART.STATUS ==3",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 310,
												"StateName": "GANTRY.DROP.OFF.PART.QUARANTINE",
												"Equation": "GANTRY.DROP.OFF.STATION.PART.STATUS ==2",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 309,
												"StateName": "GANTRY.DROP.OFF.PART.OK",
												"Equation": "GANTRY.DROP.OFF.STATION.PART.STATUS ==1",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 308,
												"StateName": "GANTRY.DROP.OFF.PART.FROM.MC3",
												"Equation": "GANTRY.DROP.OFF.STATION.MC.NUMBER ==3",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 307,
												"StateName": "GANTRY.DROP.OFF.PART.FROM.MC2",
												"Equation": "GANTRY.DROP.OFF.STATION.MC.NUMBER ==2",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 306,
												"StateName": "GANTRY.DROP.OFF.STATION.PART.FROM.MC1",
												"Equation": "GANTRY.DROP.OFF.STATION.MC.NUMBER ==1",
												"Priority": 0,
												"subassemblyName": "Gantry.Drop.Off.Station",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [
											{
												"id": 58,
												"nodeid": 58,
												"Name": "Gantry.Drop.Off.Lifter",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.DROP.OFF.LIFTER.LOWERED",
														"Address": "PLC3.I415.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.DROP.OFF.LIFTER.RAISED",
														"Address": "PLC3.I415.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.DROP.OFF.LIFTER.LOWER",
														"Address": "PLC3.Q460.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.DROP.OFF.LIFTER.RAISE",
														"Address": "PLC3.Q460.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 96,
														"StateName": "GANTRY.DROP.OFF.LIFTER.OUTPUTS.OFF",
														"Equation": "! GANTRY.DROP.OFF.LIFTER.LOWER && ! GANTRY.DROP.OFF.LIFTER.RAISE && ! GANTRY.DROP.OFF.LIFTER.LOWERED && ! GANTRY.DROP.OFF.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 95,
														"StateName": "GANTRY.DROP.OFF.LIFTER.MOVING.DOWN",
														"Equation": "GANTRY.DROP.OFF.LIFTER.LOWER && ! GANTRY.DROP.OFF.LIFTER.RAISE &&  ! GANTRY.DROP.OFF.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 94,
														"StateName": "GANTRY.DROP.OFF.LIFTER.MOVING.UP",
														"Equation": "GANTRY.DROP.OFF.LIFTER.RAISE && ! GANTRY.DROP.OFF.LIFTER.LOWER && ! GANTRY.DROP.OFF.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 93,
														"StateName": "GANTRY.DROP.OFF.LIFTER.AT.DOWN.POSITION",
														"Equation": "GANTRY.DROP.OFF.LIFTER.LOWER && ! GANTRY.DROP.OFF.LIFTER.RAISE && GANTRY.DROP.OFF.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 92,
														"StateName": "GANTRY.DROP.OFF.LIFTER.AT.UP.POSITION",
														"Equation": "GANTRY.DROP.OFF.LIFTER.RAISE && ! GANTRY.DROP.OFF.LIFTER.LOWER && GANTRY.DROP.OFF.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.DROP.OFF.LIFTER.LOWER",
														"Address": "PLC3.Q460.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.DROP.OFF.LIFTER.RAISE",
														"Address": "PLC3.Q460.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.DROP.OFF.LIFTER.LOWERED",
														"Address": "PLC3.I415.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.DROP.OFF.LIFTER.RAISED",
														"Address": "PLC3.I415.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 26
											},
											{
												"id": 59,
												"nodeid": 59,
												"Name": "Gantry.Drop.Off.Stopper",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "GANTRY.DROP.OFF.STOPPER.PART.PRESENT",
														"Address": "PLC3.I415.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "GANTRY.DROP.OFF.STOPPER.RAISED",
														"Address": "PLC3.I415.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "GANTRY.DROP.OFF.STOPPER.LOWER",
														"Address": "PLC3.Q460.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "GANTRY.DROP.OFF.STOPPER.RAISE",
														"Address": "PLC3.Q460.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 101,
														"StateName": "GANTRY.DROP.OFF.STOPPER.PART.PRESENT",
														"Equation": "GANTRY.DROP.OFF.STOPPER.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 100,
														"StateName": "GANTRY.DROP.OFF.STOPPER.MOVING.DOWN",
														"Equation": "GANTRY.DROP.OFF.STOPPER.LOWER && GANTRY.DROP.OFF.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 99,
														"StateName": "GANTRY.DROP.OFF.STOPPER.MOVING.UP",
														"Equation": "GANTRY.DROP.OFF.STOPPER.RAISE && !GANTRY.DROP.OFF.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 98,
														"StateName": "GANTRY.DROP.OFF.STOPPER.AT.DOWN",
														"Equation": "GANTRY.DROP.OFF.STOPPER.LOWER && !GANTRY.DROP.OFF.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 97,
														"StateName": "GANTRY.DROP.OFF.STOPPER.AT.UP",
														"Equation": "GANTRY.DROP.OFF.STOPPER.RAISE && GANTRY.DROP.OFF.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Gantry.Drop.Off.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "GANTRY.DROP.OFF.STOPPER.LOWER",
														"Address": "PLC3.Q462.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "GANTRY.DROP.OFF.STOPPER.RAISE",
														"Address": "PLC3.Q462.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "GANTRY.DROP.OFF.STOPPER.PART.PRESENT",
														"Address": "PLC3.I422.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "GANTRY.DROP.OFF.STOPPER.RAISED",
														"Address": "PLC3.I421.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 26
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 27,
										"nodeid": 27,
										"Name": "Laser.Marking.Station",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 3,
												"Name": "LASER.MARKER.MC.NUMBER",
												"Address": "PLC3.DB820.24,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "LASER.MARKER.PART.STATUS",
												"Address": "PLC3.DB820.22,I",
												"InputType": "Quality",
												"DataType": "INT",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 1,
												"Name": "LASER.MARKER.PART.TYPE",
												"Address": "PLC3.DB820.20,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 60,
												"nodeid": 60,
												"Name": "Laser.Marker.Lifter",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 5,
														"Name": "LASER.MARKER.LIFTER.AT.MID.POSITION",
														"Address": "PLC3.I416.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.LIFTER.LOWERED",
														"Address": "PLC3.I416.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.LIFTER.RAISED",
														"Address": "PLC3.I416.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.LIFTER.LOWER",
														"Address": "PLC3.Q460.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.LIFTER.RAISE",
														"Address": "PLC3.Q460.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 107,
														"StateName": "LASER.MARKER.LIFTER.AT.MID.POSITION",
														"Equation": "LASER.MARKER.LIFTER.AT.MID.POSITION",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 106,
														"StateName": "LASER.MARKER.LIFTER.OUTPUTS.OFF",
														"Equation": "! LASER.MARKER.LIFTER.LOWER && ! LASER.MARKER.LIFTER.RAISE && ! LASER.MARKER.LIFTER.LOWERED && ! LASER.MARKER.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 105,
														"StateName": "LASER.MARKER.LIFTER.MOVING.DOWN",
														"Equation": "LASER.MARKER.LIFTER.LOWER && ! LASER.MARKER.LIFTER.RAISE &&  ! LASER.MARKER.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 104,
														"StateName": "LASER.MARKER.LIFTER.MOVING.UP",
														"Equation": "LASER.MARKER.LIFTER.RAISE && ! LASER.MARKER.LIFTER.LOWER && ! LASER.MARKER.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 103,
														"StateName": "LASER.MARKER.LIFTER.AT.DOWN.POSITION",
														"Equation": "LASER.MARKER.LIFTER.LOWER && ! LASER.MARKER.LIFTER.RAISE && LASER.MARKER.LIFTER.LOWERED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 102,
														"StateName": "LASER.MARKER.LIFTER.AT.UP.POSITION",
														"Equation": "LASER.MARKER.LIFTER.RAISE && ! LASER.MARKER.LIFTER.LOWER && LASER.MARKER.LIFTER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Lifter",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.LIFTER.LOWER",
														"Address": "PLC3.Q460.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.LIFTER.RAISE",
														"Address": "PLC3.Q460.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.LIFTER.LOWERED",
														"Address": "PLC3.I416.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.LIFTER.RAISED",
														"Address": "PLC3.I416.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											},
											{
												"id": 61,
												"nodeid": 61,
												"Name": "Laser.Marker.Wedge.Cylinder",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACTED.2",
														"Address": "PLC3.I416.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCED.2",
														"Address": "PLC3.I416.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													},
													{
														"Id": 5,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACTED.1",
														"Address": "PLC3.I416.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 5
													},
													{
														"Id": 6,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCED.1",
														"Address": "PLC3.I416.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 6
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACT",
														"Address": "PLC3.Q460.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE",
														"Address": "PLC3.Q460.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 116,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.ALL.OFF",
														"Equation": "! LASER.MARKER.WEDGE.CYLINDER.RETRACT && ! LASER.MARKER.WEDGE.CYLINDER.ADVANCE && ! LASER.MARKER.WEDGE.CYLINDER.RETRACTED.2 && ! LASER.MARKER.WEDGE.CYLINDER.ADVANCED.2 && ! LASER.MARKER.WEDGE.CYLINDER.RETRACTED.1 && ! LASER.MARKER.WEDGE.CYLINDER.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 115,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.MOVING.TO.RETRACTED.2",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.RETRACT && ! LASER.MARKER.WEDGE.CYLINDER.RETRACTED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 114,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.MOVING.TO.RETRACTED.1",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.RETRACT && ! LASER.MARKER.WEDGE.CYLINDER.RETRACTED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 113,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.MOVING.TO.ADVANCE.2",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE && ! LASER.MARKER.WEDGE.CYLINDER.ADVANCED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 112,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.MOVING.TO.ADVANCE.1",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE && ! LASER.MARKER.WEDGE.CYLINDER.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 111,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.AT.RETRACTED.2",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.RETRACT && LASER.MARKER.WEDGE.CYLINDER.RETRACTED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 110,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.AT.RETRACTED.1",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.RETRACT && LASER.MARKER.WEDGE.CYLINDER.RETRACTED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 109,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.AT.ADVANCED2",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE && LASER.MARKER.WEDGE.CYLINDER.ADVANCED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 108,
														"StateName": "LASER.MARKER.WEDGE.CYLINDER.AT.ADVANCED.1",
														"Equation": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE && LASER.MARKER.WEDGE.CYLINDER.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Wedge.Cylinder",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACT",
														"Address": "PLC3.Q460.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCE",
														"Address": "PLC3.Q460.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACTED.2",
														"Address": "PLC3.I416.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCED.2",
														"Address": "PLC3.I416.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.RETRACTED.1",
														"Address": "PLC3.I416.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 6,
														"Name": "LASER.MARKER.WEDGE.CYLINDER.ADVANCED.1",
														"Address": "PLC3.I416.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											},
											{
												"id": 62,
												"nodeid": 62,
												"Name": "Laser.Marker.Part.Centering.Slide",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC3.I417.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC3.I417.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC3.Q461.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC3.Q461.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 120,
														"StateName": "LASER.MARKER.PART.CENTERING.SLIDE.AT.REVERSE",
														"Equation": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSED && ! LASER.MARKER.PART.CENTERING.SLIDE.FORWARDED && ( LASER.MARKER.PART.CENTERING.SLIDE.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 119,
														"StateName": "LASER.MARKER.PART.CENTERING.SLIDE.MOVING.REVERSE",
														"Equation": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSE && ! LASER.MARKER.PART.CENTERING.SLIDE.REVERSED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 118,
														"StateName": "LASER.MARKER.PART.CENTERING.SLIDE.AT.FORWARD",
														"Equation": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARDED && ! LASER.MARKER.PART.CENTERING.SLIDE.REVERSE && ( LASER.MARKER.PART.CENTERING.SLIDE.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Part.Centering.Slide",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 117,
														"StateName": "LASER.MARKER.PART.CENTERING.SLIDE.MOVING.FORWARD",
														"Equation": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARD && ! LASER.MARKER.PART.CENTERING.SLIDE.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Part.Centering.Slide",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSE",
														"Address": "PLC3.Q461.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARD",
														"Address": "PLC3.Q461.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.FORWARDED",
														"Address": "PLC3.I417.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.PART.CENTERING.SLIDE.REVERSED",
														"Address": "PLC3.I417.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											},
											{
												"id": 63,
												"nodeid": 63,
												"Name": "Laser.Marker.Door",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "LASER.MARKER.DOOR.RETRACTED.2",
														"Address": "PLC3.I417.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.DOOR.ADVANCED.2",
														"Address": "PLC3.I417.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													},
													{
														"Id": 5,
														"Name": "LASER.MARKER.DOOR.RETRACTED.1",
														"Address": "PLC3.I417.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 5
													},
													{
														"Id": 6,
														"Name": "LASER.MARKER.DOOR.ADVANCED.1",
														"Address": "PLC3.I417.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 6
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.DOOR.RETRACT",
														"Address": "PLC3.Q461.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.DOOR.ADVANCE",
														"Address": "PLC3.Q461.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 129,
														"StateName": "LASER.MARKER.DOOR.ALL.OFF",
														"Equation": "! LASER.MARKER.DOOR.RETRACT && ! LASER.MARKER.DOOR.ADVANCE && ! LASER.MARKER.DOOR.RETRACTED.2 && ! LASER.MARKER.DOOR.ADVANCED.2 && ! LASER.MARKER.DOOR.RETRACTED.1 && ! LASER.MARKER.DOOR.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 128,
														"StateName": "LASER.MARKER.DOOR.MOVING.TO.RETRACTED.2",
														"Equation": "LASER.MARKER.DOOR.RETRACT && ! LASER.MARKER.DOOR.RETRACTED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 127,
														"StateName": "LASER.MARKER.DOOR.MOVING.TO.RETRACTED.1",
														"Equation": "LASER.MARKER.DOOR.RETRACT && ! LASER.MARKER.DOOR.RETRACTED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 126,
														"StateName": "LASER.MARKER.DOOR.MOVING.TO.ADVANCE.2",
														"Equation": "LASER.MARKER.DOOR.ADVANCE && ! LASER.MARKER.DOOR.ADVANCED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 125,
														"StateName": "LASER.MARKER.DOOR.MOVING.TO.ADVANCE.1",
														"Equation": "LASER.MARKER.DOOR.ADVANCE && ! LASER.MARKER.DOOR.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 124,
														"StateName": "LASER.MARKER.DOOR.AT.RETRACTED.2",
														"Equation": "LASER.MARKER.DOOR.RETRACT && LASER.MARKER.DOOR.RETRACTED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 123,
														"StateName": "LASER.MARKER.DOOR.AT.RETRACTED.1",
														"Equation": "LASER.MARKER.DOOR.RETRACT && LASER.MARKER.DOOR.RETRACTED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 122,
														"StateName": "LASER.MARKER.DOOR.AT.ADVANCED2",
														"Equation": "LASER.MARKER.DOOR.ADVANCE && LASER.MARKER.DOOR.ADVANCED.2",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 121,
														"StateName": "LASER.MARKER.DOOR.AT.ADVANCED.1",
														"Equation": "LASER.MARKER.DOOR.ADVANCE && LASER.MARKER.DOOR.ADVANCED.1",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Door",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.DOOR.RETRACT",
														"Address": "PLC3.Q461.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.DOOR.ADVANCE",
														"Address": "PLC3.Q461.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.DOOR.RETRACTED.2",
														"Address": "PLC3.I417.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.DOOR.ADVANCED.2",
														"Address": "PLC3.I417.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "LASER.MARKER.DOOR.RETRACTED.1",
														"Address": "PLC3.I417.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 6,
														"Name": "LASER.MARKER.DOOR.ADVANCED.1",
														"Address": "PLC3.I417.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											},
											{
												"id": 64,
												"nodeid": 64,
												"Name": "Laser.Marker.Stopper",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "LASER.MARKER.STOPPER.PART.PRESENT",
														"Address": "PLC3.I418.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.STOPPER.RAISED",
														"Address": "PLC3.I418.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.STOPPER.LOWER",
														"Address": "PLC3.Q461.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.STOPPER.RAISE",
														"Address": "PLC3.Q461.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 134,
														"StateName": "LASER.MARKER.STOPPER.PART.PRESENT",
														"Equation": "LASER.MARKER.STOPPER.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 133,
														"StateName": "LASER.MARKER.STOPPER.MOVING.DOWN",
														"Equation": "LASER.MARKER.STOPPER.LOWER && LASER.MARKER.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 132,
														"StateName": "LASER.MARKER.STOPPER.MOVING.UP",
														"Equation": "LASER.MARKER.STOPPER.RAISE && !LASER.MARKER.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 131,
														"StateName": "LASER.MARKER.STOPPER.AT.DOWN",
														"Equation": "LASER.MARKER.STOPPER.LOWER && !LASER.MARKER.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 130,
														"StateName": "LASER.MARKER.STOPPER.AT.UP",
														"Equation": "LASER.MARKER.STOPPER.RAISE && LASER.MARKER.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Laser.Marker.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.STOPPER.LOWER",
														"Address": "PLC3.Q462.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.STOPPER.RAISE",
														"Address": "PLC3.Q462.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "LASER.MARKER.STOPPER.PART.PRESENT",
														"Address": "PLC3.I421.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "LASER.MARKER.STOPPER.RAISED",
														"Address": "PLC3.I421.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											},
											{
												"id": 81,
												"nodeid": 81,
												"Name": "Laser.Marker",
												"field": "SubAssembly",
												"coupleType": "Coupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 2,
														"Name": "LASER.MARKER.OVER",
														"Address": "PLC3.DB644.76,X0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.CMD",
														"Address": "PLC3.DB501.314,X5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													}
												],
												"States": [
													{
														"Id": 297,
														"StateName": "LASER.MARKING.OVER",
														"Equation": "LASER.MARKER.OVER && ( LASER.MARKER.CMD || ! LASER.MARKER.CMD)",
														"Priority": 0,
														"subassemblyName": "Laser.Marker",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 296,
														"StateName": "LASER.MARKING.IN.PROGRESS",
														"Equation": "LASER.MARKER.CMD && ! LASER.MARKER.OVER",
														"Priority": 0,
														"subassemblyName": "Laser.Marker",
														"statetype": "Action",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "LASER.MARKER.CMD",
														"Address": "PLC3.DB501.314,X5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "LASER.MARKER.OVER",
														"Address": "PLC3.DB644.76,X0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 27
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 28,
										"nodeid": 28,
										"Name": "Diverge.System",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 65,
												"nodeid": 65,
												"Name": "Diverge.System.Stopper",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 7,
														"Name": "DIVERGE.SYSTEM.MC.NUMBER",
														"Address": "PLC3.DB820.44,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 6,
														"Name": "DIVERGE.SYSTEM.PART.STATUS",
														"Address": "PLC3.DB820.42,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 5,
														"Name": "DIVERGE.SYSTEM.PART.TYPE",
														"Address": "PLC3.820.40,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.STOPPER.PART.PRESENT",
														"Address": "PLC3.I418.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.STOPPER.RAISED",
														"Address": "PLC3.I418.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.STOPPER.LOWER",
														"Address": "PLC3.Q461.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.STOPPER.RAISE",
														"Address": "PLC3.Q461.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 139,
														"StateName": "DIVERGE.SYSTEM.STOPPER.PART.PRESENT",
														"Equation": "DIVERGE.SYSTEM.STOPPER.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 138,
														"StateName": "DIVERGE.SYSTEM.STOPPER.MOVING.DOWN",
														"Equation": "DIVERGE.SYSTEM.STOPPER.LOWER && DIVERGE.SYSTEM.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 137,
														"StateName": "DIVERGE.SYSTEM.STOPPER.MOVING.UP",
														"Equation": "DIVERGE.SYSTEM.STOPPER.RAISE && !DIVERGE.SYSTEM.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Stopper",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 136,
														"StateName": "DIVERGE.SYSTEM.STOPPER.AT.DOWN",
														"Equation": "DIVERGE.SYSTEM.STOPPER.LOWER && !DIVERGE.SYSTEM.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 135,
														"StateName": "DIVERGE.SYSTEM.STOPPER.AT.UP",
														"Equation": "DIVERGE.SYSTEM.STOPPER.RAISE && DIVERGE.SYSTEM.STOPPER.RAISED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Stopper",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.STOPPER.LOWER",
														"Address": "PLC3.Q461.7",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.STOPPER.RAISE",
														"Address": "PLC3.Q461.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.STOPPER.PART.PRESENT",
														"Address": "PLC3.I418.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.STOPPER.RAISED",
														"Address": "PLC3.I418.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 28
											},
											{
												"id": 66,
												"nodeid": 66,
												"Name": "Diverge.System.Diverter1",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.FORWARDED",
														"Address": "PLC3.I421.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.REVERSED",
														"Address": "PLC3.I421.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.REVERSE",
														"Address": "PLC3.Q462.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.FORWARD",
														"Address": "PLC3.Q462.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 143,
														"StateName": "DIVERGE.SYSTEM.DIVERTER1.AT.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER1.REVERSED && ! DIVERGE.SYSTEM.DIVERTER1.FORWARDED && ( DIVERGE.SYSTEM.DIVERTER1.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 142,
														"StateName": "DIVERGE.SYSTEM.DIVERTER1.MOVING.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER1.REVERSE && ! DIVERGE.SYSTEM.DIVERTER1.REVERSED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 141,
														"StateName": "DIVERGE.SYSTEM.DIVERTER1.AT.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER1.FORWARDED && ! DIVERGE.SYSTEM.DIVERTER1.REVERSE && ( DIVERGE.SYSTEM.DIVERTER1.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter1",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 140,
														"StateName": "DIVERGE.SYSTEM.DIVERTER1.MOVING.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER1.FORWARD && ! DIVERGE.SYSTEM.DIVERTER1.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter1",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.REVERSE",
														"Address": "PLC3.Q462.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.FORWARD",
														"Address": "PLC3.Q462.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.FORWARDED",
														"Address": "PLC3.I421.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER1.REVERSED",
														"Address": "PLC3.I421.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 28
											},
											{
												"id": 67,
												"nodeid": 67,
												"Name": "Diverge.System.Diverter2",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.FORWARDED",
														"Address": "PLC3.I515.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.REVERSED",
														"Address": "PLC3.I515.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.REVERSE",
														"Address": "PLC3.Q560.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.FORWARD",
														"Address": "PLC3.Q560.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 147,
														"StateName": "DIVERGE.SYSTEM.DIVERTER2.AT.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER2.REVERSED && ! DIVERGE.SYSTEM.DIVERTER2.FORWARDED && ( DIVERGE.SYSTEM.DIVERTER2.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 146,
														"StateName": "DIVERGE.SYSTEM.DIVERTER2.MOVING.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER2.REVERSE && ! DIVERGE.SYSTEM.DIVERTER2.REVERSED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 145,
														"StateName": "DIVERGE.SYSTEM.DIVERTER2.AT.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER2.FORWARDED && ! DIVERGE.SYSTEM.DIVERTER2.REVERSE && ( DIVERGE.SYSTEM.DIVERTER2.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter2",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 144,
														"StateName": "DIVERGE.SYSTEM.DIVERTER2.MOVING.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER2.FORWARD && ! DIVERGE.SYSTEM.DIVERTER2.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter2",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.REVERSE",
														"Address": "PLC3.Q560.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.FORWARD",
														"Address": "PLC3.Q560.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.FORWARDED",
														"Address": "PLC3.I515.0",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER2.REVERSED",
														"Address": "PLC3.I515.1",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 28
											},
											{
												"id": 68,
												"nodeid": 68,
												"Name": "Diverge.System.Diverter3",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.FORWARDED",
														"Address": "PLC3.I515.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.REVERSED",
														"Address": "PLC3.I515.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.REVERSE",
														"Address": "PLC3.Q560.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.FORWARD",
														"Address": "PLC3.Q560.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0",
														"id": 2
													}
												],
												"States": [
													{
														"Id": 151,
														"StateName": "DIVERGE.SYSTEM.DIVERTER3.AT.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER3.REVERSED && ! DIVERGE.SYSTEM.DIVERTER3.FORWARDED && ( DIVERGE.SYSTEM.DIVERTER3.REVERSE || ! REVERSE)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter3",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 150,
														"StateName": "DIVERGE.SYSTEM.DIVERTER3.MOVING.REVERSE",
														"Equation": "DIVERGE.SYSTEM.DIVERTER3.REVERSE && ! DIVERGE.SYSTEM.DIVERTER3.REVERSED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter3",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 149,
														"StateName": "DIVERGE.SYSTEM.DIVERTER3.AT.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER3.FORWARDED && ! DIVERGE.SYSTEM.DIVERTER3.REVERSE && ( DIVERGE.SYSTEM.DIVERTER3.FORWARD || ! FORWARD)",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter3",
														"statetype": "Static",
														"SequenceNo": 0,
														"iscouplingstate": false
													},
													{
														"Id": 148,
														"StateName": "DIVERGE.SYSTEM.DIVERTER3.MOVING.FORWARD",
														"Equation": "DIVERGE.SYSTEM.DIVERTER3.FORWARD && ! DIVERGE.SYSTEM.DIVERTER3.FORWARDED",
														"Priority": 0,
														"subassemblyName": "Diverge.System.Diverter3",
														"statetype": "Action",
														"SequenceNo": 0,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.REVERSE",
														"Address": "PLC3.Q560.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.FORWARD",
														"Address": "PLC3.Q560.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Output",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.FORWARDED",
														"Address": "PLC3.I515.2",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "DIVERGE.SYSTEM.DIVERTER3.REVERSED",
														"Address": "PLC3.I515.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": true,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 28
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 29,
										"nodeid": 29,
										"Name": "Outfeed.Reject.Conveyor",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 82,
												"nodeid": 82,
												"Name": "OF.Reject.Dead.Stop",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "OF.REJECT.DEAD.STOP.PART.PRESENT",
														"Address": "PLC3.I422.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "OF.REJECT.DEAD.STOP.PART.TYPE",
														"Address": "PLC3.DB820.100,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "OF.REJECT.DEAD.STOP.PART.STATUS",
														"Address": "PLC3.DB820.102,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													},
													{
														"Id": 4,
														"Name": "OF.REJECT.DEAD.STOP.MC.NUMBER",
														"Address": "PLC3.DB820.104,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 299,
														"StateName": "OF.REJECT.DEAD.STOP.PART.ABSENT",
														"Equation": "OF.REJECT.DEAD.STOP.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "OF.Reject.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 298,
														"StateName": "OF.REJECT.DEAD.STOP.PART.PRESENT",
														"Equation": "! OF.REJECT.DEAD.STOP.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "OF.Reject.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "OF.REJECT.DEAD.STOP.PART.PRESENT",
														"Address": "PLC3.I422.3",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "OF.REJECT.DEAD.STOP.PART.TYPE",
														"Address": "PLC3.DB820.100,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "OF.REJECT.DEAD.STOP.PART.STATUS",
														"Address": "PLC3.DB820.102,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "OF.REJECT.DEAD.STOP.MC.NUMBER",
														"Address": "PLC3.DB820.104,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 29
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 30,
										"nodeid": 30,
										"Name": "Unloading.Lane1",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 83,
												"nodeid": 83,
												"Name": "Unloading.Lane1.Dead.Stop",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE1.PART.PRESENT",
														"Address": "PLC3.I422.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE1.PART.TYPE",
														"Address": "PLC3.DB820.160,X0",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE1.PART.STATUS",
														"Address": "PLC3.DB820.162,X0",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE1.MC.NUMBER",
														"Address": "PLC3.DB820.162,X0",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 301,
														"StateName": "UNLOADING.LANE1.PART.ABSENT",
														"Equation": "UNLOADING.LANE1.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane1.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 300,
														"StateName": "UNLOADING.LANE1.PART.PRESENT",
														"Equation": "! UNLOADING.LANE1.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane1.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE1.PART.PRESENT",
														"Address": "PLC3.I422.4",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE1.PART.TYPE",
														"Address": "PLC3.DB820.160,X0",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE1.PART.STATUS",
														"Address": "PLC3.DB820.162,X0",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE1.MC.NUMBER",
														"Address": "PLC3.DB820.162,X0",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 30
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 31,
										"nodeid": 31,
										"Name": "Unloading.Lane2",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 84,
												"nodeid": 84,
												"Name": "Unloading.Lane2.Dead.Stop",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE2.PART.PRESENT",
														"Address": "PLC3.I422.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE2.PART.TYPE",
														"Address": "PLC3.DB820.220,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE2.PART.STATUS",
														"Address": "PLC3.DB820.222,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE2.MC.NUMBER",
														"Address": "PLC3.DB820.224,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 303,
														"StateName": "UNLOADING.LANE2.PART.ABSENT",
														"Equation": "UNLOADING.LANE2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane2.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 302,
														"StateName": "UNLOADING.LANE2.PART.PRESENT",
														"Equation": "! UNLOADING.LANE2.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane2.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE2.PART.PRESENT",
														"Address": "PLC3.I422.5",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE2.PART.TYPE",
														"Address": "PLC3.DB820.220,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE2.PART.STATUS",
														"Address": "PLC3.DB820.222,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE2.MC.NUMBER",
														"Address": "PLC3.DB820.224,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 31
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									},
									{
										"id": 32,
										"nodeid": 32,
										"Name": "Unloading.Lane3",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [],
										"Outputs": [],
										"States": [],
										"nodes": [
											{
												"id": 85,
												"nodeid": 85,
												"Name": "Unloading.Lane3.Dead.Stop",
												"field": "SubAssembly",
												"coupleType": "Decoupled",
												"EquationStateNumbers": "",
												"isCoupled": false,
												"isvirtualSubAssembly": false,
												"Inputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE3.PART.PRESENT",
														"Address": "PLC3.I422.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 1
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE3.PART.TYPE",
														"Address": "PLC3.DB820.280,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 2
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE3.PART.STATUS",
														"Address": "PLC3.DB820.282,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 3
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE3.MC.NUMBER",
														"Address": "PLC3.DB820.284,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0",
														"id": 4
													}
												],
												"Outputs": [],
												"States": [
													{
														"Id": 305,
														"StateName": "UNLOADING.LANE3.DEAD.STOP.PART.ABSENT",
														"Equation": "UNLOADING.LANE3.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane3.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													},
													{
														"Id": 304,
														"StateName": "UNLOADING.LANE3.DEAD.STOP.PART.PRESENT",
														"Equation": "! UNLOADING.LANE3.PART.PRESENT",
														"Priority": 0,
														"subassemblyName": "Unloading.Lane3.Dead.Stop",
														"statetype": "Static",
														"SequenceNo": 0,
														"isCustom": true,
														"iscouplingstate": false
													}
												],
												"nodes": [],
												"InputsOutputs": [
													{
														"Id": 1,
														"Name": "UNLOADING.LANE3.PART.PRESENT",
														"Address": "PLC3.I422.6",
														"InputType": "Sequence",
														"DataType": "BOOL",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 2,
														"Name": "UNLOADING.LANE3.PART.TYPE",
														"Address": "PLC3.DB820.280,B",
														"InputType": "Quality",
														"DataType": "BYTE",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 3,
														"Name": "UNLOADING.LANE3.PART.STATUS",
														"Address": "PLC3.DB820.282,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													},
													{
														"Id": 4,
														"Name": "UNLOADING.LANE3.MC.NUMBER",
														"Address": "PLC3.DB820.284,I",
														"InputType": "Quality",
														"DataType": "INT",
														"IOType": "Input",
														"isCompulsory": false,
														"Min": "0",
														"Max": "0"
													}
												],
												"line": 1,
												"asset": 32
											}
										],
										"InputsOutputs": [],
										"line": 1,
										"asset": 6
									}
								],
								"InputsOutputs": [],
								"coupling": [],
								"EEA": [
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"IsLogging": false,
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Address": "",
										"IsLogging": true,
										"Type": "Shift"
									}
								]
							},
							{
								"id": 33,
								"nodeid": 33,
								"Name": "Nest.System",
								"field": "Asset",
								"coupleType": "Coupled",
								"isvirtualSubAssembly": false,
								"EquationStateNumbers": "",
								"Inputs": [],
								"Outputs": [],
								"States": [],
								"nodes": [
									{
										"id": 34,
										"nodeid": 34,
										"Name": "Nest1",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 1,
												"Name": "NEST1.PART.PRESENT",
												"Address": "PLC2.I88.0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [
											{
												"Id": 274,
												"StateName": "NEST1.PART.PRESENT",
												"Equation": "! NEST1.PART.PRESENT",
												"Priority": 0,
												"subassemblyName": "Nest1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 273,
												"StateName": "NEST1.PART.ABSENT",
												"Equation": "NEST1.PART.PRESENT",
												"Priority": 0,
												"subassemblyName": "Nest1",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 33
									},
									{
										"id": 36,
										"nodeid": 36,
										"Name": "Nest3",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 1,
												"Name": "NEST3.PART.PRESENT",
												"Address": "PLC2.I88.1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [],
										"States": [
											{
												"Id": 278,
												"StateName": "NEST3.PART.PRESENT",
												"Equation": "! NEST3.PART.PRESENT",
												"Priority": 0,
												"subassemblyName": "Nest3",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 277,
												"StateName": "NEST3.PART.ABSENT",
												"Equation": "NEST3.PART.PRESENT",
												"Priority": 0,
												"subassemblyName": "Nest3",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 33
									}
								],
								"InputsOutputs": [],
								"Image": "",
								"EEA": [
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"IsLogging": false,
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Address": "",
										"IsLogging": true,
										"Type": "Shift"
									}
								],
								"coupling": []
							},
							{
								"id": 86,
								"nodeid": 86,
								"Name": "Machine",
								"field": "Asset",
								"coupleType": "Coupled",
								"isvirtualSubAssembly": false,
								"EquationStateNumbers": "",
								"Inputs": [],
								"Outputs": [],
								"States": [],
								"nodes": [
									{
										"id": 89,
										"nodeid": 89,
										"Name": "Machine1",
										"field": "SubAssembly",
										"coupleType": "Decoupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 1,
												"Name": "M1.TOP.HATCH.CLOSE.ACK",
												"Address": "PLC2.DB622.18,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "M1.PART.TYPE.LOADED",
												"Address": "PLC2.DB622.17,B",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "M1.FLUSH.MC.FIXTURE",
												"Address": "PLC2.DB622.6,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 4,
												"Name": "M1.GANTRY.ABOVE.MACHINE",
												"Address": "PLC2.DB622.6,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 5,
												"Name": "M1.REQ.OPEN.HATCH",
												"Address": "PLC2.DB622.6,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 6,
												"Name": "M1.REQ.CLOSE.HATCH",
												"Address": "PLC2.DB622.6,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 7,
												"Name": "M1.CLAMP.FIXTURE",
												"Address": "PLC2.DB622.5,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 8,
												"Name": "M1.UNCLAMP.FIXTURE",
												"Address": "PLC2.DB622.5,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 9,
												"Name": "M1.GANTRY.LOAD.CYCLE.ACTIVE",
												"Address": "PLC2.DB622.5,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 10,
												"Name": "M1.GANTRY.UNLOAD.CYCLE.ACTIVE",
												"Address": "PLC2.DB622.5,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 11,
												"Name": "M1.GANTRY.LOADED.PART",
												"Address": "PLC2.DB622.5,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 12,
												"Name": "M1.GANTRY.UNLDED.PART",
												"Address": "PLC2.DB622.5,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 13,
												"Name": "M1.GNTRY.CLEAR.MC.FIXTURE",
												"Address": "PLC2.DB622.5,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 14,
												"Name": "M1.GNTRY.CLEAR.OFF.MACHINE",
												"Address": "PLC2.DB622.5,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 15,
												"Name": "M1.REQ.ADVANCE.FICTURE",
												"Address": "PLC2.DB622.3,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 16,
												"Name": "M1.REQ.RETRACT.FIXTURE",
												"Address": "PLC2.DB622.3,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 17,
												"Name": "M1.GANTRY.FAULTED",
												"Address": "PLC2.DB622.3,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 18,
												"Name": "M1.REQ.RUNOUT",
												"Address": "PLC2.DB622.2,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 19,
												"Name": "M1.ACK.OPEN.SAFETY.DOOR",
												"Address": "PLC2.DB622.1,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 20,
												"Name": "M1.ESTOP.OK",
												"Address": "PLC2.DB622.1,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 1,
												"Name": "M1.REQ.CLAMPING.REPETITION",
												"Address": "PLC2.DB621.8,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 2,
												"Name": "M1.PART.REJ.PART.SEATING.FAULT",
												"Address": "PLC2.DB621.8,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 3,
												"Name": "M1.TOOL.BROKEN",
												"Address": "PLC2.DB621.8,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 4,
												"Name": "M1.ACK.FLUSH.MC.FIXTURE",
												"Address": "PLC2.DB621.6,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 5,
												"Name": "M1.PART.SEATING.NOK",
												"Address": "PLC2.DB621.6,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 6,
												"Name": "M1.PART.SEATING.OK",
												"Address": "PLC2.DB621.6,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 7,
												"Name": "M1.ANTICIPATION.REQ.TO.UNLOAD",
												"Address": "PLC2.DB621.6,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 8,
												"Name": "M1.HATCH.OPENED",
												"Address": "PLC2.DB621.6,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 9,
												"Name": "M1.HATCH.CLOSED",
												"Address": "PLC2.DB.621.6,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 10,
												"Name": "M1.FIXTURE.UNCLAMPED",
												"Address": "PLC2.DB621.5,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 11,
												"Name": "M1.FIXTURE.CLAMPED",
												"Address": "PLC2.DB621.5,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 12,
												"Name": "M1.LOAD.PART.ENABLE",
												"Address": "PLC2.DB621.5,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 13,
												"Name": "M1.UNLOAD.PART.ENABLE",
												"Address": "PLC2.DB621.5,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 14,
												"Name": "M1.FIXTURE.ADVANCED",
												"Address": "PLC2.DB621.3,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 15,
												"Name": "M1.FIXTURE.RETRACTED",
												"Address": "PLC2.DB621.3,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 16,
												"Name": "M1.HOMING.ACTIVE",
												"Address": "PLC2.DB621.3,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 17,
												"Name": "M1.GANTRY.RETURN.TO.HOME.POSITION",
												"Address": "PLC2.DB621.3,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 18,
												"Name": "M1.HOME.POSITION",
												"Address": "PLC2.DB621.3,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 19,
												"Name": "M1.FAULT.AT.MC",
												"Address": "PLC2.DB621.3,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 20,
												"Name": "M1.MC.REQ.RUNOUT",
												"Address": "PLC2.DB621.2,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 21,
												"Name": "M1.STOP.AT.END.OF.CYCLE",
												"Address": "PLC2.DB621.1,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 22,
												"Name": "M1.ALL.ESTOP.OK",
												"Address": "PLC2.DB621.1,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 23,
												"Name": "M1.REQ.MC.SAFETY.DOOR",
												"Address": "PLC2.DB621.0,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 24,
												"Name": "M1.SAFETY.DOORS.CLOSED",
												"Address": "PLC2.DB621.0,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 328,
												"StateName": "M1.HATCH.CLOSED",
												"Equation": "M1.HATCH.CLOSED && ( M1.REQ.CLOSE.HATCH || ! M1.REQ.CLOSE.HATCH)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 327,
												"StateName": "M1.CLOSE.HATCH",
												"Equation": "M1.REQ.CLOSE.HATCH && ! M1.HATCH.CLOSED",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 326,
												"StateName": "M1.HATCH.OPENED",
												"Equation": "M1.HATCH.OPENED && ( M1.REQ.OPEN.HATCH || ! M1.REQ.OPEN.HATCH)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 325,
												"StateName": "M1.OPEN.HATCH",
												"Equation": "M1.REQ.OPEN.HATCH && ! M1.HATCH.OPENED",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 324,
												"StateName": "M1.FIXTURE.UNCLAMPED",
												"Equation": "M1.FIXTURE.UNCLAMPED && ! ( M1.UNCLAMP.FIXTURE || ! M1.UNCLAMP.FIXTURE)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 323,
												"StateName": "M1.UNCLAMP.FIXTURE",
												"Equation": "M1.UNCLAMP.FIXTURE && ! M1.FIXTURE.UNCLAMPED",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 322,
												"StateName": "M1.FIXTURE.CLAMPED",
												"Equation": "M1.FIXTURE.CLAMPED && ( M1.CLAMP.FIXTURE || ! M1.CLAMP.FIXTURE)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 321,
												"StateName": "M1.CLAMP.FIXTURE",
												"Equation": "M1.CLAMP.FIXTURE && ! M1.FIXTURE.CLAMPED",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 320,
												"StateName": "M1.ACK.END.OF.CYCLE",
												"Equation": "M1.ACK.END.OF.CYCLE && ! ( M1.STOP.AT.END.OF.CYCLE  || ! M1.STOP.AT.END.OF.CYCLE)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Static",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 319,
												"StateName": "M1.STOP.AT.END.OF.CYCLE",
												"Equation": "M1.STOP.AT.END.OF.CYCLE && ! M1.ACK.END.OF.CYCLE",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 318,
												"StateName": "M1.SAFETY.DOOR.OPENED",
												"Equation": "M1.ACK.OPEN.SAFETY.DOOR && ! M1.SAFETY.DOORS.CLOSED && ( M1.REQ.MC.SAFETY.DOOR || ! M1.REQ.MC.SAFETY.DOOR)",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 317,
												"StateName": "M1.SAFETY.DOOR.OPEN.CMD",
												"Equation": "M1.REQ.MC.SAFETY.DOOR && ! M1.ACK.OPEN.SAFETY.DOOR",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "Action",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"coupling": [],
										"EEA": [
											{
												"_id": "5b2cd24ee4b0cae581dee1b7",
												"eeId": 2,
												"Name": "Lunch Break",
												"Unit": "INT",
												"Min": "1530685800000",
												"Max": "1530687600000",
												"IsLogging": false,
												"Type": "Breakdown"
											},
											{
												"_id": "5b2cd221e4b0cae581dee1b4",
												"eeId": 1,
												"Name": "First Shift",
												"Unit": "DATETIME",
												"Min": "1529638200000",
												"Max": "1529659800000",
												"Address": "",
												"IsLogging": true,
												"Type": "Shift"
											},
											{
												"_id": "5b2b7856e4b0ca8345971e12",
												"eeId": 3,
												"Name": "Test Articats",
												"Unit": "DATETIME",
												"Min": "1529535600000",
												"Max": "1529539200000",
												"Type": "Quality"
											},
											{
												"_id": "5b2a151ce4b07b59adf08d18",
												"eeId": 2,
												"Name": "First Shift",
												"Unit": "DATETIME",
												"Min": "1529465400475",
												"Max": "1529487000475",
												"Type": "Shift"
											},
											{
												"_id": "5b29eb1ee4b0d4392184f91a",
												"eeId": 1,
												"Name": "First Shift Break 1",
												"Unit": "DATETIME",
												"Min": "1529476200258",
												"Max": "1529478000258",
												"Type": "Breakdown"
											}
										],
										"line": 1,
										"asset": 86
									},
									{
										"id": 90,
										"nodeid": 90,
										"Name": "Machine3",
										"field": "SubAssembly",
										"coupleType": "Coupled",
										"EquationStateNumbers": "",
										"isCoupled": false,
										"isvirtualSubAssembly": false,
										"Inputs": [
											{
												"Id": 1,
												"Name": "M3.TOP.HATCH.CLOSE.ACK",
												"Address": "PLC2.DB622.134,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 2,
												"Name": "M3.PART.TYPE.LOADED",
												"Address": "PLC2.DB622.133,X0",
												"InputType": "Quality",
												"DataType": "BYTE",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 3,
												"Name": "M3.FLUSH.MC.FIXTURE",
												"Address": "PLC2.DB622.122,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 4,
												"Name": "M3.GNTRY.ABOVE.MC",
												"Address": "PLC2.DB622.122,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 5,
												"Name": "M3.REQ.OPEN.HATCH",
												"Address": "PLC2.DB622.122,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 6,
												"Name": "M3.REQ.CLOSE.HATCH",
												"Address": "PLC2.DB622.122,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 7,
												"Name": "M3.CLAMP.FIXTURE",
												"Address": "PLC2.DB622.121,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 8,
												"Name": "M3.UNCLAMP.FIXTURE",
												"Address": "PLC2.DB622.121,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 9,
												"Name": "M3.GNTRY.LOAD.CYCLE.ACTIVE",
												"Address": "PLC2.DB622.121,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 10,
												"Name": "M3.GNTRY.UNLOAD.CYCLE.ACTIVE",
												"Address": "PLC2.DB622.121,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 11,
												"Name": "M3.GNTRY.LOADED.PART",
												"Address": "PLC2.DB622.121,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 12,
												"Name": "M3.GNTRY.UNLDED.PART",
												"Address": "PLC2.DB622.121,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 13,
												"Name": "M3.GNTRY.CLEAR.MC.FIXTURE",
												"Address": "PLC2.DB622.121,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 14,
												"Name": "M3.GNTRY.CLEAR.OFF.MC",
												"Address": "PLC2.DB622.121,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 15,
												"Name": "M3.REQ.ADVANCE.FIXTURE",
												"Address": "PLC2.DB622.119,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 16,
												"Name": "M3.REQ.RETRACT.FIXTURE",
												"Address": "PLC2.DB622.119,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 17,
												"Name": "M3.GANTRY.FAULTED",
												"Address": "PLC2.DB622.119,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 18,
												"Name": "M3.REQ.RUNOUT",
												"Address": "PLC2.DB622.118,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 19,
												"Name": "M3.ACK.OPEN.SAFETY.DOOR",
												"Address": "PLC2.DB622.117,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											},
											{
												"Id": 20,
												"Name": "M3.ESTOP.OK",
												"Address": "PLC2.DB622.117,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Input",
												"Min": "0",
												"Max": "0"
											}
										],
										"Outputs": [
											{
												"Id": 1,
												"Name": "M3.REQ.CLAMPING.REPETITION",
												"Address": "PLC2.DB621.124,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 2,
												"Name": "M3.REJECT.PART.SEATING.FAULT",
												"Address": "PLC2.DB621.124,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 3,
												"Name": "M3.TOOL.BROKEN",
												"Address": "PLC2.DB621.124,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 4,
												"Name": "M3.ACK.FLUSH.MC.FIXTURE",
												"Address": "PLC2.DB621.122,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 5,
												"Name": "M3.PART.SEATING.NOK",
												"Address": "PLC2.DB621.122,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 6,
												"Name": "M3.PART.SEATING.OK",
												"Address": "PLC2.DB621.122,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 7,
												"Name": "M3.ANTICIPATION.REQ",
												"Address": "PLC2.DB621.122,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 8,
												"Name": "M3.HATCH.OPENED",
												"Address": "PLC2.DB621.122,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 9,
												"Name": "M3.HATCH.CLOSED",
												"Address": "PLC2.DB621.122,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 10,
												"Name": "M3.FIXTURE.UNCLAMPED",
												"Address": "PLC2.DB621.121,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 11,
												"Name": "M3.FIXTURE.CLAMPED",
												"Address": "PLC2.DB621.121,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 12,
												"Name": "M3.LOAD.PART.ENABLE",
												"Address": "PLC2.DB621.121,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 13,
												"Name": "M3.UNLOAD.PART.ENABLE",
												"Address": "PLC2.DB621.121,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 14,
												"Name": "M3.FIXTURE.ADVANCED",
												"Address": "PLC2.DB621.119,X7",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 15,
												"Name": "M3.FIXTURE.RETRACTED",
												"Address": "PLC2.DB621.119,X6",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 16,
												"Name": "M3.HOMING.ACTIVE",
												"Address": "PLC2.DB621.119,X3",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 17,
												"Name": "M3.GANTRY.RETURN.TO.HOME.POSITION",
												"Address": "PLC2.DB621.119,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 18,
												"Name": "M3.HOME.POSITION",
												"Address": "PLC2.DB621.119,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 19,
												"Name": "M3.FAULT.AT.MC",
												"Address": "PLC2.DB621.119,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 20,
												"Name": "M3.MC.REQ.RUNOUT",
												"Address": "PLC2.DB621.118,X4",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 21,
												"Name": "M3.STOP.AT.END.OF.CYCLE",
												"Address": "PLC2.DB621.117,X1",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 22,
												"Name": "M3.ALL.ESTOPS.OK",
												"Address": "PLC2.DB621.117,X0",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 23,
												"Name": "M3.REQ.OPEN.SAFETY.DOOR",
												"Address": "PLC2.DB621.116,X5",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											},
											{
												"Id": 24,
												"Name": "M3.SAFETY.DOORS.CLOSED",
												"Address": "PLC2.DB621.116,X2",
												"InputType": "Sequence",
												"DataType": "BOOL",
												"IOType": "Output",
												"type": "Output"
											}
										],
										"States": [
											{
												"Id": 272,
												"StateName": "M3.STOP.AT.END.OF.CYCLE",
												"Equation": "M3.STOP.AT.END.OF.CYCLE",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "System",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											},
											{
												"Id": 271,
												"StateName": "M3.READY.TOSTART",
												"Equation": "M3.READY.TOSTART",
												"Priority": 0,
												"subassemblyName": "Asset",
												"statetype": "System",
												"SequenceNo": 0,
												"isCustom": true,
												"iscouplingstate": false
											}
										],
										"nodes": [],
										"InputsOutputs": [],
										"line": 1,
										"asset": 86
									}
								],
								"InputsOutputs": [],
								"EEA": [
									{
										"_id": "5b2b7856e4b0ca8345971e12",
										"eeId": 3,
										"Name": "Test Articats",
										"Unit": "DATETIME",
										"Min": "1529535600000",
										"Max": "1529539200000",
										"Type": "Quality"
									},
									{
										"_id": "5b2a151ce4b07b59adf08d18",
										"eeId": 2,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529465400475",
										"Max": "1529487000475",
										"Type": "Shift"
									},
									{
										"_id": "5b29eb1ee4b0d4392184f91a",
										"eeId": 1,
										"Name": "First Shift Break 1",
										"Unit": "DATETIME",
										"Min": "1529476200258",
										"Max": "1529478000258",
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd24ee4b0cae581dee1b7",
										"eeId": 2,
										"Name": "Lunch Break",
										"Unit": "INT",
										"Min": "1530685800000",
										"Max": "1530687600000",
										"Type": "Breakdown"
									},
									{
										"_id": "5b2cd221e4b0cae581dee1b4",
										"eeId": 1,
										"Name": "First Shift",
										"Unit": "DATETIME",
										"Min": "1529638200000",
										"Max": "1529659800000",
										"Type": "Shift"
									}
								],
								"coupling": []
							}
						],
						"coupling": []
					}
				],
				"coupling": []
			},
			"modifiedtimestamp": "07-08-2018:20:16:46",
			"monthId": 5,
			"remark": "Sterling",
			"siteId": 652,
			"siteName": "detroit",
			"userId": 13,
			"weekId": 26,
			"questions": [
				"remark",
				"lineid",
				"isactive",
				"isuploadcompleted",
				"linemodel",
				"configid"
			]
		}
	],
	"totalCount": 1
}


let allArtifacts = { "elementName": "artifactstemplate", "results": [{ "_id": "5b360ccbe4b0cae581def977", "artifactid": 7, "name": "1_Output_2_Inputs", "description": "Cylinder with 1 output and 2 inputs", "inputsoutputs": "[{\"Id\":6,\"Name\":\"RETRACT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:533\"},{\"Id\":5,\"Name\":\"ADVANCE\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:475\"},{\"Id\":4,\"Name\":\"RETRACTED.2\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:417\"},{\"Id\":3,\"Name\":\"ADVANCED.2\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:359\"},{\"Id\":2,\"Name\":\"RETRACTED.1\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:301\"},{\"Id\":1,\"Name\":\"ADVANCED.1\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:240\"}]", "states": "[{\"Id\":1,\"Name\":\"AT.ADVANCED.1\",\"statetype\":\"Static\",\"Equation\":\"ADVANCE && ADVANCED.1\",\"$$hashKey\":\"object:605\"},{\"Id\":2,\"Name\":\"AT.ADVANCED2\",\"statetype\":\"Static\",\"Equation\":\"ADVANCE && ADVANCED.2\",\"$$hashKey\":\"object:681\"},{\"Id\":3,\"Name\":\"AT.RETRACTED.1\",\"statetype\":\"Static\",\"Equation\":\"RETRACT && RETRACTED.1\",\"$$hashKey\":\"object:754\"},{\"Id\":4,\"Name\":\"AT.RETRACTED.2\",\"statetype\":\"Static\",\"Equation\":\"RETRACT && RETRACTED.2\",\"$$hashKey\":\"object:827\"},{\"Id\":5,\"Name\":\"MOVING.TO.ADVANCE.1\",\"statetype\":\"Action\",\"Equation\":\"ADVANCE && ! ADVANCED.1\",\"$$hashKey\":\"object:900\"},{\"Id\":6,\"Name\":\"MOVING.TO.ADVANCE.2\",\"statetype\":\"Action\",\"Equation\":\"ADVANCE && ! ADVANCED.2\",\"$$hashKey\":\"object:972\"},{\"Id\":7,\"Name\":\"MOVING.TO.RETRACTED.1\",\"statetype\":\"Action\",\"Equation\":\"RETRACT && ! RETRACTED.1\",\"$$hashKey\":\"object:1044\"},{\"Id\":8,\"Name\":\"MOVING.TO.RETRACTED.2\",\"statetype\":\"Action\",\"Equation\":\"RETRACT && ! RETRACTED.2\",\"$$hashKey\":\"object:1116\"},{\"Id\":9,\"Name\":\"ALL.OFF\",\"statetype\":\"Action\",\"Equation\":\"! RETRACT && ! ADVANCE && ! RETRACTED.2 && ! ADVANCED.2 && ! RETRACTED.1 && ! ADVANCED.1\",\"$$hashKey\":\"object:1192\"}]", "siteName": "detroit", "elementName": "artifactstemplate", "siteId": 652, "elementId": 14525, "customerId": 13, "userId": 13, "createdTimestamp": "29-06-2018:16:11:15", "modifiedtimestamp": "29-06-2018:16:11:15", "weekId": 26, "monthId": 5, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b345bbde4b0cae581def0a7", "artifactid": 6, "createdTimestamp": "28-06-2018:09:23:33", "customerId": 13, "description": "Lifter", "elementId": 14525, "elementName": "artifactstemplate", "inputsoutputs": "[{\"Id\":4,\"Name\":\"LOWER\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:502\"},{\"Id\":3,\"Name\":\"RAISE\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:393\"},{\"Id\":2,\"Name\":\"LOWERED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:335\"},{\"Id\":1,\"Name\":\"RAISED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:274\"}]", "modifiedtimestamp": "29-06-2018:15:52:02", "monthId": 5, "name": "Lifter", "siteId": 652, "siteName": "detroit", "states": "[{\"Id\":1,\"Name\":\"AT.UP.POSITION\",\"statetype\":\"Static\",\"Equation\":\"RAISE && ! LOWER && RAISED\",\"$$hashKey\":\"object:572\"},{\"Id\":2,\"Name\":\"AT.DOWN.POSITION\",\"statetype\":\"Static\",\"Equation\":\"LOWER && ! RAISE && LOWERED\",\"$$hashKey\":\"object:644\"},{\"Id\":3,\"Name\":\"MOVING.UP\",\"statetype\":\"Action\",\"Equation\":\"RAISE && ! LOWER && ! RAISED\",\"$$hashKey\":\"object:713\"},{\"Id\":4,\"Name\":\"MOVING.DOWN\",\"statetype\":\"Action\",\"Equation\":\"LOWER && ! RAISE &&  ! LOWERED\",\"$$hashKey\":\"object:783\"},{\"Id\":5,\"Name\":\"OUTPUTS.OFF\",\"statetype\":\"Static\",\"Equation\":\"! LOWER && ! RAISE && ! LOWERED && ! RAISED\",\"$$hashKey\":\"object:921\"}]", "userId": 13, "weekId": 26, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b335923e4b0cae581deef80", "artifactid": 5, "name": "Escapement Stopper", "description": "Escapement Stopper", "inputsoutputs": "[{\"Id\":5,\"Name\":\"LOWER\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:497\"},{\"Id\":4,\"Name\":\"RAISE\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:439\"},{\"Id\":3,\"Name\":\"PART.PASS\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:381\"},{\"Id\":2,\"Name\":\"PART.PRESENT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:323\"},{\"Id\":1,\"Name\":\"RAISED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:262\"}]", "states": "[{\"Id\":1,\"Name\":\"ESCAPEMENT.STOPPER.UP.WITH.PART.PRESENT\",\"statetype\":\"Static\",\"Equation\":\"RAISE && RAISED && ! PART.PRESENT\",\"$$hashKey\":\"object:567\"},{\"Id\":2,\"Name\":\"ESCAPEMENT.STOPPER.DOWN.WITH.PART.PRESENT\",\"statetype\":\"Static\",\"Equation\":\"LOWER && ! RAISED && !  PART.PRESENT\",\"$$hashKey\":\"object:641\"},{\"Id\":3,\"Name\":\"ESCAPEMENT.STOPPER.UP.WITH.PART.ABSENT\",\"statetype\":\"Static\",\"Equation\":\"RAISE && RAISED &&  PART.PRESENT\",\"$$hashKey\":\"object:908\"},{\"Id\":4,\"Name\":\"ESCAPEMENT.STOPPER.DOWN.WITH.PART.ABSENT\",\"statetype\":\"Static\",\"Equation\":\"LOWER && !  RAISED && PART.PRESENT\",\"$$hashKey\":\"object:1224\"},{\"Id\":5,\"Name\":\"ESCAPEMENT.STOPPER.MOVING.UP.WITH.PART.PRESENT\",\"statetype\":\"Action\",\"Equation\":\"RAISE && ! RAISED && ! PART.PRESENT\",\"$$hashKey\":\"object:1295\"},{\"Id\":6,\"Name\":\"ESCAPEMENT.STOPPER.MOVING.UP.WITH.PART.ABSENT\",\"statetype\":\"Action\",\"Equation\":\"RAISE && ! RAISED && PART.PRESENT\",\"$$hashKey\":\"object:1368\"},{\"Id\":7,\"Name\":\"ESCAPEMENT.STOPPER.MOVING.DOWN.WITH.PART.PRESENT\",\"statetype\":\"Action\",\"Equation\":\"LOWER && RAISED && ! PART.PRESENT\",\"$$hashKey\":\"object:1439\"},{\"Id\":8,\"Name\":\"ESCAPMENT.STOPPER.MOVING.DOWN.WITH.PART.ABSENT\",\"statetype\":\"Action\",\"Equation\":\"LOWER && RAISED && PART.PRESENT\",\"$$hashKey\":\"object:1510\"}]", "siteName": "detroit", "elementName": "artifactstemplate", "siteId": 652, "elementId": 14525, "customerId": 13, "userId": 13, "createdTimestamp": "27-06-2018:15:00:11", "modifiedtimestamp": "27-06-2018:15:00:11", "weekId": 26, "monthId": 5, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b32115be4b0cae581deed27", "artifactid": 4, "createdTimestamp": "26-06-2018:15:41:39", "customerId": 13, "description": "Accumulating Stopper", "elementId": 14525, "elementName": "artifactstemplate", "inputsoutputs": "[{\"Id\":4,\"Name\":\"LOWER\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:20712\"},{\"Id\":3,\"Name\":\"RAISE\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:20654\"},{\"Id\":2,\"Name\":\"PART.PRESENT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:20596\"},{\"Id\":1,\"Name\":\"RAISED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:20535\"}]", "modifiedtimestamp": "29-06-2018:17:34:40", "monthId": 5, "name": "Accumulating Stopper", "siteId": 652, "siteName": "detroit", "states": "[{\"Id\":1,\"Name\":\"AT.UP\",\"statetype\":\"Static\",\"Equation\":\"RAISE && RAISED\",\"$$hashKey\":\"object:20839\"},{\"Id\":2,\"Name\":\"AT.DOWN\",\"statetype\":\"Static\",\"Equation\":\"LOWER && !RAISED\",\"$$hashKey\":\"object:20911\"},{\"Id\":3,\"Name\":\"MOVING.UP\",\"statetype\":\"Action\",\"Equation\":\"RAISE && !RAISED\",\"$$hashKey\":\"object:21038\"},{\"Id\":4,\"Name\":\"MOVING.DOWN\",\"statetype\":\"Action\",\"Equation\":\"LOWER && RAISED\",\"$$hashKey\":\"object:21106\"},{\"Id\":5,\"Name\":\"PART.PRESENT\",\"statetype\":\"Static\",\"Equation\":\"PART.PRESENT\",\"$$hashKey\":\"object:21170\"}]", "userId": 13, "weekId": 26, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b1f49a5e4b018ea8d4879b3", "artifactid": 3, "createdTimestamp": "12-06-2018:09:48:45", "customerId": 13, "elementId": 14525, "elementName": "artifactstemplate", "inputsoutputs": "[{\"Id\":3,\"Name\":\"SLIDE.FORWARD.INPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:491\"},{\"Id\":2,\"Name\":\"SLIDE.REVERSE.INPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:433\"},{\"Id\":1,\"Name\":\"SLIDE.OUTPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:369\"}]", "modifiedtimestamp": "12-06-2018:09:49:47", "monthId": 5, "name": "CYLINDER-2I/1O", "siteId": 652, "siteName": "detroit", "states": "[{\"Id\":1,\"Name\":\"SLIDE.MOVING.FORWARD\",\"statetype\":\"Action\",\"Equation\":\"SLIDE.OUTPUT && ! SLIDE.FORWARD.INPUT\",\"$$hashKey\":\"object:556\"},{\"Id\":2,\"Name\":\"SLIDE.MOVING.REVERSE\",\"statetype\":\"Action\",\"Equation\":\"SLIDE.OUTPUT && ! SLIDE.REVERSE.INPUT\",\"$$hashKey\":\"object:625\"},{\"Id\":3,\"Name\":\"SLIDE.AT.FORWARD\",\"statetype\":\"Static\",\"Equation\":\"SLIDE.FORWARD.INPUT\",\"$$hashKey\":\"object:690\"},{\"Id\":4,\"Name\":\"SLIDE.AT.REVERSE\",\"statetype\":\"Static\",\"Equation\":\"SLIDE.REVERSE.INPUT\",\"$$hashKey\":\"object:751\"}]", "userId": 13, "weekId": 24, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b1f4838e4b018ea8d487959", "artifactid": 2, "name": "RFID", "inputsoutputs": "[{\"Id\":4,\"Name\":\"RFID.READ.OUTPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:2395\"},{\"Id\":3,\"Name\":\"RFID.WRITE.OUTPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:2337\"},{\"Id\":2,\"Name\":\"RFID.WRITE.INPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:2279\"},{\"Id\":1,\"Name\":\"RFID.READ.INPUT\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:2221\"}]", "states": "[{\"Id\":1,\"Name\":\"RFID.READING\",\"statetype\":\"Action\",\"Equation\":\"RFID.READ.OUTPUT && ! RFID.READ.INPUT && ! RFID.WRITE.OUTPUT\",\"$$hashKey\":\"object:2463\"},{\"Id\":2,\"Name\":\"RFID.READ.OVER\",\"statetype\":\"Static\",\"Equation\":\"RFID.READ.INPUT && ! RFID.WRITE.OUTPUT && ! RFID.READ.OUTPUT\",\"$$hashKey\":\"object:2529\"},{\"Id\":3,\"Name\":\"RFID.WRITE.OVER\",\"statetype\":\"Static\",\"Equation\":\"RFID.WRITE.INPUT && ! RFID.READ.INPUT && ! RFID.WRITE.INPUT\",\"$$hashKey\":\"object:2661\"},{\"Id\":4,\"Name\":\"RFID.WRITING\",\"statetype\":\"Action\",\"Equation\":\"RFID.WRITE.OUTPUT && ! RFID.WRITE.INPUT && ! RFID.READ.OUTPUT\",\"$$hashKey\":\"object:2792\"}]", "siteName": "detroit", "elementName": "artifactstemplate", "siteId": 652, "elementId": 14525, "customerId": 13, "userId": 13, "createdTimestamp": "12-06-2018:09:42:40", "modifiedtimestamp": "12-06-2018:09:42:40", "weekId": 24, "monthId": 5, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }, { "_id": "5b14f2aee4b018ea8d4857dd", "artifactid": 1, "createdTimestamp": "04-06-2018:13:35:02", "customerId": 13, "description": "Two inputs and two outputs", "elementId": 14525, "elementName": "artifactstemplate", "inputsoutputs": "[{\"Name\":\"REVERSE\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:1514\"},{\"Name\":\"FORWARD\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Output\",\"$$hashKey\":\"object:1453\"},{\"Name\":\"FORWARDED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:1218\"},{\"Name\":\"REVERSED\",\"InputType\":\"Sequence\",\"DataType\":\"BOOL\",\"IOType\":\"Input\",\"$$hashKey\":\"object:989\"}]", "modifiedtimestamp": "28-06-2018:12:14:22", "monthId": 5, "name": "CYLINDER-2I/2O", "siteId": 652, "siteName": "detroit", "states": "[{\"Id\":1,\"Name\":\"MOVING.FORWARD\",\"statetype\":\"Action\",\"Equation\":\"FORWARD && ! FORWARDED\",\"$$hashKey\":\"object:227\"},{\"Id\":2,\"Name\":\"AT.FORWARD\",\"statetype\":\"Static\",\"Equation\":\"FORWARDED && ! REVERSE && ( FORWARD || ! FORWARD)\",\"$$hashKey\":\"object:294\"},{\"Id\":3,\"Name\":\"MOVING.REVERSE\",\"statetype\":\"Action\",\"Equation\":\"REVERSE && ! REVERSED\",\"$$hashKey\":\"object:427\"},{\"Id\":4,\"Name\":\"AT.REVERSE\",\"statetype\":\"Static\",\"Equation\":\"REVERSED && ! FORWARDED && ( REVERSE || ! REVERSE)\",\"$$hashKey\":\"object:658\"}]", "userId": 13, "weekId": 23, "questions": ["artifactid", "name", "description", "states", "inputsoutputs"] }], "totalCount": 7 }

let artifactList = {
	"elementName": "artifactstemplate",
	"results": [
		{
			"_id": "5b360ccbe4b0cae581def977",
			"artifactid": 7,
			"name": "1_Output_2_Inputs",
			"description": "Cylinder with 1 output and 2 inputs",
			"inputsoutputs": [
				{
					"Id": 6,
					"Name": "RETRACT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:533"
				},
				{
					"Id": 5,
					"Name": "ADVANCE",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:475"
				},
				{
					"Id": 4,
					"Name": "RETRACTED.2",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:417"
				},
				{
					"Id": 3,
					"Name": "ADVANCED.2",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:359"
				},
				{
					"Id": 2,
					"Name": "RETRACTED.1",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:301"
				},
				{
					"Id": 1,
					"Name": "ADVANCED.1",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:240"
				}
			],
			"states": [
				{
					"Id": 1,
					"Name": "AT.ADVANCED.1",
					"statetype": "Static",
					"Equation": "ADVANCE && ADVANCED.1",
					"$$hashKey": "object:605"
				},
				{
					"Id": 2,
					"Name": "AT.ADVANCED2",
					"statetype": "Static",
					"Equation": "ADVANCE && ADVANCED.2",
					"$$hashKey": "object:681"
				},
				{
					"Id": 3,
					"Name": "AT.RETRACTED.1",
					"statetype": "Static",
					"Equation": "RETRACT && RETRACTED.1",
					"$$hashKey": "object:754"
				},
				{
					"Id": 4,
					"Name": "AT.RETRACTED.2",
					"statetype": "Static",
					"Equation": "RETRACT && RETRACTED.2",
					"$$hashKey": "object:827"
				},
				{
					"Id": 5,
					"Name": "MOVING.TO.ADVANCE.1",
					"statetype": "Action",
					"Equation": "ADVANCE && ! ADVANCED.1",
					"$$hashKey": "object:900"
				},
				{
					"Id": 6,
					"Name": "MOVING.TO.ADVANCE.2",
					"statetype": "Action",
					"Equation": "ADVANCE && ! ADVANCED.2",
					"$$hashKey": "object:972"
				},
				{
					"Id": 7,
					"Name": "MOVING.TO.RETRACTED.1",
					"statetype": "Action",
					"Equation": "RETRACT && ! RETRACTED.1",
					"$$hashKey": "object:1044"
				},
				{
					"Id": 8,
					"Name": "MOVING.TO.RETRACTED.2",
					"statetype": "Action",
					"Equation": "RETRACT && ! RETRACTED.2",
					"$$hashKey": "object:1116"
				},
				{
					"Id": 9,
					"Name": "ALL.OFF",
					"statetype": "Action",
					"Equation": "! RETRACT && ! ADVANCE && ! RETRACTED.2 && ! ADVANCED.2 && ! RETRACTED.1 && ! ADVANCED.1",
					"$$hashKey": "object:1192"
				}
			],
			"siteName": "detroit",
			"elementName": "artifactstemplate",
			"siteId": 652,
			"elementId": 14525,
			"customerId": 13,
			"userId": 13,
			"createdTimestamp": "29-06-2018:16:11:15",
			"modifiedtimestamp": "29-06-2018:16:11:15",
			"weekId": 26,
			"monthId": 5,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b345bbde4b0cae581def0a7",
			"artifactid": 6,
			"createdTimestamp": "28-06-2018:09:23:33",
			"customerId": 13,
			"description": "Lifter",
			"elementId": 14525,
			"elementName": "artifactstemplate",
			"inputsoutputs": [
				{
					"Id": 4,
					"Name": "LOWER",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:502"
				},
				{
					"Id": 3,
					"Name": "RAISE",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:393"
				},
				{
					"Id": 2,
					"Name": "LOWERED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:335"
				},
				{
					"Id": 1,
					"Name": "RAISED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:274"
				}
			],
			"modifiedtimestamp": "29-06-2018:15:52:02",
			"monthId": 5,
			"name": "Lifter",
			"siteId": 652,
			"siteName": "detroit",
			"states": [
				{
					"Id": 1,
					"Name": "AT.UP.POSITION",
					"statetype": "Static",
					"Equation": "RAISE && ! LOWER && RAISED",
					"$$hashKey": "object:572"
				},
				{
					"Id": 2,
					"Name": "AT.DOWN.POSITION",
					"statetype": "Static",
					"Equation": "LOWER && ! RAISE && LOWERED",
					"$$hashKey": "object:644"
				},
				{
					"Id": 3,
					"Name": "MOVING.UP",
					"statetype": "Action",
					"Equation": "RAISE && ! LOWER && ! RAISED",
					"$$hashKey": "object:713"
				},
				{
					"Id": 4,
					"Name": "MOVING.DOWN",
					"statetype": "Action",
					"Equation": "LOWER && ! RAISE &&  ! LOWERED",
					"$$hashKey": "object:783"
				},
				{
					"Id": 5,
					"Name": "OUTPUTS.OFF",
					"statetype": "Static",
					"Equation": "! LOWER && ! RAISE && ! LOWERED && ! RAISED",
					"$$hashKey": "object:921"
				}
			],
			"userId": 13,
			"weekId": 26,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b335923e4b0cae581deef80",
			"artifactid": 5,
			"name": "Escapement Stopper",
			"description": "Escapement Stopper",
			"inputsoutputs": [
				{
					"Id": 5,
					"Name": "LOWER",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:497"
				},
				{
					"Id": 4,
					"Name": "RAISE",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:439"
				},
				{
					"Id": 3,
					"Name": "PART.PASS",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:381"
				},
				{
					"Id": 2,
					"Name": "PART.PRESENT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:323"
				},
				{
					"Id": 1,
					"Name": "RAISED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:262"
				}
			],
			"states": [
				{
					"Id": 1,
					"Name": "ESCAPEMENT.STOPPER.UP.WITH.PART.PRESENT",
					"statetype": "Static",
					"Equation": "RAISE && RAISED && ! PART.PRESENT",
					"$$hashKey": "object:567"
				},
				{
					"Id": 2,
					"Name": "ESCAPEMENT.STOPPER.DOWN.WITH.PART.PRESENT",
					"statetype": "Static",
					"Equation": "LOWER && ! RAISED && !  PART.PRESENT",
					"$$hashKey": "object:641"
				},
				{
					"Id": 3,
					"Name": "ESCAPEMENT.STOPPER.UP.WITH.PART.ABSENT",
					"statetype": "Static",
					"Equation": "RAISE && RAISED &&  PART.PRESENT",
					"$$hashKey": "object:908"
				},
				{
					"Id": 4,
					"Name": "ESCAPEMENT.STOPPER.DOWN.WITH.PART.ABSENT",
					"statetype": "Static",
					"Equation": "LOWER && !  RAISED && PART.PRESENT",
					"$$hashKey": "object:1224"
				},
				{
					"Id": 5,
					"Name": "ESCAPEMENT.STOPPER.MOVING.UP.WITH.PART.PRESENT",
					"statetype": "Action",
					"Equation": "RAISE && ! RAISED && ! PART.PRESENT",
					"$$hashKey": "object:1295"
				},
				{
					"Id": 6,
					"Name": "ESCAPEMENT.STOPPER.MOVING.UP.WITH.PART.ABSENT",
					"statetype": "Action",
					"Equation": "RAISE && ! RAISED && PART.PRESENT",
					"$$hashKey": "object:1368"
				},
				{
					"Id": 7,
					"Name": "ESCAPEMENT.STOPPER.MOVING.DOWN.WITH.PART.PRESENT",
					"statetype": "Action",
					"Equation": "LOWER && RAISED && ! PART.PRESENT",
					"$$hashKey": "object:1439"
				},
				{
					"Id": 8,
					"Name": "ESCAPMENT.STOPPER.MOVING.DOWN.WITH.PART.ABSENT",
					"statetype": "Action",
					"Equation": "LOWER && RAISED && PART.PRESENT",
					"$$hashKey": "object:1510"
				}
			],
			"siteName": "detroit",
			"elementName": "artifactstemplate",
			"siteId": 652,
			"elementId": 14525,
			"customerId": 13,
			"userId": 13,
			"createdTimestamp": "27-06-2018:15:00:11",
			"modifiedtimestamp": "27-06-2018:15:00:11",
			"weekId": 26,
			"monthId": 5,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b32115be4b0cae581deed27",
			"artifactid": 4,
			"createdTimestamp": "26-06-2018:15:41:39",
			"customerId": 13,
			"description": "Accumulating Stopper",
			"elementId": 14525,
			"elementName": "artifactstemplate",
			"inputsoutputs": [
				{
					"Id": 4,
					"Name": "LOWER",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:20712"
				},
				{
					"Id": 3,
					"Name": "RAISE",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:20654"
				},
				{
					"Id": 2,
					"Name": "PART.PRESENT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:20596"
				},
				{
					"Id": 1,
					"Name": "RAISED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:20535"
				}
			],
			"modifiedtimestamp": "29-06-2018:17:34:40",
			"monthId": 5,
			"name": "Accumulating Stopper",
			"siteId": 652,
			"siteName": "detroit",
			"states": [
				{
					"Id": 1,
					"Name": "AT.UP",
					"statetype": "Static",
					"Equation": "RAISE && RAISED",
					"$$hashKey": "object:20839"
				},
				{
					"Id": 2,
					"Name": "AT.DOWN",
					"statetype": "Static",
					"Equation": "LOWER && !RAISED",
					"$$hashKey": "object:20911"
				},
				{
					"Id": 3,
					"Name": "MOVING.UP",
					"statetype": "Action",
					"Equation": "RAISE && !RAISED",
					"$$hashKey": "object:21038"
				},
				{
					"Id": 4,
					"Name": "MOVING.DOWN",
					"statetype": "Action",
					"Equation": "LOWER && RAISED",
					"$$hashKey": "object:21106"
				},
				{
					"Id": 5,
					"Name": "PART.PRESENT",
					"statetype": "Static",
					"Equation": "PART.PRESENT",
					"$$hashKey": "object:21170"
				}
			],
			"userId": 13,
			"weekId": 26,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b1f49a5e4b018ea8d4879b3",
			"artifactid": 3,
			"createdTimestamp": "12-06-2018:09:48:45",
			"customerId": 13,
			"elementId": 14525,
			"elementName": "artifactstemplate",
			"inputsoutputs": [
				{
					"Id": 3,
					"Name": "SLIDE.FORWARD.INPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:491"
				},
				{
					"Id": 2,
					"Name": "SLIDE.REVERSE.INPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:433"
				},
				{
					"Id": 1,
					"Name": "SLIDE.OUTPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:369"
				}
			],
			"modifiedtimestamp": "12-06-2018:09:49:47",
			"monthId": 5,
			"name": "CYLINDER-2I/1O",
			"siteId": 652,
			"siteName": "detroit",
			"states": [
				{
					"Id": 1,
					"Name": "SLIDE.MOVING.FORWARD",
					"statetype": "Action",
					"Equation": "SLIDE.OUTPUT && ! SLIDE.FORWARD.INPUT",
					"$$hashKey": "object:556"
				},
				{
					"Id": 2,
					"Name": "SLIDE.MOVING.REVERSE",
					"statetype": "Action",
					"Equation": "SLIDE.OUTPUT && ! SLIDE.REVERSE.INPUT",
					"$$hashKey": "object:625"
				},
				{
					"Id": 3,
					"Name": "SLIDE.AT.FORWARD",
					"statetype": "Static",
					"Equation": "SLIDE.FORWARD.INPUT",
					"$$hashKey": "object:690"
				},
				{
					"Id": 4,
					"Name": "SLIDE.AT.REVERSE",
					"statetype": "Static",
					"Equation": "SLIDE.REVERSE.INPUT",
					"$$hashKey": "object:751"
				}
			],
			"userId": 13,
			"weekId": 24,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b1f4838e4b018ea8d487959",
			"artifactid": 2,
			"name": "RFID",
			"inputsoutputs": [
				{
					"Id": 4,
					"Name": "RFID.READ.OUTPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:2395"
				},
				{
					"Id": 3,
					"Name": "RFID.WRITE.OUTPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:2337"
				},
				{
					"Id": 2,
					"Name": "RFID.WRITE.INPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:2279"
				},
				{
					"Id": 1,
					"Name": "RFID.READ.INPUT",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:2221"
				}
			],
			"states": [
				{
					"Id": 1,
					"Name": "RFID.READING",
					"statetype": "Action",
					"Equation": "RFID.READ.OUTPUT && ! RFID.READ.INPUT && ! RFID.WRITE.OUTPUT",
					"$$hashKey": "object:2463"
				},
				{
					"Id": 2,
					"Name": "RFID.READ.OVER",
					"statetype": "Static",
					"Equation": "RFID.READ.INPUT && ! RFID.WRITE.OUTPUT && ! RFID.READ.OUTPUT",
					"$$hashKey": "object:2529"
				},
				{
					"Id": 3,
					"Name": "RFID.WRITE.OVER",
					"statetype": "Static",
					"Equation": "RFID.WRITE.INPUT && ! RFID.READ.INPUT && ! RFID.WRITE.INPUT",
					"$$hashKey": "object:2661"
				},
				{
					"Id": 4,
					"Name": "RFID.WRITING",
					"statetype": "Action",
					"Equation": "RFID.WRITE.OUTPUT && ! RFID.WRITE.INPUT && ! RFID.READ.OUTPUT",
					"$$hashKey": "object:2792"
				}
			],
			"siteName": "detroit",
			"elementName": "artifactstemplate",
			"siteId": 652,
			"elementId": 14525,
			"customerId": 13,
			"userId": 13,
			"createdTimestamp": "12-06-2018:09:42:40",
			"modifiedtimestamp": "12-06-2018:09:42:40",
			"weekId": 24,
			"monthId": 5,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		},
		{
			"_id": "5b14f2aee4b018ea8d4857dd",
			"artifactid": 1,
			"createdTimestamp": "04-06-2018:13:35:02",
			"customerId": 13,
			"description": "Two inputs and two outputs",
			"elementId": 14525,
			"elementName": "artifactstemplate",
			"inputsoutputs": [
				{
					"Name": "REVERSE",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:1514"
				},
				{
					"Name": "FORWARD",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Output",
					"$$hashKey": "object:1453"
				},
				{
					"Name": "FORWARDED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:1218"
				},
				{
					"Name": "REVERSED",
					"InputType": "Sequence",
					"DataType": "BOOL",
					"IOType": "Input",
					"$$hashKey": "object:989"
				}
			],
			"modifiedtimestamp": "28-06-2018:12:14:22",
			"monthId": 5,
			"name": "CYLINDER-2I/2O",
			"siteId": 652,
			"siteName": "detroit",
			"states": [
				{
					"Id": 1,
					"Name": "MOVING.FORWARD",
					"statetype": "Action",
					"Equation": "FORWARD && ! FORWARDED",
					"$$hashKey": "object:227"
				},
				{
					"Id": 2,
					"Name": "AT.FORWARD",
					"statetype": "Static",
					"Equation": "FORWARDED && ! REVERSE && ( FORWARD || ! FORWARD)",
					"$$hashKey": "object:294"
				},
				{
					"Id": 3,
					"Name": "MOVING.REVERSE",
					"statetype": "Action",
					"Equation": "REVERSE && ! REVERSED",
					"$$hashKey": "object:427"
				},
				{
					"Id": 4,
					"Name": "AT.REVERSE",
					"statetype": "Static",
					"Equation": "REVERSED && ! FORWARDED && ( REVERSE || ! REVERSE)",
					"$$hashKey": "object:658"
				}
			],
			"userId": 13,
			"weekId": 23,
			"questions": [
				"artifactid",
				"name",
				"description",
				"states",
				"inputsoutputs"
			]
		}
	],
	"totalCount": 7
}


let eventList = {
	"elementName": "events",
	"results": [{ "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525332729112E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:51:54", "modifiedtimestamp": "03-07-2018:14:51:54", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b30e" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "badEvent", "performance": true, "timestamp": 1.525332723309E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:51:46", "modifiedtimestamp": "03-07-2018:14:51:46", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b30f" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "badEvent", "performance": true, "timestamp": 1.525332334807E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:47:29", "modifiedtimestamp": "03-07-2018:14:47:29", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b310" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "abcEvent", "performance": true, "timestamp": 1.525331975913E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:44:38", "modifiedtimestamp": "03-07-2018:14:44:38", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b311" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "abcEvent", "performance": true, "timestamp": 1.525331707499E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:41:14", "modifiedtimestamp": "03-07-2018:14:41:14", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b312" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "badEvent", "performance": true, "timestamp": 1.525328586214E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:38:02", "modifiedtimestamp": "03-07-2018:14:38:02", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b313" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525328465205E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:36:27", "modifiedtimestamp": "03-07-2018:14:36:27", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b314" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525328261906E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:33:33", "modifiedtimestamp": "03-07-2018:14:33:33", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b315" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525328140709E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:31:58", "modifiedtimestamp": "03-07-2018:14:31:58", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b316" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525328092614E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:31:20", "modifiedtimestamp": "03-07-2018:14:31:20", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b317" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525327941903E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:27:42", "modifiedtimestamp": "03-07-2018:14:27:42", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b318" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525327276407E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:24:40", "modifiedtimestamp": "03-07-2018:14:24:40", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b319" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.5253271556E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:23:02", "modifiedtimestamp": "03-07-2018:14:23:02", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31a" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525327063514E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:20:45", "modifiedtimestamp": "03-07-2018:14:20:45", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31b" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525326942321E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:18:35", "modifiedtimestamp": "03-07-2018:14:18:35", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31c" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525325814904E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:15:39", "modifiedtimestamp": "03-07-2018:14:15:39", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31d" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525325693718E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:14:04", "modifiedtimestamp": "03-07-2018:14:14:04", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31e" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525325574129E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:11:10", "modifiedtimestamp": "03-07-2018:14:11:10", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b31f" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525325438206E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:09:26", "modifiedtimestamp": "03-07-2018:14:09:26", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b320" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525325039704E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:06:33", "modifiedtimestamp": "03-07-2018:14:06:33", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b321" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324918414E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:04:57", "modifiedtimestamp": "03-07-2018:14:04:57", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b322" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324719405E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:02:00", "modifiedtimestamp": "03-07-2018:14:02:00", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b323" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324598208E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:14:00:10", "modifiedtimestamp": "03-07-2018:14:00:10", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b324" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324347314E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:57:19", "modifiedtimestamp": "03-07-2018:13:57:19", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b325" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324225602E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:55:48", "modifiedtimestamp": "03-07-2018:13:55:48", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b326" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525324067808E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:53:02", "modifiedtimestamp": "03-07-2018:13:53:02", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b327" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525323923399E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:51:25", "modifiedtimestamp": "03-07-2018:13:51:25", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b328" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525323152514E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:48:21", "modifiedtimestamp": "03-07-2018:13:48:21", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b329" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.5253230311E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:46:51", "modifiedtimestamp": "03-07-2018:13:46:51", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32a" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525322814306E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:44:04", "modifiedtimestamp": "03-07-2018:13:44:04", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32b" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525322693001E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:42:35", "modifiedtimestamp": "03-07-2018:13:42:35", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32c" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.5253225163E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:39:43", "modifiedtimestamp": "03-07-2018:13:39:43", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32d" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525322395025E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:38:13", "modifiedtimestamp": "03-07-2018:13:38:13", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32e" }, { "lineno": "1", "sequenceno": "1", "assetno": "48", "currentstate": "1913", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525322289101E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:35:32", "modifiedtimestamp": "03-07-2018:13:35:32", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b32f" }, { "lineno": "1", "sequenceno": "1", "assetno": "19", "currentstate": "1839", "nextstate": "0", "eventtype": "goodEvent", "performance": true, "timestamp": 1.525322159213E12, "siteName": "detroit", "elementName": "events", "siteId": 652, "elementId": 13916, "customerId": 13, "userId": 13, "createdTimestamp": "03-07-2018:13:33:54", "modifiedtimestamp": "03-07-2018:13:33:54", "weekId": 27, "monthId": 6, "questions": ["nextstate", "currentstate", "performance", "lineno", "sequenceno", "assetno", "timestamp", "eventtype"], "_id": "5b3f64ef758801481fe3b330" }],
	"totalCount": 35
}

let performanceData = [[1534767950000, 30], [1534771550000, 25], [1534775150000, 0], [1534778750000, 36], [1534782350000, 27]]
let trendData = [[1534767950000, 22.600011551956413], [1534771550000, 23.100005775981117], [1534775150000, 23.60000000000582], [1534778750000, 24.099994224030524], [1534782350000, 24.599988448055228]]
let cycleData =
	[[1534767947765, 1], [1534767981717, 1], [1534767981717, 2], [1534768015774, 2], [1534768015774, 3],
	[1534768332683, 3], [1534768332683, 4], [1534768365690, 4], [1534768365690, 5], [1534768401176, 5],
	[1534768401176, 6], [1534768434803, 6], [1534768434803, 7], [1534768469279, 7], [1534768469279, 8],
	[1534768503659, 8], [1534768503659, 9], [1534768539527, 9], [1534768539527, 10], [1534768573287, 10],
	[1534768573287, 11], [1534768607248, 11], [1534768607248, 12], [1534768671150, 12], [1534768671150, 13],
	[1534768704658, 13], [1534768704658, 14], [1534768738734, 14], [1534768738734, 15], [1534768809043, 15],
	[1534768809043, 16], [1534768842311, 16], [1534768842311, 17], [1534768876732, 17], [1534768876732, 18],
	[1534768948185, 18], [1534768948185, 19], [1534768981798, 19], [1534768981798, 20], [1534769016338, 20],
	[1534769016338, 21], [1534769086003, 21], [1534769086003, 22], [1534769120185, 22], [1534769120185, 23],
	[1534769224129, 23], [1534769224129, 24], [1534769257701, 24], [1534769257701, 25], [1534769812010, 25],
	[1534769812010, 26], [1534769846666, 26], [1534769846666, 27], [1534769882002, 27], [1534769882002, 28],
	[1534770011388, 28], [1534770011388, 29], [1534770045323, 29], [1534770045323, 30], [1534770079749, 30],
	[1534770079749, 31], [1534770113276, 31], [1534770113276, 32], [1534770151151, 32], [1534770151151, 33],
	[1534770200637, 33], [1534770200637, 34], [1534770234264, 34], [1534770234264, 35], [1534770268307, 35],
	[1534770268307, 36], [1534770343927, 36], [1534770343927, 37], [1534770378210, 37], [1534770378210, 38],
	[1534770412218, 38], [1534770412218, 39], [1534770494749, 39], [1534770494749, 40], [1534770527739, 40],
	[1534770527739, 41], [1534770562253, 41], [1534770562253, 42], [1534770645658, 42], [1534770645658, 43],
	[1534770679188, 43], [1534770679188, 44], [1534770712308, 44], [1534770712308, 45], [1534770796478, 45],
	[1534770796478, 46], [1534770829814, 46], [1534770829814, 47], [1534770863775, 47], [1534770863775, 48],
	[1534770946478, 48], [1534770946478, 49], [1534770980251, 49], [1534770980251, 50], [1534771014343, 50]]


const performanceOptionsObj = {
	chart: {
		zoomType: 'x',
		panning: true,
		panKey: 'shift'
	},

	// boost: {
	//     useGPUTranslations: true
	// },

	title: {
		text: ' Performance Chart '
	},

	subtitle: {
		text: ''
	},

	xAxis: {
		type: 'datetime',
		dateTimeLabelFormats: {
			day: "%e. %b",
			month: "%b '%y",
			year: "%Y",
			second: '%H:%M:%S',
		}
	},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},

	plotOptions: {
		series: {
			label: {
				connectorAllowed: false
			},

		}
	},

	tooltip: {
		valueDecimals: 2
	},

	series: [{
		data: performanceData,
		lineWidth: 0.5
	}],

	responsive: {
		rules: [{
			condition: {
				maxWidth: 500
			},
			chartOptions: {
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom'
				}
			}
		}]
	}

}

const trendOptionsObj = {
	chart: {
		zoomType: 'x',
		panning: true,
		panKey: 'shift'
	},

	// boost: {
	//     useGPUTranslations: true
	// },

	title: {
		text: ' Trend Line Chart '
	},

	subtitle: {
		text: ''
	},

	xAxis: {
		type: 'datetime',
		dateTimeLabelFormats: {
			day: "%e. %b",
			month: "%b '%y",
			year: "%Y"
		}
	},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},

	plotOptions: {
		series: {
			label: {
				connectorAllowed: false
			},
			pointStart: 2010
		}
	},

	tooltip: {
		valueDecimals: 2
	},

	series: [{
		data: trendData,
		lineWidth: 0.5
	}],

	responsive: {
		rules: [{
			condition: {
				maxWidth: 500
			},
			chartOptions: {
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom'
				}
			}
		}]
	}

}
const cycleOptionsObj = {
	chart: {
		zoomType: 'x',
		panning: true,
		panKey: 'shift'
	},

	// boost: {
	//     useGPUTranslations: true
	// },

	title: {
		text: ' Cycle Data Chart '
	},

	subtitle: {
		text: ''
	},

	xAxis: {
		type: 'datetime',
		dateTimeLabelFormats: {
			day: "%e. %b",
			month: "%b '%y",
			year: "%Y"
		}
	},
	yAxis: {
		title: {
			text: 'Cycles'
		}
	},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},

	plotOptions: {
		series: {
			label: {
				connectorAllowed: false
			},
			// pointStart: 2010
		}
	},

	tooltip: {
		valueDecimals: 2
	},

	series: [{
		data: cycleData,
		lineWidth: 0.5
	}],

	responsive: {
		rules: [{
			condition: {
				maxWidth: 500
			},
			chartOptions: {
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom'
				}
			}
		}]
	}

}


// export function _getConfigObj() {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => res({ ...configObj }), 1000)
// 	})
// }

// export function _saveConfig(resultItem) {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => {
// 			let parentChild = [];
// 			let myparentChild = addParentChild(resultItem.linemodel, resultItem.linemodel, parentChild)

// 			let itemFound = false
// 			let myResult = configObj.results.map((result) => {
// 				if (result._id === resultItem._id) {
// 					resultItem.linemodel.ParentChild = myparentChild
// 					itemFound = true
// 					return resultItem
// 				} else {
// 					if (resultItem.isactive) {
// 						result.isactive = false
// 					}
// 					return result
// 				}

// 			})

// 			let results = []
// 			if (itemFound) {
// 				//myResult.ParentChild = parentChild
// 				results = myResult
// 				configObj = Object.assign({}, configObj, { results: myResult })
// 			} else {
// 				resultItem.linemodel.ParentChild = myparentChild
// 				results = configObj.results.concat([resultItem])
// 				configObj = Object.assign({}, configObj, { results: Object.assign([...configObj.results], { [configObj.results.length]: resultItem }) })
// 			}
// 			res(results)
// 		}, 1000)
// 	})
// }

let myVal = 0;
function generateUID() {
	myVal += 1
	return myVal
}

// export function _getArtifactsData() {
// 	return allArtifacts;
// }

// export function _getEventList () {
// 	eventList.results.sort((a, b) => {
// 		return a.timestamp > b.timestamp
// 	})

// 	return eventList
// }

function getUID() {
	return myVal
}

function generateConfigID() {
	return new Date().getTime()
}

// export function _getConfig(_id) {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => {
// 			let configData = configObj.results.filter((iData) => iData._id === _id)
// 			const config = configData[0]
// 			res({ config })
// 		}, 1000)
// 	})
// }

// export function _getActiveConfig() {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => {
// 			let configData = configObj.results.filter((iData) => iData.isactive === true)
// 			const config = configData[0]
// 			res({ config })
// 		}, 1000)
// 	})
// }

// const addParentChild = (currentConfig, config, parentChildArray) => {
// 	for (let i = 0; i < currentConfig.nodes.length; i++) {
// 		parentChildArray.push({
// 			parent: currentConfig.nodeid,
// 			child: currentConfig.nodes[i].nodeid,
// 			isCopy: false
// 		})
// 		addParentChild(currentConfig.nodes[i], config, parentChildArray)
// 	}
// 	return parentChildArray
// }

// export function _saveArtifact(artifact) {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => {
// 			let itemFound = false
// 			let myResult = artifactList.results.map((result) => {
// 				if (result._id === artifact._id) {
// 					itemFound = true
// 					return artifact
// 				} else {
// 					return result
// 				}
// 			})

// 			let results = [];
// 			if (itemFound) {
// 				results = myResult
// 				artifactList = Object.assign({}, artifactList, {results: myResult})
// 			} else {
// 				results = artifactList.results.concat([artifact])
// 				artifactList = Object.assign({}, artifactList, {results: Object.assign([...artifactList.results], {[artifactList.results.length]: artifact})})
// 			}
// 			res(artifact)
// 		}, 1000)
// 	})
// }

// export function _getArtifact(_id) {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => {
// 			let artifactData = artifactList.results.filter((iData) => iData._id === _id)
// 			const artifact = artifactData[0]
// 			res({ artifact })
// 		}, 1000)
// 	})
// }

// export function _getArtifactList() {
// 	return new Promise((res, rej) => {
// 		setTimeout(() => res({ ...artifactList }), 1000)
// 	})
// }

// export function _emptyArtifact() {
// 	return null
// }
module.export = {
	_getPerformanceOptions: function () {
		return performanceOptionsObj
	}
}
module.export = {
	_getTrendOptions: function () {
		return trendOptionsObj
	}
}
const linemodel = (x) => (
	{
		nodeid: (x + 1),
		Name: "Line1",
		field: "Line",
		coupleType: "",
		isvirtualSubAssembly: false,
		linelogo: "",
		linedescription: "",
		Inputs: [],
		Outputs: [],
		States: [],
		inputsoutputs: [],
		nodes: [
			{
				// id:generateUID(),
				nodeid: (x + 2),
				Name: "Cell1",
				field: "Cell",
				coupleType: "",
				isvirtualSubAssembly: false,
				Inputs: [],
				Outputs: [],
				States: [],
				inputsoutputs: [],
				nodes: [
					{
						// id:generateUID(),
						nodeid: (x + 3),
						Name: "Asset1",
						field: "Asset",
						coupleType: "",
						isvirtualSubAssembly: false,
						Inputs: [],
						Outputs: [],
						States: [],
						inputsoutputs: [],
						nodes: [
							{
								// id:generateUID(),
								nodeid: (x + 4),
								Name: "SubAssembly",
								field: "SubAssembly",
								coupleType: "",
								isCoupled: false,
								isvirtualSubAssembly: false,
								Inputs: [],
								Outputs: [],
								States: [],
								nodes: [],
								inputsoutputs: []
							}
						]
					}
				]
			}
		]
	}
)

const result = (userid) => (
	{
		// _id: ""+generateConfigID(),
		lineid: 1,
		linemodel: {},
		// isActive: false,
		//configid: generateUID(),
		isuploadcompleted: false,
		remark: 'NA',
		// siteName: 'detroit',
		// elementName: 'pssconfiguration',
		// siteId: undefined,
		// elementId: undefined,
		customerId: parseInt(userid),
		nodeCount: 4,
		//userId: 13,
		// createdTimestamp: undefined,
		// modifiedtimestamp: undefined,
		// weekId: undefined,
		// monthId: undefined,
		// questions: [
		// 	'questions',
		// 	'lineid',
		// 	'isactive',
		// 	'isuploadcompleted',
		// 	'linemodel',
		// 	'configid'
		// ]
	}
)
const initResultItem = function (userid) {

	let resultObj = result(userid)
	resultObj.linemodel = linemodel(0)
	resultObj.nodeCount = 4
	resultObj.createdTimestamp = new Date().toISOString()
	resultObj.modifiedtimestamp = new Date().toISOString()
	return resultObj

}
module.exports = {
	configObj,
	artifactList,
	initResultItem,
	performanceOptionsObj,
	trendOptionsObj,
	cycleOptionsObj
}
