const generateConfigID = function () {
	return new Date().getTime()
}

let performanceData1 = [[1534767950000, 30], [1534771550000, 25], [1534775150000, 0], [1534778750000, 36], [1534782350000, 27]]
let performanceData2 = [[1534767950000, 20], [1534771550000, 30], [1534775150000, 2], [1534778750000, 18], [1534782350000, 21]]

const formatPerformanceObj = function () {
	//return "abcd"
	let myData = {
		chart: {
			zoomType: 'x',
			panning: true,
			panKey: 'shift'
		},	
		title: {
			text: ' Performance Chart '
		},	
		subtitle: {
			text: ''
		},	
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: {
				day: "%e. %b",
				month: "%b '%y",
				year: "%Y",
				second: '%H:%M:%S',
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},	
		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				},	
			}
		},
		tooltip: {
			valueDecimals: 2
		},	
		series: [{
			data: performanceData1,
			lineWidth: 0.5
		},
		{
			data: performanceData2,
			lineWidth: 0.5
		}],	
		responsive: {
			rules: [{
				condition: {
					maxWidth: 500
				},
				chartOptions: {
					legend: {
						layout: 'horizontal',
						align: 'center',
						verticalAlign: 'bottom'
					}
				}
			}]
		}	
	}
	return myData;
}

let trendData1 = [[1534767950000, 23.600011551956413], [1534771550000, 24.100005775981117], [1534775150000, 23.12000000000582], [1534778750000, 22.099994224030524], [1534782350000, 24.629988448055228]]
let trendData2 = [[1534767950000, 22.600011551956413], [1534771550000, 23.100005775981117], [1534775150000, 23.60000000000582], [1534778750000, 24.099994224030524], [1534782350000, 24.599988448055228]]

const formatTrendsObj = function() {
	return  {
		chart: {
			zoomType: 'x',
			panning: true,
			panKey: 'shift'
		},
	
		// boost: {
		//     useGPUTranslations: true
		// },
	
		title: {
			text: ' Trend Line Chart '
		},
	
		subtitle: {
			text: ''
		},
	
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: {
				day: "%e. %b",
				month: "%b '%y",
				year: "%Y"
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},
	
		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				},
				pointStart: 2010
			}
		},
	
		tooltip: {
			valueDecimals: 2
		},
	
		series: [{
			data: trendData1,
			lineWidth: 0.5
		},{
			data: trendData2,
			lineWidth: 0.5
		}],
	
		responsive: {
			rules: [{
				condition: {
					maxWidth: 500
				},
				chartOptions: {
					legend: {
						layout: 'horizontal',
						align: 'center',
						verticalAlign: 'bottom'
					}
				}
			}]
		}
	
	}
}

module.exports = {
	generateConfigID,
	formatPerformanceObj,
	formatTrendsObj
}