const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const db = require('_helpers/db');
// let Counter = db.Counter
// const config = require('config.json');
// var db = mongoose.connect(config.connectionString);
const config = require('config.json');
let connection = mongoose.createConnection(config.connectionString);
autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(connection);


const configSchema = new Schema({
    configid: { type: Number, unique: true, required: true, default: 1 },
    createdTimestamp: { type: Date, default: Date.now },
    customerId: { type: Number, },
    isactive: { type: Boolean, default: false },
    isuploadcompleted: { type: Boolean, default: false },
    lineid: { type: Number, },
    linemodel: { type: Object, },
    modifiedtimestamp: { type: Array },
    remark: { type: String },
    isdeleted: { type: Boolean, default: false },
    nodeCount: { type: Number, default: 4 }
}, { collection: 'pssconfiguration' });


configSchema.plugin(autoIncrement.plugin, { model: 'pssconfiguration', field: 'configid', startAt: 1 });

configSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('pssconfiguration', configSchema);