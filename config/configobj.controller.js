const express = require('express');
const router = express.Router();
const configService = require('./configobj.service');

router.get('/', getConfigObj);
router.get('/active', getActiveConfig);
router.get('/:id/', getConfigWithID);
router.post('/add', saveConfig);
router.get('/delete/:id', deleteConfig);
//router.get('/custome', customeUpdateConfig);


module.exports = router;

// function getArtifacts(req, res, next) {
//     configService.getArtifacts()
//         .then(artifacts => res.json(artifacts))
//         .catch(err => next(err));
// }

// function saveConfig(req,res,next){
//   let resultItem = req.body.config;
// let itemFound = false
// let myResult = configObj.results.map((result) => {
// 				if (result._id === resultItem._id) {
// 				  itemFound = true
// 					return resultItem
// 				}
//
// if(itemFound){
//   configService.saveConfig(myResult[0]._id,resultItem)
// }else {
//   configService.saveNewConfig(resultItem)
// }
//
//   res.send("DONE")
// })
// }

function saveConfig(req, res, next) {
    configService.saveConfig(req.body)
        .then((message) => res.json(message))
        .catch(err => next(err))
}

function getConfigObj(req, res, next) {
    let { customerid, pageno } = req.headers
    configService.getConfigObj(customerid, pageno)
        .then(configObj => res.json(configObj))
        .catch(err => next(err));
}

function getActiveConfig(req, res, next) {
    configService.getActiveConfig()
        .then(activeConfig => res.json(activeConfig))
        .catch(err => next(err));
}

function getConfigWithID(req, res, next) {
    const _id = req.params.id
    const userid = req.headers.userid
    configService.getConfigWithID(_id, userid)
        .then(config => res.json(config))
        .catch(err => next(err));
}


function deleteConfig(req, res, next) {
    const _id = req.params.id
    const customerid = req.headers.customerid
    configService.deleteConfig(_id, customerid)
        .then(configObj => res.json(configObj))
        .catch(err => next(err));
}

function customeUpdateConfig(req, res, next) {
    configService.customeUpdateConfig("JSON")
        .then(configObj => res.json(configObj))
        .catch(err => next(err));
}