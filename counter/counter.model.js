const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var counterSchema = Schema({
  seq: { type: Number, default: 0 }
});

counterSchema.pre('save', function (next) {
  next();

});

module.exports = mongoose.model('Counter', counterSchema);