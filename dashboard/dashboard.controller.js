const express = require('express');
const router = express.Router();
const dashboardService = require('./dashboard.service');


router.get('/performance', getPerformanceData);
router.get('/trend', getTrendData)
router.get('/cycle', getCycleData)
router.get('/line', getLineData)

module.exports = router;


function getPerformanceData(req, res, next) {
  dashboardService.getPerformanceData(req.body)
    .then(performanceOptionsObj => {
      return res.json(performanceOptionsObj)
    })
    .catch(err => next(err));
}

function getTrendData(req, res, next) {
  dashboardService.getTrendData(req.body)
    .then(trendObj => {
      return res.json(trendObj)
    })
    .catch(err => next(err));
}

function getCycleData(req, res, next) {
  dashboardService.getCycleData(req.body)
    .then(cycleObj => {
      return res.json(cycleObj)
    })
    .catch(err => next(err));
}

function getLineData(req, res, next) {
  dashboardService.getLineData(req.body)
    .then(lineObj => {
      return res.json(lineObj)
    })
    .catch(err => next(err));
}