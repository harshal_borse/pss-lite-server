var _Data = require('../utils/_Data.js')
const Commons = require('../utils/commons')

module.exports = {
	getPerformanceData,
	getTrendData,
	getCycleData,
	getLineData
}

async function getPerformanceData() {
	return await Commons.formatPerformanceObj()
}

async function getTrendData() {
	return await Commons.formatTrendsObj()
}

async function getCycleData() {
	return await _Data.cycleOptionsObj
}

async function getLineData() {
	//return await _Data.cycleOptionsObj
	//return await Commons.formatPerformanceObj()

	return await { "linedata": { "myList": [{ "name": "Bhupa" }, { "name": "Tandel" }] } }
}
