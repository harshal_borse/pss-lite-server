const mongoose = require('mongoose');
const config = require('config.json');
const Schema = mongoose.Schema;
let connection = mongoose.createConnection(config.connectionString);
autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(connection);

const schema = new Schema({
    userid: { type: String, unique: true, required: true, default: 1 },
    username: { type: String, required: true },
    hash: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    createdDate: { type: Date, default: Date.now }
});

schema.plugin(autoIncrement.plugin, { model: 'User', field: 'userid', startAt: 1 });

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);