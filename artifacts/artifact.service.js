const db = require('_helpers/db');
const User = db.User;
var _Data = require('../utils/_Data.js')
const Commons = require('../utils/commons.js')

module.exports = {
    getArtifacts,
    getArtifactWithID,
    addArtifact,
}

async function getArtifacts () {
    return await _Data.artifactList
}

async function getArtifactWithID (_id) {
    if (_id === 'new') {
        return await { _id: "" + Commons.generateConfigID(), artifactid: "" + Commons.generateConfigID(), name: '', description: '', inputsoutputs: [], states: [] }
    } else {
        const artifactData = _Data.artifactList.results.filter((iData) => iData._id === _id)
        const artifact = artifactData[0]
        return await artifact 
    }
    
}

async function addArtifact(requestParams) {
    let itemFound = false
    const { artifact } = requestParams
    let myResult = await _Data.artifactList.results.map((result) => {
        if (result._id === artifact._id) {
            itemFound = true
            return artifact
        } else {
            return result
        }
    })

    if (itemFound) {
        _Data.artifactList = Object.assign({}, _Data.artifactList, { results: myResult })
        return await {message: "Artifact updated successfully!!"}
    } else {
        _Data.artifactList = Object.assign({}, _Data.artifactList, { results: Object.assign([..._Data.artifactList.results], { [_Data.artifactList.results.length]: artifact }) })
        return await {message: "Artifact added successfully!!"}
    }
}
