const express = require('express');
const router = express.Router();
const artifactService = require('./artifact.service');

router.get('/', getArtifacts);
router.get('/:id', getArtifactWithID);
router.post('/add', addArtifact);

module.exports = router;

function getArtifacts(req, res, next) {
    artifactService.getArtifacts()
        .then(artifacts => res.json(artifacts))
        .catch(err => next(err));
}

function getArtifactWithID(req, res, next) {
    const _id = req.params.id
    artifactService.getArtifactWithID(_id)
        .then(artifact => res.json(artifact))
        .catch(err => next(err));
}

function addArtifact(req, res, next) {
    artifactService.addArtifact(req.body)
        .then((message) => res.json(message))
        .catch(err => next(err))
    //return getArtifacts(req,res,next)
}

// router.get('/:id', function(req, res, next) {
//     const _id = req.params.id
// })

